#include "../track.h"
#include "../syscall.h"
#include "../priorities.h"
#include "../userTasks.h"
#include "../io_helpers.h"

// need to reserve edges
// when an edge is reserved, the reverse edge should also be reserved

static int isReserved[TRACK_MAX][2];
static struct Reservations res;

static void addRes(int termAdmin, int train, int node, int edge, int isReserving) {
    assert(termAdmin, res.len < MAX_NUM_RESERVATIONS, "Too many reservations", 21);
    res.trainNum[res.len] = train;
    res.nodeIds[res.len] = node;
    res.edgeIds[res.len] = edge;
    res.isReserving[res.len] = isReserving;
    ++res.len;
    // if (isReserving)
    //     term_print(termAdmin, "r ", 2);
    // else
    //     term_print(termAdmin, "l ", 2);
}

void ReservationManager() {
    // setup
    RegisterAs("ReservManager");

    int termAdmin = WhoIs("TermAdmin");

    int tid, otherIdx, edgeIdx;
    enum MessageType type;

    // courier to tell termAdmin about reservations
    int termCourier = Create(CLIENT_PRIORITY_HIGH, Courier);
    struct CourierArgs config;
    config.server = termAdmin;
    config.maxMsgSize = sizeof(struct Reservations);
    TypedReceive(&tid, &type, "", 0);
    TypedReply(termCourier, SUCCESS, (char*) &config, sizeof(config));

    int termCourierReady = 0;

    // arguments: track node, edge idx and train number
    int args[3];

    for (int i = 0; i < TRACK_MAX; ++i)
        for (int j = 0; j < 2; ++j)
            isReserved[i][j] = -1;

    track_node* node;

    while(1) {
        // wait for request
        TypedReceive(&tid, &type, (char*) args, sizeof(args));
        // term_print(termAdmin, " tid: ", 6);
        // term_print_int(termAdmin, tid);
        // term_print(termAdmin, " type: ", 7);
        // term_print_int(termAdmin, type);
        // term_newline(termAdmin);
        switch(type) {
            case RESERVE_SEGMENT:
                // term_print(termAdmin, "got reserve segment", 19);
                // term_newline(termAdmin);
                if (isReserved[args[0]][args[1]] == args[2]) {
                    // you already own this track segment!
                    // term_print(termAdmin, "You own this", 12);
                    // term_newline(termAdmin);
                    TypedReply(tid, SUCCESS, "", 0);
                    break;
                }
#ifndef NORESERVE
                if (isReserved[args[0]][args[1]] != -1) {
                    // someone else has reserved this track segment 
                    // term_print(termAdmin, "Someone else owns this", 22);
                    // term_newline(termAdmin);
                    TypedReply(tid, ERROR, "", 0);
                    break;
                }
#endif
                TypedReply(tid, SUCCESS, "", 0);

                // term_print(termAdmin, "xxxx", 4);
                // term_newline(termAdmin);

                isReserved[args[0]][args[1]] = args[2];
                // also reserve the reverse
                node = getNode(args[0])->edge[args[1]].dest->reverse;
                otherIdx = node->id;
                edgeIdx = 0;

                if (node->type == NODE_BRANCH) {
                    if (node->edge[1].dest->reverse->id == args[0])
                        edgeIdx = 1;
                }

                isReserved[otherIdx][edgeIdx] = args[2];

                addRes(termAdmin, args[2], args[0], args[1], 1);
                addRes(termAdmin, args[2], otherIdx, edgeIdx, 1);

                // term_print(termAdmin, "Reserve ", 8);
                // term_print_int(termAdmin, args[0]);
                // term_print(termAdmin, " for train ", 11);
                // term_print_int(termAdmin, args[2]);
                // term_newline(termAdmin);

                // term_print(termAdmin, "Reserve ", 8);
                // term_print_int(termAdmin, otherIdx);
                // term_print(termAdmin, " for train ", 11);
                // term_print_int(termAdmin, args[2]);
                // term_newline(termAdmin);

                if (termCourierReady) {
                    TypedReply(termCourier, RESERVE, (char*) &res, sizeof(res));
                    termCourierReady = 0;
                    res.len = 0;
                }

                break;

            case RELEASE_SEGMENT:
                if (isReserved[args[0]][args[1]] != args[2]) {
                    // this train doesn't own this track segment
                    TypedReply(tid, ERROR, "", 0);
                    break;
                }

                TypedReply(tid, SUCCESS, "", 0);

                isReserved[args[0]][args[1]] = -1;
                // also free the reverse
                node = getNode(args[0])->edge[args[1]].dest->reverse;
                otherIdx = node->id;
                edgeIdx = 0;
                if (node->type == NODE_BRANCH) {
                    if (node->edge[1].dest->reverse->id == args[0])
                        edgeIdx = 1;
                }
                isReserved[otherIdx][edgeIdx] = -1;

                addRes(termAdmin, args[2], args[0], args[1], 0);
                addRes(termAdmin, args[2], otherIdx, edgeIdx, 0);

                // term_print(termAdmin, "Release ", 8);
                // term_print_int(termAdmin, args[0]);
                // term_print(termAdmin, " for train ", 11);
                // term_print_int(termAdmin, args[2]);
                // term_newline(termAdmin);

                // term_print(termAdmin, "Release ", 8);
                // term_print_int(termAdmin, otherIdx);
                // term_print(termAdmin, " for train ", 11);
                // term_print_int(termAdmin, args[2]);
                // term_newline(termAdmin);

                if (termCourierReady) {
                    TypedReply(termCourier, RESERVE, (char*) &res, sizeof(res));
                    termCourierReady = 0;
                    res.len = 0;
                }
                
            break;

            case RELEASE_ALL:
                TypedReply(tid, SUCCESS, "", 0);
                for (int i = 0; i < TRACK_MAX; ++i) {
                    for (int j = 0; j < 2; ++j) {
                        if (isReserved[i][j] == args[0]) {
                            isReserved[i][j] = -1;
                            addRes(termAdmin, args[0], i, j, 0);
                        }
                    }
                }

                if (termCourierReady) {
                    TypedReply(termCourier, RESERVE, (char*) &res, sizeof(res));
                    termCourierReady = 0;
                    res.len = 0;
                }
            break;

            case COURIER_QUERY:
                termCourierReady = 1;
                if (res.len > 0) {
                    TypedReply(termCourier, RESERVE, (char*) &res, sizeof(res));
                    termCourierReady = 0;
                    res.len = 0;
                }
                break;

            default:
                TypedReply(tid, ERROR, "", 0);
        }
    }
}
