#include "../io_helpers.h"
#include "../syscall.h"

// set up datastructures
struct Client {
	int signedUp;
	int opponentTid;
	int opponentQuit;
	char play;
};

static struct Client clients[100];
static int clientQueue = -1;

static void print_tie(int a, int b, char play) {
	int writeServer = WhoIs("TermSendServer");
	Puts(writeServer, "Task ", 5);
	print_int(writeServer, a);
	Puts(writeServer, " played ", 8);
	Putc(writeServer, play);
	Puts(writeServer, ".\r\n", 3);
	Puts(writeServer, "Task ", 5);
	print_int(writeServer, b);
	Puts(writeServer, " played ", 8);
	Putc(writeServer, play);
	Puts(writeServer, ".\r\n", 3);
	Puts(writeServer, "The tasks tie!\r\n\n", 17);

	int readServer = WhoIs("TermReadServer");
	Getc(readServer); // busy wait until get confirmation
}

static void print_win(int winner, char winningPlay, int loser, char losingPlay) {
	int writeServer = WhoIs("TermSendServer");
	Puts(writeServer, "Task ", 5);
	print_int(writeServer, winner);
	Puts(writeServer, " played ", 8);
	Putc(writeServer, winningPlay);
	Puts(writeServer, ".\r\n", 3);
	Puts(writeServer, "Task ", 5);
	print_int(writeServer, loser);
	Puts(writeServer, " played ", 8);
	Putc(writeServer, losingPlay);
	Puts(writeServer, ".\r\n", 3);
	Puts(writeServer, "Task ", 5);
	print_int(writeServer, winner);
	Puts(writeServer, " wins!\r\n\n", 9);

	int readServer = WhoIs("TermReadServer");
	Getc(readServer); // busy wait until get confirmation
} 

// if we have an error, wait for a proper message
static void play(int tid, char *msg, int len) {
	if (!clients[tid].signedUp) { // if not signed up, reply with error
		TypedReply(tid, ERROR, "", 0);
		return;
	}
	if (clients[tid].opponentQuit) { // if opponent quit, send quit response
		TypedReply(tid, QUIT_RPS, "", 0);
		clients[tid].signedUp = 0;
		return;
	}
	if (len) { // need to have sent a char
		if (msg[0] != 'R' && msg[0] != 'P' && msg[0] != 'S') { // if non-valid play, reply with error
			TypedReply(tid, ERROR, "", 0);
			return;
		}
		clients[tid].play = msg[0];
		int other = clients[tid].opponentTid;
		if (clients[other].play) {
			if (clients[other].play == msg[0]) { // we have a tie!
				TypedReply(tid, RESULT_RPS, "T", 1);
				TypedReply(other, RESULT_RPS, "T", 1);
				print_tie(tid, other, msg[0]);
			} else {
				switch(msg[0]) {
					case 'R':
						if (clients[other].play == 'P') {
							TypedReply(tid, RESULT_RPS, "L", 1);
							TypedReply(other, RESULT_RPS, "W", 1);
							print_win(other, 'P', tid, 'R');
						} else { // other is S
							TypedReply(tid, RESULT_RPS, "W", 1);
							TypedReply(other, RESULT_RPS, "L", 1);
							print_win(tid, 'R', other, 'S');
						}
						break;
					case 'P':
						if (clients[other].play == 'R') {
							TypedReply(tid, RESULT_RPS, "W", 1);
							TypedReply(other, RESULT_RPS, "L", 1);
							print_win(tid, 'P', other, 'R');
						} else { // other is S
							TypedReply(tid, RESULT_RPS, "L", 1);
							TypedReply(other, RESULT_RPS, "W", 1);
							print_win(other, 'S', tid, 'P');
						}
						break;
					case 'S':
						if (clients[other].play == 'R') {
							TypedReply(tid, RESULT_RPS, "L", 1);
							TypedReply(other, RESULT_RPS, "W", 1);
							print_win(other, 'R', tid, 'S');
						} else { // other is P
							TypedReply(tid, RESULT_RPS, "W", 1);
							TypedReply(other, RESULT_RPS, "L", 1);
							print_win(tid, 'S', other, 'P');
						}
						break; 
				}
			}
			// clear the play characters
			clients[tid].play = 0;
			clients[other].play = 0;
		}
	} else {
		TypedReply(tid, ERROR, "", 0);
	}
}

static void signup(int tid) {
	if (clients[tid].signedUp) {
		TypedReply(tid, ERROR, "", 0);
		return;
	}
	int writeServer = WhoIs("TermSendServer");
	Puts(writeServer, "Signed up task ", 15);
	print_int(writeServer, tid);
	Puts(writeServer, ".\r\n\n", 4);
	if (clientQueue < 0) {
		clientQueue = tid;
		return;
	}
	
	// two tasks are waiting to play
	clients[clientQueue].signedUp = 1;
	clients[clientQueue].opponentTid = tid;
	clients[clientQueue].opponentQuit = 0;
	clients[clientQueue].play = 0;

	clients[tid].signedUp = 1;
	clients[tid].opponentTid = clientQueue;
	clients[tid].opponentQuit = 0;
	clients[tid].play = 0;

	TypedReply(clientQueue, SUCCESS, "", 0);

	clientQueue = -1;

	TypedReply(tid, SUCCESS, "", 0);
}

static void quit(int tid) {
	if (!clients[tid].signedUp) {
		TypedReply(tid, ERROR, "", 0);
		return;
	}
	clients[tid].signedUp = 0;

	int other = clients[tid].opponentTid;
	if (clients[other].signedUp) { // if other player did not also quit
		clients[other].opponentQuit = 1;
		if (clients[other].play) { // if other player already played, send response now
			clients[other].signedUp = 0;
			TypedReply(other, QUIT_RPS, "", 0);
		}
	}

	TypedReply(tid, SUCCESS, "", 0);

	int writeServer = WhoIs("TermReadServer");

	Puts(writeServer, "Task ", 5);
	print_int(writeServer, tid);
	Puts(writeServer, " quit.\r\n\n", 9);
}

void RPSServer() {
	for (int i = 0; i < 100; ++i)
		clients[i].signedUp = 0;

	int success = RegisterAs("RPSServer");
	int writeServer = WhoIs("TermReadServer");
	assert(writeServer, success == 0, "Failed to register\r\n", 20);

	char msg[] = "RPSServer is live. After each round, press any key to resume play.\r\n";
	Puts(writeServer, msg, sizeof(msg) - 1);

	while(1){
		int tid;
		char msg[10];
		enum MessageType type;
		int len = TypedReceive(&tid, &type, msg, 10);
		if (type == PLAY_RPS) {
			play(tid, msg, len);
		} else if (type == SIGNUP_RPS) {
			signup(tid);
		} else if (type == QUIT_RPS) { 
			quit(tid);
		} else { // unsupported message type
			TypedReply(tid, ERROR, "", 0);
		}
	}
}
