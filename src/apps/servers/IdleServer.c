#include "../userTasks.h"
#include "../priorities.h"
#include "../syscall.h"
#include "../io_helpers.h"

void IdleServer() {
    // create courier to termAdmin
    int tid;
    enum MessageType type;

    RegisterAs("IdleServer");

    int termAdmin = WhoIs("TermAdmin");

    int courier = Create(CLIENT_PRIORITY_LOW, Courier);
    struct CourierArgs config;
    config.server = termAdmin;
    config.maxMsgSize = 4; // only one integer
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == COURIER_ARGS, "Incorrect config request idle courier\r\n", 39);
    TypedReply(tid, SUCCESS, (char*) &config, sizeof(config));

    int arg;
    int idlePercentage = 0;
    int hasChanged = 0;
    int courierWaiting = 0;
    while(1) {
        TypedReceive(&tid, &type, (char*) &arg, 4);

        if (type == UPDATE_IDLETIME) {
            TypedReply(tid, SUCCESS, "", 0);
            idlePercentage = arg;
            hasChanged = 1;
            if (courierWaiting) {
                TypedReply(courier, UPDATE_IDLETIME, (char*) &idlePercentage, 4);
                hasChanged = 0;
                courierWaiting = 0;
            }

        } else if (type == COURIER_QUERY) {
            courierWaiting = 1;
            if (hasChanged) {
                TypedReply(courier, UPDATE_IDLETIME, (char*) &idlePercentage, 4);
                hasChanged = 0;
                courierWaiting = 0;
            }

        } else {
            TypedReply(tid, ERROR, "", 0);
        }
    }
}