#include "../syscall.h"
#include "../userTasks.h"
#include "../interrupts.h"
#include "../io_helpers.h"
#include <string.h>
#include "../priorities.h"

#define MAX_MSG_SIZE 300

static void setupWorkers(int *notifier, int *CTSnotifier, int *relay, int uart) {
    // set up notifiers
    int tid;
    enum MessageType type;
    struct NotifierArgs notifierArgs;
    struct NotifierArgs ctsNotifierArgs;
    
    *notifier = Create(UARTSERVER_PRIORITY, Notifier);
    notifierArgs.eventID = uart ? TRAIN_TRANSMIT_INTERRUPT : TERMINAL_TRANSMIT_INTERRUPT; 
    notifierArgs.notificationType = TRANSMIT_NOTIFICATION;
    notifierArgs.shouldWaitToEnable = 1;

    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == NOTIFIER_ARGS, "Incorrect config request send notifier\r\n", 40);
    

    // if train server, set up the CTS notifier
    // in TERMONLY mode, set up delaycourier to imitate CTS notifier
    
    if (uart) {
        #ifndef TERMONLY
        *CTSnotifier = Create(UARTSERVER_PRIORITY, Notifier);
        ctsNotifierArgs.eventID = TRAIN_CTS_INTERRUPT;
        ctsNotifierArgs.notificationType = CTS_NOTIFICATION;
        ctsNotifierArgs.shouldWaitToEnable = 0;

        TypedReceive(&tid, &type, "", 0);
        debug_assert(type == NOTIFIER_ARGS, "Incorrect config request cts notifier\r\n", 39);
        #else
        *CTSnotifier = Create(UARTSERVER_PRIORITY, DelayCourier);

        TypedReceive(&tid, &type, "", 0);
        debug_assert(type == DELAY_ARGS, "Incorrect config request cts delay\r\n", 36);
        #endif
    }
    

    // set up relay
    struct RelayArgs relayArgs;
    *relay = Create(UARTSERVER_PRIORITY, Relay);
    relayArgs.name = uart ? "TrainSendServer" : "TermSendServer";
    relayArgs.maxMsgSize = MAX_MSG_SIZE;

    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == RELAY_ARGS, "Incorrect config request send relay\r\n", 37);

    // unblock workers after all have been created so receives above are actually from new tasks
    TypedReply(*notifier, SUCCESS, (char*) &notifierArgs, sizeof(notifierArgs));
    if (uart) {
        #ifndef TERMONLY
        TypedReply(*CTSnotifier, SUCCESS, (char*) &ctsNotifierArgs, sizeof(ctsNotifierArgs));
        #else
        int isDelayUntil = 0;
        TypedReply(*CTSnotifier, SUCCESS, (char*) &isDelayUntil, sizeof(isDelayUntil));
        #endif
    }
    TypedReply(*relay, SUCCESS, (char*) &relayArgs, sizeof(relayArgs));
}

// will return 0 if cannot send yet
static int putCharacter(int uart, char c) {
    /*if(uart){
	    debug("put ", 4);
	    debug_int(c);
	    debug("\r\n", 2);
    }*/
    if(UartWriteRegister(uart, UART_THR, c) == 0)
        return 1;
    return 0;
}

static void tryPutString(int *totalCharacters, int *charactersPut, int *waitingOnCTS, int *waitingOnInterrupt, int *lastWasSwitch, int uart, char *msg, int relay, int notifier, int CTSnotifier) {
    if (!(*waitingOnInterrupt) && !(*waitingOnCTS) && *totalCharacters) {
        for (int i = *charactersPut; i < *totalCharacters; ++i) {
            if (putCharacter(uart, msg[i])) {
                ++(*charactersPut);
                if (uart) {
                    #ifndef TERMONLY
                    *waitingOnCTS = 2;
                    #else
                    *waitingOnCTS = 1;
                    int delay = 1; // wait a little bit for the buffer to be empty again
                    TypedReply(CTSnotifier, SUCCESS, (char*) &delay, 4);
                    #endif
                    return; // train set is slow...
                }
                
            } else {
                TypedReply(notifier, SUCCESS, "", 0);
                *waitingOnInterrupt = 1;
                return;
            }
        }
        // wow we finished!
        TypedReply(relay, SUCCESS, "", 0);
        // RESET
        *totalCharacters = 0;
        *charactersPut = 0;
        #ifdef TERMONLY
        if(uart) {
            // for simplicity this assumes each Puts call will only have one command
            // this is currently how we use the code
            // if no CTS persists for longer, we can reassess this
            int delay = 2; // default for train command, reset sensor command, etc
            switch(msg[0]) {
                case 0x21: 
                case 0x22: // switch command
                    if (*lastWasSwitch) {
                        delay = 15; // consecutive
                    } else {
                        delay = 2; //first
                    }
                    *lastWasSwitch = 1;
                    break;
                case 0x20: // solenoid
                    delay = 6;
                    *lastWasSwitch = 0;
                    break;
            }
            TypedReply(CTSnotifier, SUCCESS, (char*) &delay, 4);
            *waitingOnCTS = 1; // since we are simulating CTS
        }
        #endif
    }
}

void UARTSendServer() {
    enum MessageType type;
    int uart, tid, len;
    TypedSend(MyParentTid(), UARTSERVER_ARGS, "", 0, &type, (char *) &uart, 4);
    debug_assert(type == SUCCESS, "UART server could not get args\r\n", 32);

    int relay, notifier, CTSnotifier;
    setupWorkers(&notifier, &CTSnotifier, &relay, uart);

    // bools for the state of the system
    int waitingOnCTS = 0;
    #ifdef TERMONLY
    if (uart)
        waitingOnCTS = 1; // wait for delaycourier
    #endif
    int waitingOnInterrupt = 1; // gets a send from notifier to block once initially
    int lastWasSwitch = 0;
    
    // saved state if waiting
    int totalCharacters = 0;
    int charactersPut = 0;
    char savedMsg[MAX_MSG_SIZE];

    char msg[MAX_MSG_SIZE];

    // handle requests
    while (1) {
        len = TypedReceive(&tid, &type, msg, sizeof(msg));

        if (type == PUT_CHAR) {
            totalCharacters = 1;
            savedMsg[0] = msg[0]; // in case we don't succeed
            tryPutString(&totalCharacters, &charactersPut, &waitingOnCTS, &waitingOnInterrupt, &lastWasSwitch, uart, savedMsg, relay, notifier, CTSnotifier);

        } else if (type == PUT_STRING) {
	    totalCharacters = len;
	    for(int i = 0; i < len; ++i){
		    savedMsg[i] = msg[i];
	    }

        tryPutString(&totalCharacters, &charactersPut, &waitingOnCTS, &waitingOnInterrupt, &lastWasSwitch, uart, savedMsg, relay, notifier, CTSnotifier);
            
        } else if (type == TRANSMIT_NOTIFICATION) {
            debug_assert(waitingOnInterrupt, "Transmit interrupt should be disabled\r\n", 39);
            waitingOnInterrupt = 0;
            tryPutString(&totalCharacters, &charactersPut, &waitingOnCTS, &waitingOnInterrupt, &lastWasSwitch, uart, savedMsg, relay, notifier, CTSnotifier);

        } else if (type == CTS_NOTIFICATION) {
            debug_assert(uart, "Should not get CTS notifications\r\n", 34);
            TypedReply(tid, SUCCESS, "", 0);
            waitingOnCTS--; 
            tryPutString(&totalCharacters, &charactersPut, &waitingOnCTS, &waitingOnInterrupt, &lastWasSwitch, uart, savedMsg, relay, notifier, CTSnotifier);

        } else if (type == DELAY_QUERY) {
            debug_assert(uart, "Should not get CTS notifications\r\n", 34);
            waitingOnCTS--;
            tryPutString(&totalCharacters, &charactersPut, &waitingOnCTS, &waitingOnInterrupt, &lastWasSwitch, uart, savedMsg, relay, notifier, CTSnotifier);

        } else {
            TypedReply(tid, ERROR, "", 0);
        }
    }
}
