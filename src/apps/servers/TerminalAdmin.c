#include "../syscall.h"
#include "../io_helpers.h"
#include "../priorities.h"
#include "../userTasks.h"
#include "../sensors.h"
#include "../track.h"

#define NUM_TRAIN_SLOTS 3

static const int termRow = 32;
static int termCol = 1;
static const int idleCol = 63;
static const int timeCol = 9;
static const int swRow = 3;
static const int sensRows[4] = {14, 19, 24, 29};
static const int sensStartCol = 8;
static const int trainInfoCol = 3;
static const int diagramRow = 4;
static const int diagramCol = 70;
static const int startDebugRow = 34;
static int debugRow = 34;
static int debugCol = 1;

static const int destCol = 21;
static const int queuedDestCol = 43;


static char UI1[] = "Uptime:                           \033[35mOSu!\033[0m         Idle fraction:\r\n\r\n"
                    "Switch positions                                                     Reservations\r\n"
                    "------------------------------------------------------------------\r\n";
static char UI2[] = "| Sw Nr  || 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 |\r\n"
                    "| Sw Pos || C  | C  | C  | C  | C  | C  | C  | C  | C  | C  | C  |\r\n"
                    "------------------------------------------------------------------\r\n"
                    "| Sw Nr  || 12 | 13 | 14 | 15 | 16 | 17 | 18 | 153| 154| 155| 156|\r\n";
static char UI3[] = "| Sw Pos || C  | C  | C  | C  | C  | C  | C  | C  | S  | C  | S  |\r\n"
                    "------------------------------------------------------------------\r\n\r\n"
                    "------------------------------------------------------------------\r\n"
                    "| Unregistered                                                   |\r\n";
static char UI4[] = "| Sens                                                           |\r\n"
                    "| Δt                                                             |\r\n"
                    "| Δx                                                             |\r\n"
                    "------------------------------------------------------------------\r\n";
static char UI5[] = "| Unregistered                                                   |\r\n"
                    "| Sens                                                           |\r\n"
                    "| Δt                                                             |\r\n"
                    "| Δx                                                             |\r\n";
static char UI6[] = "------------------------------------------------------------------\r\n"
                    "| Unregistered                                                   |\r\n"
                    "| Sens                                                           |\r\n"
                    "| Δt                                                             |\r\n";
static char UI7[] = "| Δx                                                             |\r\n"
                    "------------------------------------------------------------------\r\n"
                    "| Unattributed                                                   |\r\n"
                    "| Sens                                                           |\r\n";
static char UI8[] = "------------------------------------------------------------------\r\n";

static char TA1[] = "       ---- B ------ B ----";
static char TA2[] = "     /    /            \\    \\";
static char TA3[] = "   S    S                S    S";
static char TA4[] = " /    /                    \\    \\";
static char TA5[] = "|   S                        S   S   E";
static char TA6[] = "|   |                        |   |   |";
static char TA7[] = "|   B                        B   B   S";
static char TA8[] = "|   | \\                    / |   | \\ |";
static char TA9[] = "S   S   S                S   S   S   B";
static char TA10[] = "|   |     \\            /     |   |   |";
static char TA11[] = "|   |       S        S       |   |   |";
static char TA12[] = "|   |         \\    /         |   |   |";
static char TA13[] = "|   |          B--B          |   |   |";
static char TA14[] = "|   |         /    \\         |   |   |";
static char TA15[] = "|   |       S        S       |   |   |";
static char TA16[] = "|   |     /            \\     |   |   |";
static char TA17[] = "S   S   S                S   S   S   B";
static char TA18[] = "|   | /                    \\ |   | / |";
static char TA19[] = "|   B                        B   B   S";
static char TA20[] = "|   |                        |   |   |";
static char TA21[] = "B   S                        S   S   B";
static char TA22[] = "| \\   \\                    /   /   / |";
static char TA23[] = "B   --- B                B ---   B   S";
static char TA24[] = "| \\       \\            /       / |   |";
static char TA25[] = "S   B       S ------ S       B   S   S";
static char TA26[] = "|   | \\                    / |   |   |";
static char TA27[] = "E   S   S                S   S   S   E";
static char TA28[] = "    |   |                |   |   |";
static char TA29[] = "    E   E                E   S   E";
static char TA30[] = "                             |";
static char TA31[] = "                             E";

static void moveCursor(int sendServer, int row, int col) {
    Puts(sendServer, "\033[", 2);
    print_int(sendServer, row);
    Putc(sendServer, ';');
    print_int(sendServer, col);
    Putc(sendServer, 'H');
}

static const int colCodeLen = 5;
static char T0ColCode[] = "\033[35m";
static char T1ColCode[] = "\033[31m";
static char T2ColCode[] = "\033[32m";
static char noneColCode[] = "\033[37m";

static void addReservation(int sendServer, int nodeNr, int edge, char* colCode, int len) {
    // default won't change anything
    int row = 0;
    int col = 0;
    char c = ' ';
    get_diagram_info(nodeNr, edge, &row, &col, &c);
    moveCursor(sendServer, diagramRow + row, diagramCol + col);
    Puts(sendServer, colCode, len);
    Putc(sendServer, c);
    Puts(sendServer, noneColCode, sizeof(noneColCode) - 1);
    moveCursor(sendServer, termRow, termCol);
}

static void drawDiagram(int sendServer, int startRow, int startCol) {
    moveCursor(sendServer, startRow, startCol);
    Puts(sendServer, TA1, sizeof(TA1) - 1);
    moveCursor(sendServer, startRow + 1, startCol);
    Puts(sendServer, TA2, sizeof(TA2) - 1);
    moveCursor(sendServer, startRow+2, startCol);
    Puts(sendServer, TA3, sizeof(TA3) - 1);
    moveCursor(sendServer, startRow+3, startCol);
    Puts(sendServer, TA4, sizeof(TA4) - 1);
    moveCursor(sendServer, startRow+4, startCol);
    Puts(sendServer, TA5, sizeof(TA5) - 1);
    moveCursor(sendServer, startRow+5, startCol);
    Puts(sendServer, TA6, sizeof(TA6) - 1);
    moveCursor(sendServer, startRow+6, startCol);
    Puts(sendServer, TA7, sizeof(TA7) - 1);
    moveCursor(sendServer, startRow+7, startCol);
    Puts(sendServer, TA8, sizeof(TA8) - 1);
    moveCursor(sendServer, startRow+8, startCol);
    Puts(sendServer, TA9, sizeof(TA9) - 1);
    moveCursor(sendServer, startRow+9, startCol);
    Puts(sendServer, TA10, sizeof(TA10) - 1);
    moveCursor(sendServer, startRow+10, startCol);
    Puts(sendServer, TA11, sizeof(TA11) - 1);
    moveCursor(sendServer, startRow+11, startCol);
    Puts(sendServer, TA12, sizeof(TA12) - 1);
    moveCursor(sendServer, startRow+12, startCol);
    Puts(sendServer, TA13, sizeof(TA13) - 1);
    moveCursor(sendServer, startRow+13, startCol);
    Puts(sendServer, TA14, sizeof(TA14) - 1);
    moveCursor(sendServer, startRow+14, startCol);
    Puts(sendServer, TA15, sizeof(TA15) - 1);
    moveCursor(sendServer, startRow+15, startCol);
    Puts(sendServer, TA16, sizeof(TA16) - 1);
    moveCursor(sendServer, startRow+16, startCol);
    Puts(sendServer, TA17, sizeof(TA17) - 1);
    moveCursor(sendServer, startRow+17, startCol);
    Puts(sendServer, TA18, sizeof(TA18) - 1);
    moveCursor(sendServer, startRow+18, startCol);
    Puts(sendServer, TA19, sizeof(TA19) - 1);
    moveCursor(sendServer, startRow+19, startCol);
    Puts(sendServer, TA20, sizeof(TA20) - 1);
    moveCursor(sendServer, startRow+20, startCol);
    Puts(sendServer, TA21, sizeof(TA21) - 1);
    moveCursor(sendServer, startRow+21, startCol);
    Puts(sendServer, TA22, sizeof(TA22) - 1);
    moveCursor(sendServer, startRow+22, startCol);
    Puts(sendServer, TA23, sizeof(TA23) - 1);
    moveCursor(sendServer, startRow+23, startCol);
    Puts(sendServer, TA24, sizeof(TA24) - 1);
    moveCursor(sendServer, startRow+24, startCol);
    Puts(sendServer, TA25, sizeof(TA25) - 1);
    moveCursor(sendServer, startRow+25, startCol);
    Puts(sendServer, TA26, sizeof(TA26) - 1);
    moveCursor(sendServer, startRow+26, startCol);
    Puts(sendServer, TA27, sizeof(TA27) - 1);
    moveCursor(sendServer, startRow+27, startCol);
    Puts(sendServer, TA28, sizeof(TA28) - 1);
    moveCursor(sendServer, startRow+28, startCol);
    Puts(sendServer, TA29, sizeof(TA29) - 1);
    moveCursor(sendServer, startRow+29, startCol);
    Puts(sendServer, TA30, sizeof(TA30) - 1);
    moveCursor(sendServer, startRow+30, startCol);
    Puts(sendServer, TA31, sizeof(TA31) - 1);
}

static int minutes;
static int seconds;
static int dseconds;

static void updateTime(int sendServer) {
    ++dseconds;
    if (dseconds == 10) {
        ++seconds;
        dseconds = 0;
        if (seconds == 60) {
            ++minutes;
            seconds = 0;
        }
    }
    // TODO: optimize so only rewrite
    moveCursor(sendServer, 1, timeCol); 
    print_int(sendServer, minutes);
    Puts(sendServer, " mins ", 6);
    print_int(sendServer, seconds);
    Putc(sendServer, '.');
    print_int(sendServer, dseconds);
    Puts(sendServer, " secs ", 6);
    moveCursor(sendServer, termRow, termCol);
}

static void printSwitch(int sendServer, int sw, char posn) {
	if (sw < 12) { // on first row
		moveCursor(sendServer, swRow + 3, 8 + 5 * sw);
		Putc(sendServer,posn);
	} else { // on second row
		if (sw > 18) // adjust for large switch numbers
			sw = sw - 134;
		moveCursor(sendServer, swRow + 6, 8 + 5 * (sw - 11));
		Putc(sendServer,posn);
	}
    moveCursor(sendServer, termRow, termCol);
}

static char* getColCode(int id) {
    switch(id) {
        case 0:
            return T0ColCode;
        case 1:
            return T1ColCode;
        case 2:
            return T2ColCode;
        default:
            return noneColCode;
    }
}

static int getIdFromNum(int train, int *regTrain) {
    for (int i = 0; i < NUM_TRAIN_SLOTS; ++i)
        if (regTrain[i] == train)
            return i;
    return -1;
}

void TerminalAdmin() {
    // INITIALIZATION
    int success = RegisterAs("TermAdmin");

    int sendServer = WhoIs("TermSendServer");
    debug_assert(sendServer >= 0, "WhoIs failed\r\n", 13);
    assert(sendServer, success == 0, "Unable to register\r\n", 20);
    
    // UI currently supports two registered trains
    int regTrain[NUM_TRAIN_SLOTS];
    for (int i = 0; i < NUM_TRAIN_SLOTS; ++i) {
        regTrain[i] = -1;
    }

    // Init DelayCourier
    Create(ADMIN_PRIORITY, DelayCourier);
    int isDelayUntil = 1;
    int tid;
    enum MessageType type;
    
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == DELAY_ARGS, "Incorrect config request read delay\r\n", 37);
    TypedReply(tid, SUCCESS, (char*) &isDelayUntil, 4);
    
    // clear screen
    Puts(sendServer, "\033[2J\033[H", 7);
    
    // Print initial interface
    Puts(sendServer, UI1, sizeof(UI1) - 1);
    Puts(sendServer, UI2, sizeof(UI2) - 1);
    Puts(sendServer, UI3, sizeof(UI3) - 1);
    Puts(sendServer, UI4, sizeof(UI4) - 1);
    Puts(sendServer, UI5, sizeof(UI5) - 1);
    Puts(sendServer, UI6, sizeof(UI6) - 1);
    Puts(sendServer, UI7, sizeof(UI7) - 1);
    Puts(sendServer, UI8, sizeof(UI8) - 1);

    int isTrackA = 0;
    // move cursor to command line
    moveCursor(sendServer, termRow, 1);
    
    // ask user to specify track
    Puts(sendServer, "Please enter 'A' or 'a' for track A. Enter 'B' or 'b' for track B", 65);

    int nextDelay = 0;
    int len;
    char args[sizeof(struct Reservations)]; // size of largest type of message received
    struct SensorMessage *msg;

    int lastCol = 0;

    // handle requests
    while (1) {
        len = TypedReceive(&tid, &type, args, sizeof(args));

        switch(type) {
            case UPDATE_IDLETIME: // first four chars of args is the idle percentage
                TypedReply(tid, SUCCESS, "", 0);
                int *percentage = (int*) args;
                moveCursor(sendServer, 1, idleCol);
                print_int(sendServer, *percentage);
                Puts(sendServer, "% ", 2);
                moveCursor(sendServer, termRow, termCol);
                break;

            case UPDATE_SWITCH: // first four chars of args is the switch number
                                // next char is the new switch position
                TypedReply(tid, SUCCESS, "", 0);
                int *sw = (int*) args;
                char posn = args[4];
                assert(sendServer, posn == 'C' || posn == 'S', "Incorrect switch position\r\n", 27);
                printSwitch(sendServer, *sw, posn);
                break;

            case COMMAND_CHARACTER: // one character, the character to be written to the screen
                TypedReply(tid, SUCCESS, "", 0);
                Putc(sendServer, args[0]);
                ++termCol;
                break;

            case COMMAND_BACKSPACE:
                TypedReply(tid, SUCCESS, "", 0);
                if (termCol > 3) {
                    --termCol;
                    moveCursor(sendServer, termRow, termCol);
                    Putc(sendServer,' ');
                    moveCursor(sendServer, termRow, termCol);
                }
                break;

            case COMMAND_CLEAR:
                TypedReply(tid, SUCCESS, "", 0);
                termCol = 3;
                moveCursor(sendServer, termRow, termCol);
                Puts(sendServer,"                    ", 20);
                moveCursor(sendServer, termRow, termCol);
                break;

            case DEBUG:
                TypedReply(tid, SUCCESS, "", 0);
                moveCursor(sendServer, debugRow, debugCol);
                Puts(sendServer, args, len);
                debugCol += len;
                moveCursor(sendServer, termRow, termCol);
                break;
            
            case DEBUG_NEWLINE:
                TypedReply(tid, SUCCESS, "", 0);
                ++debugRow;
                debugCol = 1;
                break;

            case DEBUG_CLEAR:
                TypedReply(tid, SUCCESS, "", 0);
                while(debugRow >= startDebugRow) {
                    moveCursor(sendServer, debugRow, 1);
                    Puts(sendServer, "\033[K", 3);
                    --debugRow;
                }
                debugRow = startDebugRow;
                debugCol = 1;
                moveCursor(sendServer, termRow, termCol);
                break;

            case SENSOR_TRIGGERED:
                TypedReply(tid, SUCCESS, "", 0);
                msg = (struct SensorMessage *) args;
                int col = sensStartCol;
                int regId = msg->trainId;
                if (regId >= NUM_TRAIN_SLOTS)
                    regId = NUM_TRAIN_SLOTS;

                // print new data
                for (int i = 0; i < msg->num; ++i) {
                    moveCursor(sendServer, sensRows[regId], col);
                    Putc(sendServer, msg->sensors[i].ch);
                    print_int(sendServer, msg->sensors[i].num);
                    Putc(sendServer, ' ');
                    if (regId != NUM_TRAIN_SLOTS) {
                        // these are attributed sensor measurements
                        moveCursor(sendServer, sensRows[regId] + 1, col);
                        print_int(sendServer, msg->sensorTimeDiffs[i]);
                        Puts(sendServer, "  ", 2);
                        moveCursor(sendServer, sensRows[regId] + 2, col);
                        print_int(sendServer, msg->sensorDistances[i]);
                        Puts(sendServer, "   ", 3);
                    }
                    col = col+5;
                }
                moveCursor(sendServer, termRow, termCol);
                break;

            case DELAY_QUERY:
                nextDelay += 10;
                TypedReply(tid, SUCCESS, (char*) &nextDelay, 4);
                updateTime(sendServer);
                break;

            case TRACK_INITIALIZED:
                TypedReply(tid, SUCCESS, "", 0);
                moveCursor(sendServer, termRow, termCol);
                Puts(sendServer, ">                                                                ", 65);
                termCol = 3;
                if (args[0] == 'A' || args[0] == 'a') {
                    drawDiagram(sendServer, diagramRow, diagramCol);
                    isTrackA = 1;
                }
                moveCursor(sendServer, termRow, termCol);
                break;
            case CONFIRM_POSITION:
                int train = (int) *args;
                if (getIdFromNum(train, regTrain) != -1) {
                    TypedReply(tid, ERROR, "", 0); // train already registered
                    break;
                }
                TypedReply(tid, SUCCESS, "", 0);
                termCol = 1;
                moveCursor(sendServer, termRow, termCol);
                Puts(sendServer, "Press any key to confirm train position leads to >= 2 sensors.", 62);
                break;
            case POSITION_CONFIRMED:
                TypedReply(tid, SUCCESS, "", 0);
                moveCursor(sendServer, termRow, termCol);
                Puts(sendServer, ">                                                                ", 65);
                termCol = 3;
                moveCursor(sendServer, termRow, termCol);
                break;
            case REGISTRATION:
                TypedReply(tid, SUCCESS, "", 0);
                // resp[0] is train num
                // resp[1] is registration ID
                int *resp = (int*) args;
                regTrain[resp[1]] = resp[0];
                // print train information
                moveCursor(sendServer, sensRows[resp[1]] - 1, trainInfoCol);
                Puts(sendServer, getColCode(resp[1]), colCodeLen);
                Puts(sendServer, "Train ", 6);
                print_int(sendServer, resp[0]);
                Puts(sendServer, getColCode(-1), colCodeLen);
                Puts(sendServer, "    Dest: None     Queued Dest: None", 36);
                moveCursor(sendServer, termRow, termCol);
                break;

            case RESERVE_TEST:
                TypedReply(tid, SUCCESS, "", 0);
                int *node = (int*) args;
                // alternate colours
                // both edges if branch
                if (isTrackA) {
                    if (lastCol) {
                        addReservation(sendServer, *node, 0, T0ColCode, sizeof(T0ColCode) - 1);
                        addReservation(sendServer, *node, 1, T0ColCode, sizeof(T0ColCode) - 1);
                        lastCol = 0;
                    } else {
                        addReservation(sendServer, *node, 0, T1ColCode, sizeof(T1ColCode) - 1);
                        addReservation(sendServer, *node, 1, T1ColCode, sizeof(T1ColCode) - 1);
                        lastCol = 1;
                    }
                }
                break;
            case RESERVE:
                TypedReply(tid, SUCCESS, "", 0);
                struct Reservations *res = (struct Reservations*) args;
                char *color;
                
                for (int i = 0; i < res->len; ++i) {
                    color = getColCode(-1);
                    if (res->isReserving[i]) {
                        int id = getIdFromNum((int)res->trainNum[i], regTrain);
                        color = getColCode(id);
                    }

                    // moveCursor(sendServer, debugRow, debugCol);
                    // Puts(sendServer, "Print ", 6);
                    // print_int(sendServer, res->nodeIds[i]);
                    // ++debugRow;
                    // debugCol = 1;
                    
                    addReservation(sendServer, (int) res->nodeIds[i], (int) res->edgeIds[i], color, colCodeLen);
                }
                break;
            case UPDATE_DESTS:
                TypedReply(tid, SUCCESS, "", 0);
                int *dests = (int*) args;
                // args[0] is id
                // args[1] is target or -1
                // args[2] is queued target or -1
                moveCursor(sendServer, sensRows[dests[0]]-1, destCol);
                if (dests[1] != -1) {
                    print_int(sendServer, dests[1]);
                    Puts(sendServer, "   ", 3);
                } else {
                    Puts(sendServer, "None", 4);
                }
                moveCursor(sendServer, sensRows[dests[0]]-1, queuedDestCol);
                if (dests[2] != -1) {
                    print_int(sendServer, dests[2]);
                    Puts(sendServer, "   ", 3);
                } else {
                    Puts(sendServer, "None", 4);
                }
                break;
                
            default:
                TypedReply(tid, ERROR, "", 0);
        }
    }
}
