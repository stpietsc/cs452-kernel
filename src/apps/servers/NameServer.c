#include "../syscall.h"
#include "../NameServer.h"
#include <string.h>
#include "../io_helpers.h"

// Names must always be null-terminated
struct TaskName {
    int tid;
    char name[NAME_LENGTH + 1]; // extra space for null terminator
    int nameLen;
};

// will never run out of space - one slot for each task and each task has at most one entry
struct RegisteredNames {
    struct TaskName tname[NUM_ENTRIES];
    int len; 
};

static struct RegisteredNames names;

// returns index of tasks currently entry, if it exists
// otherwise, returns -1
static int get_entry_idx(int tid) {
    for (int i = 0; i < names.len; ++i) {
        if (names.tname[i].tid == tid)
            return i;
    }
    return -1;
}

// Overwrites existing entry if one already exists
// returns 0 if there is no more space in the array
static int add_entry(int tid, const char *n, int nlen) {
    int idx = get_entry_idx(tid);
    if (idx == NUM_ENTRIES)
        return 0;
    if (idx < 0) {
        idx = names.len;
        ++names.len;
    }
    names.tname[idx].tid = tid;
    strncpy(names.tname[idx].name, n, nlen); // n is guaranteed to be null-terminated and correct length
    names.tname[idx].nameLen = nlen;
    return 1;
}

// returns -1 if nname is not in names
// starts looking at the back of names, ensuring we start with more recent entries and can retrieve
// correct tid for overwritten names
static int get_tid(const char *name, int nlen) {
    int equal;
    for (int i = names.len - 1; i >= 0; --i) {
        if (names.tname[i].nameLen == nlen) {
            equal = 1;
            for (int j = 0; j < nlen; ++j) {
                if (names.tname[i].name[j] != name[j]) {
                    equal = 0;
                    break;
                }  
            }
            if (equal)
                return names.tname[i].tid;
        }
    }
    return -1;
}

// what if we receive an identical name from a new task? 
// always return the id of the last task to register under that name
// if we get a new call to register then delete old entry
void NameServer() {
    // set up
    names.len = 0;

    while(1) {
        int tid;
        char msg[NAME_LENGTH + 1]; // extra space for null terminator
        enum MessageType type;
        // wait for request
        int len = TypedReceive(&tid, &type, msg, NAME_LENGTH + 1);

        // respond to request
        if (type == REGISTER_NAME) { // always succeeds - name length checked before sending to name server
            int success = add_entry(tid, msg, len);
            if (success)
                TypedReply(tid, SUCCESS, "", 0);
            else
                TypedReply(tid, ERROR, "", 0);
        } else if (type == QUERY_NAME) {
            int query_tid = get_tid(msg, len);
            if (query_tid < 0)
                TypedReply(tid, ERROR, "", 0);
            else {
                char reply[1];
                reply[0] = query_tid;
                TypedReply(tid, TASK_ID, reply, 1);
            }
            
        } else { // not a valid name server request
            TypedReply(tid, ERROR, "", 0);
        }
    }
}