#include "../syscall.h"
#include "../io_helpers.h"

void TrainProprietor() {
    // initialization
    RegisterAs("TrainProprietor");

    int sendServer = WhoIs("TrainSendServer");
    int readServer = WhoIs("TrainReadServer");

    char args[2]; // bytes to be sent to train set
    int len, tid;
    enum MessageType type;
    char sensBytes[10];
    // handle requests
    while (1) {
        len = TypedReceive(&tid, &type, (char*) args, sizeof(args));
        debug_assert(len <= 2, "Command too long\r\n", 18);

        if (type == TRAIN_TRACK_COMMAND) {
            TypedReply(tid, SUCCESS, "", 0);
            Puts(sendServer, args, len);

        } else if (type == SENSOR_COMMAND) { // does not accept any other requests while waiting on sensor data
            Putc(sendServer, 133);
            len = Gets(readServer, sensBytes, 10, 10);
            TypedReply(tid, SUCCESS, sensBytes, len);

        } else {
            TypedReply(tid, ERROR, "", 0);
        }
    }
}
