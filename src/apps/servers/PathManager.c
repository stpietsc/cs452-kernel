#include "../userTasks.h"
#include "../syscall.h"
#include "../train_data.h"
#include "../priorities.h"
#include "../path.h"
#include "../io_helpers.h"
#include "../sensors.h"
#include "../velocity_helpers.h"
#include "../track.h"

// anticipate we can start executing tasks 100ms after starting planning
#define BEGIN_TIME_BUFFER 500

// saves actual train number of each id
static int trainNums[NUM_TRAINS] = {0};
// save position of the ith train to register
static struct train_position positions[NUM_TRAINS];
static int trainTargets[NUM_TRAINS]; // -1 for no pathing
static int savedTrainTargets[NUM_TRAINS];

static int termAdmin;
static int trackAdmin;
static int clockServer;

// get train index from tid in execPaths or from trainNum in trainIds
static int getTrainIndex(int id, int *execPaths) {
    for (int i = 0; i < NUM_TRAINS; ++i)
        if (execPaths) {
            if (execPaths[i] == id)
                return i;
        } else {
            if (trainNums[i] == id)
                return i;
        }
            
    return -1;
}

static int pathFinder = 0;
static int pathFinderReady = 0;
struct PathFinderRequest pathFindReqs[NUM_TRAINS];
static int savedOffset = 0;

static void updatePathFindReqOnFail(struct PathFinderRequest* req){
    // term_print(termAdmin, "Fail pathfinding", 16);
    // term_newline(termAdmin);
	// if the train has moved to a different position, first try again from the new position
    if (req->num_failures > 8) {
        // give up, go to random location first instead
        term_print(termAdmin, "Rerouting to random", 19);
        term_newline(termAdmin);
        req->targetNode = Time(clockServer) % 144;
        req->num_failures = 0;
        return;
    }
        if(req->srcNode != positions[req->pathId].nodeId){
		req->srcNode = positions[req->pathId].nodeId;
        req->num_failures++;
		return;
	}
	switch(req->num_failures % 2){
		case 0:
			// try going to the reverse node
			req->targetNode = getNode(req->targetNode)->reverse->id;
			req->num_failures++;
			break;
		case 1:
			// try reversing the train
			req->srcNode = getNode(req->srcNode)->reverse->id;
			req->num_failures++;
			break;
	}
}

static void sendNewPathFindReq(){
	if(pathFinderReady == 0) return;
	for(int i = 0; i < NUM_TRAINS; ++i){
		if (pathFindReqs[i].targetNode != -1) {
            term_print(termAdmin, "Sending req", 11);
            term_print_int(termAdmin, i);
            term_newline(termAdmin);
            TypedReply(pathFinder, SUCCESS, (char*) &pathFindReqs[i], sizeof(pathFindReqs[i]));
            pathFinderReady = 0;
		    return;
        }
	}
}

static void setupPathFindReq(int pathId, int target, int targetOffset, int exclude) {
    enum MessageType type;
    pathFindReqs[pathId].srcNode = positions[pathId].nodeId;

    int args[MAX_EXCLUDED+1];
    args[0] = 0;

    struct track_node *next = getNode(positions[pathId].nodeId)->edge[0].dest;
    if (next->type == NODE_BRANCH) {
        // switch safety - exclude one setting of switch right after src
        args[0] = 1;
        char pos;
        TypedSend(trackAdmin, GET_POSITION, (char*) &(next->num), 4, &type, &pos, 1);

        if (pos == 'C') {
            args[1] = next->edge[DIR_AHEAD].dest->id;
        } else {
            args[1] = next->edge[DIR_CURVED].dest->id;
        }
    }

    if (exclude != -1) {
        ++args[0];
        args[args[0]] = exclude;
    }

    pathFindReqs[pathId].targetNode = target;
    pathFindReqs[pathId].pathId = pathId;
    savedOffset = targetOffset;
    pathFindReqs[pathId].num_excluded = args[0];
    for(int i = 0; i < args[0]; ++i){
        pathFindReqs[pathId].excluded_nodes[i] = args[1 + i];
    }
}

void PathManager() {
    RegisterAs("PathManager");

    termAdmin = WhoIs("TermAdmin");
    int trainAdmin = WhoIs("TrainAdmin");
    trackAdmin = WhoIs("TrackAdmin");
    clockServer = WhoIs("ClockServer");
    int sensorAdmin = WhoIs("SensorAdmin");

    int tid;
    enum MessageType type;

    // set up workers
    int termCourier = Create(ADMIN_PRIORITY, Courier);
    struct CourierArgs config;
    config.server = termAdmin;
    config.maxMsgSize = sizeof(struct SensorMessage);
    TypedReceive(&tid, &type, "", 0);

    int termCourier2 = Create(ADMIN_PRIORITY, Courier);
    struct CourierArgs config2;
    config2.server = termAdmin;
    config2.maxMsgSize = 12; // 3 integers
    TypedReceive(&tid, &type, "", 0);

    int sensorCourier = Create(ADMIN_PRIORITY, Courier);
    struct CourierArgs sensConfig;
    sensConfig.server = sensorAdmin;
    sensConfig.maxMsgSize = 4; // one integer
    TypedReceive(&tid, &type, "", 0);
    
    TypedReply(termCourier, SUCCESS, (char*) &config, sizeof(config));
    TypedReply(termCourier2, SUCCESS, (char*) &config2, sizeof(config2));
    TypedReply(sensorCourier, SUCCESS, (char*) &sensConfig, sizeof(sensConfig));

    // set up clients
    // owns PathFinder
    pathFinder = Create(CLIENT_PRIORITY, PathFinder);
    for(int i = 0; i < NUM_TRAINS; ++i){
	pathFindReqs[i].srcNode = -1;
	pathFindReqs[i].targetNode = -1;
	pathFindReqs[i].num_excluded = 0;
    trainTargets[i] = -1;
    savedTrainTargets[i] = -1;
    }

    // owns instance of PathPlanner
    int pathPlanner = Create(CLIENT_PRIORITY, PathPlanner);
    
    struct PathPlannerRequest pathPlannerReq;
    int pathPlannerReady = 0;
    pathPlannerReq.pathId = -1;

    // owns instance of CollisionAvoidance
    int collisionAvoidance = 0;
#ifndef NOCA
    collisionAvoidance = Create(CLIENT_PRIORITY, CollisionAvoidance);
#endif
    struct CollisionAvoidanceRequest collisionReq;
    int collisionAvoidanceReady = 0;
    collisionReq.num_trains = 0;
    collisionReq.handled = 0;

    // own path execution tasks
    // created on train registration
    int execPath[NUM_TRAINS];

    int execPathReady[NUM_TRAINS] = { 0 };
    int readyForExec[NUM_TRAINS] = { 0 };

    struct ExecutePathRequest execReq;

    // 4 + MAX_EXCLUDED is used for GOTO_NODE
    int args[4 + MAX_EXCLUDED];
    int numTrainsRegistered = 0;

    // saved data

    int termCourierReady = 0;
    int termCourier2Ready = 0;
    int sensCourierReady = 0;

    int targetsChanged[NUM_TRAINS] = {0};

    int waitingOnReg = -1;
    int waitingOnRegId = -1;
    
    int train;
    int idx;
    int len;

    int pathId = -1;
    int triggerTime = -1;
    while(1) {
        // wait for request
        len = TypedReceive(&tid, &type, (char*) args, sizeof(args));

        // term_print(termAdmin, " tid: ", 6);
        // term_print_int(termAdmin, tid);
        // term_print(termAdmin, " type: ", 7);
        // term_print_int(termAdmin, type);
        // term_newline(termAdmin);

        switch(type) {
            case STOP_PATHING:
                TypedReply(tid, SUCCESS, "", 0);
                term_print(termAdmin, "Stop", 4);
                term_newline(termAdmin);
                pathId = getTrainIndex(args[0], 0);
                trainTargets[pathId] = -1;
                savedTrainTargets[pathId] = -1;

                if (termCourier2Ready) {
                    int dests[3];
                    dests[0] = pathId;
                    dests[1] = trainTargets[pathId];
                    dests[2] = savedTrainTargets[pathId];
                    TypedReply(termCourier2, UPDATE_DESTS, (char*) dests, sizeof(dests));
                    termCourier2Ready = 0;
                    targetsChanged[pathId] = 0;
                } else {
                    targetsChanged[pathId] = 1;
                }
            break;
            case GOTO_NODE:
                // args[0] holds train number
                // args[1] holds node number
                // args[2] holds offset
		// args[3] holds number of excluded nodes
		// remaining args hold excluded nodes

            args[3] = 0; // not used for now

		    pathId = getTrainIndex(args[0], 0);

		    if (pathId < 0) {
                term_print(termAdmin, "Train not registered", 20);
                term_newline(termAdmin);
                TypedReply(tid, ERROR, "", 0);
                break;
            }
            TypedReply(tid, SUCCESS, "", 0);

            // term_print(termAdmin, "Cur target ", 11);
            // term_print_int(termAdmin, trainTargets[pathId]);
            // term_newline(termAdmin);

            if (trainTargets[pathId] == -1) {
                trainTargets[pathId] = args[1];
                savedTrainTargets[pathId] = Time(clockServer) % 144; // continue pathfinding after reaching the target

                if (pathFindReqs[pathId].targetNode == -1) { 
        
                    setupPathFindReq(pathId, args[1], args[2], -1);

                    // term_print(termAdmin, "Goto request received", 21);
                    // term_newline(termAdmin);

                    if (pathFinderReady) {
                        sendNewPathFindReq();
                    }
                }
            } else {
                savedTrainTargets[pathId] = args[1];
            }

            if (termCourier2Ready) {
                int dests[3];
                dests[0] = pathId;
                dests[1] = trainTargets[pathId];
                dests[2] = savedTrainTargets[pathId];
                TypedReply(termCourier2, UPDATE_DESTS, (char*) dests, sizeof(dests));
                termCourier2Ready = 0;
                targetsChanged[pathId] = 0;
            } else {
                targetsChanged[pathId] = 1;
            }

            break;
            case REGISTER_TRAIN:
                // TEMPORARY CODE
                // initialize position based on unattributed sensor triggers
                if (numTrainsRegistered == NUM_TRAINS) {
                    term_print(termAdmin, "Already registered max number of trains", 39);
                    term_newline(termAdmin);
                    TypedReply(tid, ERROR, "", 0);
                } else {
                    assert(termAdmin, sensCourierReady, "Sensor courier got lost", 23);

                    // request position of new train
                    TypedReply(sensorCourier, NEW_TRAIN_POS, "", 0);

                    // TypedReply(tid, SUCCESS, "", 0);
                    trainNums[numTrainsRegistered] = args[0];
                    waitingOnRegId = numTrainsRegistered;

                    execPath[numTrainsRegistered] = Create(CLIENT_PRIORITY_HIGH, ExecutePath);
		    collisionReq.train_ids[numTrainsRegistered] = args[0];

                    ++numTrainsRegistered;
		    collisionReq.num_trains = numTrainsRegistered;

                    waitingOnReg = tid;
                }
            break;
            // Path finding
            case FOUND_PATH:
                // args[0] is pathId of the path updated
		
	            pathId = args[0];
                pathFinderReady = 1;
		        int src = pathFindReqs[pathId].srcNode; 
		        int curPos = positions[pathId].nodeId;
		
		        if((src == curPos) || (src == getNode(curPos)->reverse->id)){
			        pathFindReqs[pathId].targetNode = -1;

			        // this should be ok??? we'll only ever have 2 trains and PathPlanner should never fail
	                assert(termAdmin, pathPlannerReq.pathId == -1, "PathPlanner too busy", 20);

                    train = trainNums[pathId];
                    int resp[2];
                    TypedSend(trainAdmin, GET_SPEED, (char*) &train, 4, &type, (char*) resp, sizeof(resp));
                    int time = Time(clockServer) + BEGIN_TIME_BUFFER;

                    int lastCmd[3];
                    TypedSend(trainAdmin, GET_LAST_CMD, (char*) &train, 4, &type, (char*) lastCmd, sizeof(lastCmd));
	
        	        pathPlannerReq.pathId = pathId;
                	pathPlannerReq.srcOffset = distance_micrometers_traveled_between(termAdmin, train, lastCmd[0], lastCmd[1], lastCmd[2], resp[0], positions[args[0]].nodeId, resp[0], positions[args[0]].time, time) / 1000;
                    // term_print(termAdmin, "Source off ", 11);
                    // term_print_int(termAdmin, pathPlannerReq.srcOffset);
                    // term_newline(termAdmin);
	                pathPlannerReq.targetOffset = savedOffset;
        	        savedOffset = 0;
                	pathPlannerReq.beginTime = time + BEGIN_TIME_BUFFER;
                	pathPlannerReq.train = train;
			        pathPlannerReq.reverseAtStart = (src != curPos);

                	term_print(termAdmin, "Path planning request set up", 28);
                	term_newline(termAdmin);

                	if (pathPlannerReady) {
                            term_print(termAdmin, "Path planning request sent", 26);
                	        term_newline(termAdmin);
                    		TypedReply(pathPlanner, SUCCESS, (char*) &pathPlannerReq, sizeof(pathPlannerReq));
                    		pathPlannerReady = 0;
                    		pathPlannerReq.pathId = -1;
                	}
		        } else {
                    term_print(termAdmin, "Path from incorrect pos", 23);
                    term_newline(termAdmin);
			        // if the train has moved from its intended (possibly reversed) start node, just do pathfinding again
			        updatePathFindReqOnFail(&pathFindReqs[pathId]);
		        }

		        sendNewPathFindReq();
            break;
            case FIND_PATH_UNSUCCESSFUL:
                term_print(termAdmin, "Find path failed",16);
                term_newline(termAdmin); 
                pathFinderReady = 1;
		pathId = args[0];
		updatePathFindReqOnFail(&pathFindReqs[pathId]);
		sendNewPathFindReq();
            break;
	    case FIND_PATH_INIT:
                pathFinderReady = 1;
                // term_print(termAdmin, "Path finder ready", 17);
                // term_newline(termAdmin);
		break;
            // Path planning
            case PLAN_PATH_UNSUCCESSFUL:
                term_print(termAdmin, "Path plan unsuccessful", 22);
                term_newline(termAdmin);
                if (len) {
                    // weren't able to use the path, so go to random destination first
                    updatePathFindReqOnFail(&pathFindReqs[args[0]]);
                    sendNewPathFindReq();
                }
                pathPlannerReady = 1;
                if (pathPlannerReq.pathId != -1) {
                    term_print(termAdmin, "Path planning request sent", 26);
                    term_newline(termAdmin);
                    TypedReply(pathPlanner, SUCCESS, (char*) &pathPlannerReq, sizeof(pathPlannerReq));
                    pathPlannerReady = 0;
                    pathPlannerReq.pathId = -1;
                }
            break;
            case PLAN_PATH:
                // returns the pathId
                pathPlannerReady = 1;

                term_print(termAdmin, "Path planning complete", 22);
                term_newline(termAdmin);

                // Debug: print the path planning commands
                //int arg1, arg2;
                // if (getReverseAtStart(args[0])){
                // 	term_print(termAdmin, "reverse at start", 16);
                // 	term_newline(termAdmin);
                // }
                // // if (getBeginCommand(args[0], &arg1, &arg2)) {
                // //     term_print(termAdmin, "Begin: speed", 12);
                // //     print_int(termAdmin, arg2);
                // //     term_newline(termAdmin);
                // // }
                // struct path_node *node;
                // for (int i = 0; i < getPathLength(args[0]); ++i) {
                //     node = getPathNode(args[0], i);
                //     term_print_int(termAdmin, node->nodeId);
                //     term_print(termAdmin, " ", 1);
                //     term_print_int(termAdmin, node->arrivalTime);
                //     if (node->commandType) {
                //         term_print(termAdmin, " : ", 3);
                //         term_print_int(termAdmin, node->commandArgs[0]);
                //         term_print(termAdmin, " ", 1);
                //         term_print_int(termAdmin, node->commandArgs[1]);
                //         term_print(termAdmin, " ", 1);
                //         term_print_int(termAdmin, node->commandDelay);
                //     }
                //     term_newline(termAdmin);
                // } 

                // todo: check for collision avoidance

                readyForExec[args[0]] = 1;
            break;
            // Path execution
            case UNEXPECTED_POSITION:
                idx = getTrainIndex(tid, execPath);
		/*if(idx < 0){
			if(tid == collisionAvoidance){
				idx = getTrainIndex(args[2], 0); // assume train number is passed as 3rd arg
			}else{
				term_print(termAdmin, "position update from unknown tid", 32);
				break;
			}
		}*/
                readyForExec[idx] = 0;
                if (trainTargets[idx] != -1) {
                    term_print(termAdmin, "Updating pathfinding", 20);
                    term_newline(termAdmin);

		    positions[idx].nodeId = args[0];
                    positions[idx].time = Time(clockServer);

                    updatePathFindReqOnFail(&pathFindReqs[idx]);
                    sendNewPathFindReq();
                }

                // continue to update position
            case UPDATE_POSITION:
                // args[0] is sensor num
                // args[1] is distance from previous sensor
                idx = getTrainIndex(tid, execPath);
                train = trainNums[idx];
/*		
		term_print(termAdmin, "update pos of ", 14);
		term_print_int(termAdmin, train);
		term_print(termAdmin, ", id ", 5);
		term_print_int(termAdmin, idx);
*/
                execReq.nodeId = args[0];
                execReq.train = train;
                execReq.pathId = idx;

                if (readyForExec[idx]) {
                    TypedReply(tid, EXEC_PATH, (char*) &execReq, sizeof(execReq));
                } else {
                    TypedReply(tid, TRACK_POSITION, (char*) &execReq, sizeof(execReq));
                }
                
                // term_print(termAdmin, "Updating position of ", 21);
                // term_print_int(termAdmin, train);
                // term_newline(termAdmin);
                triggerTime = Time(clockServer);
                int lastSensor = positions[idx].nodeId;

                // term_print(termAdmin, "Distance ", 9);
                // term_print_int(termAdmin, args[1]);
                // term_newline(termAdmin);

                // term_print(termAdmin, "Last sensor ", 12);
                // term_print_int(termAdmin, lastSensor);
                // term_print(termAdmin, " this sensor ", 13);
                // term_print_int(termAdmin, args[0]);
                // term_newline(termAdmin);

                // determine expected time
                const struct train_data *data = getTrainData(train, lastSensor, args[0], termAdmin);
                // ensure nullptr is not dereferenced
		if(data == 0){
                	assert(termAdmin, data != 0, "No data for train! ", 19);
			term_print_int(termAdmin, train);
			term_print(termAdmin, " ", 1);
			term_print_int(termAdmin, idx);
			term_print(termAdmin, " ", 1);
			term_print_int(termAdmin, args[2]);
			term_newline(termAdmin);
		}
                if (data != 0) {
                    int resp[2];
                    TypedSend(trainAdmin, GET_SPEED, (char*) &train, 4, &type, (char*) resp, sizeof(resp));

                    // term_print(termAdmin, "Current speed ", 14);
                    // term_print_int(termAdmin, resp[0]);
                    // term_print(termAdmin, " ", 1);
                    // term_print_int(termAdmin, resp[1]);
                    // term_newline(termAdmin);

                    int velocity = data->micrometers_per_tick[resp[0]][resp[1]];
                    int ticks = args[1] * 1000 / velocity;
                    int timeDiff = triggerTime - positions[idx].time - ticks;

                    // term_print(termAdmin, "Time required ", 14);
                    // term_print_int(termAdmin, ticks);
                    // term_newline(termAdmin);

                    // term_print(termAdmin, "Time diff ", 10);
                    // term_print_int(termAdmin, timeDiff);
                    // term_newline(termAdmin);

                    add_sensor(args[0], idx, timeDiff, velocity * timeDiff / 1000);

                    updateTrainVelocity(train, lastSensor, args[0], args[1], resp[0], resp[1], timeDiff, triggerTime, termAdmin);

                    if (termCourierReady) {
                        send_to_terminal(termCourier, &termCourierReady, idx);
                    }
                }

                positions[idx].nodeId = args[0];
                positions[idx].time = triggerTime;


		// COLLISION AVOIDANCE
		collisionReq.positions[idx] = args[0];
		//collisionReq.offsets[idx] = args[1];
		collisionReq.offsets[idx] = 0;
	
		/*	
		// if the position update we got was from collision avoidance, don't need to call it again
		if(tid == collisionAvoidance){
			collisionReq.handled = 1;
			break;
		}*/
		
		if(collisionAvoidanceReady && collisionReq.num_trains > 1){
			//term_print(termAdmin, "calling CA", 10);
			//term_newline(termAdmin);
                        TypedReply(collisionAvoidance, SUCCESS, (char*) &collisionReq, sizeof(collisionReq));
                	collisionAvoidanceReady = 0;
			collisionReq.handled = 1;
		}else{
			/*
			term_print(termAdmin, "num trains: ", 11);
			term_print_int(termAdmin, collisionReq.num_trains);
			term_newline(termAdmin);
			*/
			/*
			if(collisionReq.num_trains > 1){
				term_print(termAdmin, "2 trains, c not ready", 21);
				term_newline(termAdmin);
			}*/
			collisionReq.handled = 0;
		}
            break;
	    case COLLISION_AVOIDED:
	    	idx = getTrainIndex(args[2], 0); // assume train number is passed as 3rd arg

                readyForExec[idx] = 0;
                // cancel all pathfinding targets for now
                if(trainTargets[idx] != -1){
                        updatePathFindReqOnFail(&pathFindReqs[idx]);
            		sendNewPathFindReq();
                }

		triggerTime = Time(clockServer);

	    	positions[idx].nodeId = args[0];
                positions[idx].time = triggerTime;

                // COLLISION AVOIDANCE
                collisionReq.positions[idx] = args[0];
                //collisionReq.offsets[idx] = args[1];
                collisionReq.offsets[idx] = 0;

                // disable collision avoidance for now
                collisionReq.handled = 1;

		TypedReply(tid, SUCCESS, "", 0);
	    break;
            case NO_UPDATE:
                idx = getTrainIndex(tid, execPath);

                train = trainNums[idx];

                execReq.nodeId = positions[idx].nodeId;
                execReq.train = train;
                execReq.pathId = idx;

                if (readyForExec[idx]) {
                    TypedReply(tid, EXEC_PATH, (char*) &execReq, sizeof(execReq));
                } else {
                    TypedReply(tid, TRACK_POSITION, (char*) &execReq, sizeof(execReq));
                }
            break;
            case RESERVATION_FAILED:
            idx = getTrainIndex(tid, execPath);
            term_print(termAdmin, "res failed ", 11);
            term_print_int(termAdmin, args[0]);
            term_newline(termAdmin);
            setupPathFindReq(idx, trainTargets[idx], 0, args[0]);
            sendNewPathFindReq();
            TypedReply(tid, TRACK_POSITION, (char*) &execReq, sizeof(execReq));
            break;
            case PATH_EXEC_INIT:
                idx = getTrainIndex(tid, execPath);
                execPathReady[idx] = 1;
                readyForExec[idx] = 0;
            break;
            case PATH_EXEC_COMPLETE:
                term_print(termAdmin, "Done!", 4);
                term_newline(termAdmin);
                idx = getTrainIndex(tid, execPath);
                execReq.nodeId = positions[idx].nodeId;
                execReq.train = trainNums[idx];
                readyForExec[idx] = 0;
                trainTargets[idx] = savedTrainTargets[idx];
                if (trainTargets[idx] != -1) {
                    term_print(termAdmin, "Continuing with random", 22);
                    term_newline(termAdmin);
                    savedTrainTargets[idx] = Time(clockServer) % 144;
                } else {
                    term_print(termAdmin, "Not continuing path finding", 27);
                    term_newline(termAdmin);
                    savedTrainTargets[idx] = -1;
                }
                    
                if (termCourier2Ready) {
                    int dests[3];
                    dests[0] = idx;
                    dests[1] = trainTargets[idx];
                    dests[2] = savedTrainTargets[idx];
                    TypedReply(termCourier2, UPDATE_DESTS, (char*) dests, sizeof(dests));
                    termCourier2Ready = 0;
                    targetsChanged[idx] = 0;
                } else {
                    targetsChanged[idx] = 1;
                }
                
                if (trainTargets[idx] != -1) {
                    setupPathFindReq(idx, trainTargets[idx], 0, -1);
                    sendNewPathFindReq();
                }
                TypedReply(tid, TRACK_POSITION, (char*) &execReq, sizeof(execReq));
            break;
            // TODO: collision avoidance
            // workers
            case COURIER_QUERY:
                if (tid == termCourier) {
                    termCourierReady = 1;
                    for (int i = 0; i < NUM_TRAINS; ++i) {
                        send_to_terminal(termCourier, &termCourierReady, i);
                        if (!termCourierReady)
                            break;
                    }
                } else if (tid == termCourier2) {
                    termCourier2Ready = 1;
                    for (int i = 0; i < NUM_TRAINS; ++i) {
                        if (targetsChanged[i]) {
                            int dests[3];
                            dests[0] = i;
                            dests[1] = trainTargets[i];
                            dests[2] = savedTrainTargets[i];
                            TypedReply(termCourier2, UPDATE_DESTS, (char*) dests, sizeof(dests));
                            termCourier2Ready = 0;
                            targetsChanged[i] = 0;
                            break;
                        }
                    }
                }else if (tid == sensorCourier) {
                    sensCourierReady = 1;
                    if (waitingOnReg != -1) {
                        // position of the new train
                        positions[waitingOnRegId].nodeId = args[0];
                        positions[waitingOnRegId].time = Time(clockServer);

                        add_sensor(args[0], waitingOnRegId, 0, 0);
                        send_to_terminal(termCourier, &termCourierReady, waitingOnRegId);

                        term_print(termAdmin, "Train registered", 16);
                        term_newline(termAdmin);

                        // send position to exec path to make it wait on stuff
                        assert(termAdmin, execPathReady[waitingOnRegId], "Exec path not ready", 19);
                        execReq.nodeId = args[0];
                        execReq.train = trainNums[waitingOnRegId];
                        TypedReply(execPath[waitingOnRegId], TRACK_POSITION, (char*) &execReq, sizeof(execReq));

                        term_print(termAdmin, "ExecPath started", 16);

                        // stop train so we can start pathfinding from a stationary position
                        // this means we won't run over branches we were supposed to switch
                        // also we can do shorter distances
                        int cmd[2];
                        cmd[0] = trainNums[waitingOnRegId];
                        cmd[1] = 0;
                        TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);

                        // reply to command buffer
                        // term_print_int(termAdmin, waitingOnReg);
                        // term_newline(termAdmin);
                        TypedReply(waitingOnReg, SUCCESS, (char*) &waitingOnRegId, 4);

                        waitingOnReg = -1;
                    }
                }
                
                break;
	    case CHECK_COLLISION:
		if(!collisionReq.handled && collisionReq.num_trains > 1){
			assert(termAdmin, tid == collisionAvoidance, "weird check collision", 21);
			TypedReply(tid, SUCCESS, (char*) &collisionReq, sizeof(collisionReq));
			collisionReq.handled = 1;
		}else{
			collisionAvoidanceReady = 1;
		}
		break;
            default:
                TypedReply(tid, ERROR, "", 0);
        }
    }

}
