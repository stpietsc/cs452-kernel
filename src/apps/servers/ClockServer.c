#include "../syscall.h"
#include "../io_helpers.h"
#include "../userTasks.h"
#include "../interrupts.h"
#include "../priorities.h"

static int system_ticks;

static int wait[100];

static void addDelay(int tid, int ticks) {
    if (ticks == system_ticks) {
        TypedReply(tid, SUCCESS, (char*) &system_ticks, 1);
        return;
    }
    // add to data structure
    wait[tid] = ticks;
}

static void checkDelays() {
    for (int i = 0; i < 100; ++i) {
        if (wait[i] >= 0 && wait[i] <= system_ticks) {
            TypedReply(i, SUCCESS, (char*) &system_ticks, 4);
            // reset wait
            wait[i] = -1;
        }
    }
}

void ClockServer() {
    // setup
    int success = RegisterAs("ClockServer");
    debug_assert(success == 0, "RegisterAs failed\r\n", 19);

    // create its notifier
    int tid;
    struct NotifierArgs notifierArgs;
    enum MessageType type;
    int notifier = Create(CLOCKSERVER_PRIORITY, Notifier);
    notifierArgs.eventID = TIMER_INTERRUPT; 
    notifierArgs.notificationType = CLOCK_NOTIFICATION;
    notifierArgs.shouldWaitToEnable = 0;

    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == NOTIFIER_ARGS, "Incorrect config request clock notifier\r\n", 41);
    debug_assert(tid == notifier, "Incorrect task\r\n", 16);
    TypedReply(tid, SUCCESS, (char*) &notifierArgs, sizeof(notifierArgs));

    system_ticks = 0;

    for (int i = 0; i < 100; ++i)
        wait[i] = -1;

    int msg;

    while(1) {
        // wait for request
        TypedReceive(&tid, &type, (char*) &msg, 4);

        if (type == TIME_REQUEST) {
            TypedReply(tid, SUCCESS, (char*) &system_ticks, 4);
        } else if (type == DELAY_REQUEST) {
            if (msg < 0)
                TypedReply(tid, NEGATIVE_DELAY, "", 0);
            else
                addDelay(tid, system_ticks + msg);
        } else if (type == DELAYUNTIL_REQUEST) {
            if (msg < system_ticks)
                TypedReply(tid, NEGATIVE_DELAY, "", 0);
            else
                addDelay(tid, msg);
        } else if (type == CLOCK_NOTIFICATION) {
            debug_assert(tid == notifier, "Incorrect notifier!\r\n", 21);
            TypedReply(tid, SUCCESS, "", 0);
            ++system_ticks; // increment ticks
            checkDelays();
        } else {
            TypedReply(tid, ERROR, "", 0);
        }
    }
}
