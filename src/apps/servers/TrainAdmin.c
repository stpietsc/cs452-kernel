#include "../syscall.h"
#include "../priorities.h"
#include "../userTasks.h"
#include "../io_helpers.h"
#include "../train_data.h"
#include "../path.h"
#include "../train_data.h"
#include "../velocity_helpers.h"

#define DELAY_BUFFER 10

struct PathTarget {
    int id;
    int offset;
};

#define DIST_BUFFER_MM 30

static int lastCmdTime[LARGEST_TRAIN_NUM] = { 0 };
static int prevSpeeds[LARGEST_TRAIN_NUM] = { 0 };
static int prevFromAbove[LARGEST_TRAIN_NUM] = { 0 };

static int savedTrainSpeeds[LARGEST_TRAIN_NUM] = { 0 };
static int trainSpeeds[LARGEST_TRAIN_NUM] = { 0 };
static int trainSpeedFromAbove[LARGEST_TRAIN_NUM] = { 0 };
static int trainCouriers[LARGEST_TRAIN_NUM];
static int isReversing[LARGEST_TRAIN_NUM] = { 0 };
static int hasSentDelay[LARGEST_TRAIN_NUM] = { 0 };
static int delay[LARGEST_TRAIN_NUM] = { 0 };

static int clockServer = -1;

static void setSpeed(int trainProprietor, int train, int speed) {
    int time = Time(clockServer);
    lastCmdTime[train] = time;
    prevSpeeds[train] = trainSpeeds[train];
    prevFromAbove[train] = trainSpeedFromAbove[train];
    setUpdateTime(train, time);
    trainSpeedFromAbove[train] = 0;
    if (trainSpeeds[train] > speed)
        trainSpeedFromAbove[train] = 1;
    trainSpeeds[train] = speed;
    char bytes[2];
    bytes[0] = speed;
    bytes[1] = train;
    enum MessageType type;
    TypedSend(trainProprietor, TRAIN_TRACK_COMMAND, bytes, 2, &type, "", 0);
    debug_assert(type == SUCCESS, "Sending speed command failed\r\n", 30);
}

static void sendReverse(int trainProprietor, int train) {
    char reverse[2];
    reverse[0] = 15;
    reverse[1] = train;
    enum MessageType type;
    TypedSend(trainProprietor, TRAIN_TRACK_COMMAND, reverse, 2, &type, "", 0);
    debug_assert(type == SUCCESS, "Reversing failed\r\n", 18);
}

void TrainAdmin() {
    // initialize
    RegisterAs("TrainAdmin");
    clockServer = WhoIs("ClockServer");
    int termAdmin = WhoIs("TermAdmin");
    
    init_train_data();

    for (int i = 0; i < LARGEST_TRAIN_NUM; ++i)
        trainCouriers[i] = -1;

    int trainProprietor = WhoIs("TrainProprietor");

    // set train speeds to 0 on startup
    for (int i = 0; i < LARGEST_TRAIN_NUM; ++i)
        if (get_train_index(i) != -1) {
            setSpeed(trainProprietor, i, 0);
        }

    int tid;
    
    enum MessageType type;
    int args[6];
    int trainNum;
    int isDelayUntil = 0;

    int stopOnSensorArgs[2];
    int accelOnSensorArgs[3];
    // handle requests
    while (1) {
        TypedReceive(&tid, &type, (char*) args, sizeof(args));

        switch(type) {
            case SET_SPEED:
                savedTrainSpeeds[args[0]] = args[1];
                if (!hasSentDelay[args[0]]) // currently stopping...
                    setSpeed(trainProprietor, args[0], args[1]);
                TypedReply(tid, SUCCESS, "", 0);
                break;
            case GET_SPEED:
                // args[0] is train num
                ;
		int resp[2];
                resp[0] = trainSpeeds[args[0]];
                resp[1] = trainSpeedFromAbove[args[0]];
                TypedReply(tid, SUCCESS, (char*) resp, sizeof(resp));
            break;
            case GET_LAST_CMD:
            int r[3];
            r[0] = lastCmdTime[args[0]];
            r[1] = prevSpeeds[args[0]];
            r[2] = prevFromAbove[args[0]];
            TypedReply(tid, SUCCESS, (char*) r, sizeof(r));
            break;
            case REVERSE_TRAIN:
                if (isReversing[args[0]]){
		    // already reversing
                    TypedReply(tid, ALREADY_REVERSING, "", 0);
		    break;
		}
                TypedReply(tid, SUCCESS, "", 0);
                if (savedTrainSpeeds[args[0]] == 0) {
                    // send reversing request
                    sendReverse(trainProprietor, args[0]);
                } else {
                    delay[args[0]] = ticks_to_stop(termAdmin, args[0], trainSpeeds[args[0]], trainSpeedFromAbove[args[0]]) + DELAY_BUFFER;
                    isReversing[args[0]] = 1;
                    setSpeed(trainProprietor, args[0], 0);
                    // if don't yet have a courier, set one up now
                    if (trainCouriers[args[0]] < 0) {
                        trainCouriers[args[0]] = Create(ADMIN_PRIORITY, DelayCourier);
                    } else {
                        TypedReply(trainCouriers[args[0]], SUCCESS, (char*) &delay[args[0]], 4);
                        hasSentDelay[args[0]] = 1;
                    }
                }
                
                break;
            case DELAY_QUERY:
                // which train number is this?
                trainNum = -1;
                for (int i = 0; i < LARGEST_TRAIN_NUM; ++i)
                    if (trainCouriers[i] == tid) {
                        trainNum = i;
                        break;
                    }
                if(trainNum == -1) break; // if this is not a courier we ignore it

                if (isReversing[trainNum] && !hasSentDelay[trainNum]) { // this is a new courier, send immediately
                    TypedReply(tid, SUCCESS, (char*) &delay[trainNum], 4);
                    hasSentDelay[trainNum] = 1;
                } else if (isReversing[trainNum]) {
                    // send reversing request
                    sendReverse(trainProprietor, trainNum);
                    setSpeed(trainProprietor, trainNum, savedTrainSpeeds[trainNum]);
                    // reset state
                    isReversing[trainNum] = 0;
                    hasSentDelay[trainNum] = 0;
                } else {
                    debug("Non-reversing train requested delay\r\n", 37);
                }
                break;
            case STOP_ALL_TRAINS:
                for(int i = 0; i < LARGEST_TRAIN_NUM; ++i){
                	if (savedTrainSpeeds[i] && !hasSentDelay[i]){
                        savedTrainSpeeds[i] = 0;
                        setSpeed(trainProprietor, i, 0);
                    }
                } 
                TypedReply(tid, SUCCESS, "", 0);
                break;
            case STOP_ON_SENSOR:
                Create(CLIENT_PRIORITY, stopOnSensor);
                stopOnSensorArgs[0] = args[0];
                stopOnSensorArgs[1] = args[1];
                TypedReply(tid, SUCCESS, "", 0);
                break;
            case STOP_ON_SENSOR_ARGS:
                TypedReply(tid, SUCCESS, (char*) stopOnSensorArgs, sizeof(stopOnSensorArgs));
                break;
            case ACCEL_ON_SENSOR:
                Create(CLIENT_PRIORITY, accelOnSensor);
                accelOnSensorArgs[0] = args[0];
                accelOnSensorArgs[1] = args[1];
                accelOnSensorArgs[2] = args[2];
                TypedReply(tid, SUCCESS, "", 0);
                break;
            case ACCEL_ON_SENSOR_ARGS:
                TypedReply(tid, SUCCESS, (char*) accelOnSensorArgs, sizeof(accelOnSensorArgs));
                break;
            case DELAY_ARGS:
                TypedReply(tid, SUCCESS, (char*) &isDelayUntil, 4);
                break;
	    case EMERGENCY_STOP:
		trainNum = args[0];
		sendReverse(trainProprietor, trainNum);
		//sendReverse(trainProprietor, trainNum); // better to reverse the train than have it in the same direction
		setSpeed(trainProprietor, trainNum, 0);
                TypedReply(tid, SUCCESS, "", 0);
		break;
            default:
                TypedReply(tid, ERROR, "", 0);
        }
    }
}
