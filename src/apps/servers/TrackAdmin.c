#include "../syscall.h"
#include "../priorities.h"
#include "../userTasks.h"
#include "../io_helpers.h"
#include "../track.h"
#include "../sensors.h"
#include "../train_data.h"

// switch nums: 1-18 and 153-156
// if in 1-18, subtract 1 to get index
// if in 153-156, subtract 135 to get index
#define NUM_SWITCHES 22
static int trainProprietor;
static int delayCourier;
static char switch_positions[NUM_SWITCHES];
static int switch_changed[NUM_SWITCHES];
static int switch_broken[NUM_SWITCHES] = {0};
static int dirty_bits = 0;
static int init_complete = 0;

static int get_switch_index(int switch_num){
	if (switch_num >= 1 && switch_num <= 18){
		return switch_num - 1;
	} else if (switch_num >= 153 && switch_num <= 156){
		return switch_num - 135;
	}
	return -1;
}

static int get_switch_num(int index){
	if (index >= 0 && index <= 17){
		return index + 1;
	} else if (index >= 18 && index < NUM_SWITCHES){
		return index + 135;
	}
	return -1;
}

static int get_changed_switch(int* sw, char* pos){
	//if(!dirty_bits) return 0; //TODO: fix or get rid of this
	for(int i = 0; i < NUM_SWITCHES; ++i){
		if(switch_changed[i]){
			switch_changed[i] = 0;
			*sw = get_switch_num(i);
			*pos = switch_positions[i];
			dirty_bits--;
			return 1;	
		}
	}
	return 0;
}

static void setSwitch(int sw, char pos) {
    char bytes[2];
    if(pos == 'S'){
    	bytes[0] = 0x21;
    }else if(pos == 'C'){
    	bytes[0] = 0x22;
    }else return;
    bytes[1] = (char) sw;
    enum MessageType type;
    // debug("sending ", 8);
    // debug_int(sw);
    // debug(" ", 1);
    // debug(&pos, 1);
    // debug("\r\n", 2);
    TypedSend(trainProprietor, TRAIN_TRACK_COMMAND, bytes, 2, &type, "", 0);
    debug_assert(type == SUCCESS, "Sending switch command failed\r\n", 31);

    switch_positions[get_switch_index(sw)] = pos;
    switch_changed[get_switch_index(sw)] = 1;
    dirty_bits++;
    // middle switches must be set to S/C or C/S, setting one sets the other automatically
    if(sw >= 153 && sw <= 156){
	int sw2 = (sw % 2) ? sw + 1 : sw - 1;
	char pos2 = pos == 'S' ? 'C' : 'S';
    	bytes[1] = (char) sw2;
	bytes[0] = pos2 == 'S' ? 0x21 : 0x22;
	TypedSend(trainProprietor, TRAIN_TRACK_COMMAND, bytes, 2, &type, "", 0);
	debug_assert(type == SUCCESS, "Sending switch command failed\r\n", 31);

	int idx = get_switch_index(sw2);
	if (idx >= 0) {
		switch_positions[idx] = pos2;
        switch_changed[idx] = 1;
	}
	dirty_bits++;
    }
}

static void disengage(){
    char bytes[1];
    bytes[0] = 0x20;
    enum MessageType type;
    TypedSend(trainProprietor, TRAIN_TRACK_COMMAND, bytes, 1, &type, "", 0);
    debug_assert(type == SUCCESS, "Disengage selenoid command failed\r\n", 35);
}

static void sendDelay(int delay){
	TypedReply(delayCourier, SUCCESS, (char*) &delay, 4);
}

static void findNextSensors(int node, int dist, struct SurroundSensors* sSurround, struct SurroundSensors *visited) {
	for (int i = 0; i < visited->num; ++i)
		if (node == visited->sensors[i])
			return;
	
	visited->sensors[visited->num] = node;
	visited->num++;

	int termAdmin = WhoIs("TermAdmin");
/*	
	term_print(termAdmin, "Find ", 5);
	term_print_int(termAdmin, node);
	term_newline(termAdmin);
*/
	track_node *curNode = getNode(node);
	track_node *revNode;

	while (curNode->type != NODE_SENSOR && curNode->type != NODE_EXIT) {
		if (curNode->type == NODE_BRANCH) {
			findNextSensors(curNode->edge[0].dest->id, dist + curNode->edge[0].dist, sSurround, visited);
			findNextSensors(curNode->edge[1].dest->id, dist + curNode->edge[1].dist, sSurround, visited);
			return;
		} else if (curNode->type == NODE_MERGE) {
			revNode = curNode->reverse;
			findNextSensors(revNode->edge[0].dest->id, dist + revNode->edge[0].dist, sSurround, visited);
			findNextSensors(revNode->edge[1].dest->id, dist + revNode->edge[1].dist, sSurround, visited);
			dist += curNode->edge[DIR_AHEAD].dist;
			curNode = curNode->edge[DIR_AHEAD].dest;
		} else {
			dist += curNode->edge[DIR_AHEAD].dist;
			curNode = curNode->edge[DIR_AHEAD].dest;
		}
	}
	if (curNode->type == NODE_EXIT) {
		return;
	}
	sSurround->sensors[sSurround->num] = curNode->id;
	sSurround->dists[sSurround->num] = dist;
	sSurround->num++;
}

// returns 0 on error
static int getNextSensorAndBranch(int lastSensor, int sensor[], int branch[]) {
	branch[0] = -1; // ret value if there is no branch between sensors
	int branchFound = 0;
	int idx;
	track_node *curNode = getNode(lastSensor);
	if (curNode->type == NODE_EXIT) {
		return 0;
	}
	int dist = curNode->edge[DIR_AHEAD].dist;
	curNode = curNode->edge[DIR_AHEAD].dest;

	while (curNode->type != NODE_SENSOR && curNode->type != NODE_EXIT) {
		if (curNode->type == NODE_BRANCH) {
			idx = DIR_STRAIGHT;
			if (switch_positions[get_switch_index(curNode->num)] == 'C')
				idx = DIR_CURVED;
			if (!branchFound) {
				int otherIdx = idx ? 0 : 1;
				branch[0] = curNode->edge[otherIdx].dest->id;
				branch[1] = dist + curNode->edge[otherIdx].dist;
			}
			branchFound = 1;
			dist += curNode->edge[idx].dist;
			curNode = curNode->edge[idx].dest;
		} else {
			dist += curNode->edge[DIR_AHEAD].dist;
			curNode = curNode->edge[DIR_AHEAD].dest;
		}
	}
	if (curNode->type == NODE_EXIT) {
		return 0;
	}
		
	sensor[0] = curNode->num;
	sensor[1] = dist;
	
	return 1;
}

// return 0 on error
static int getNextSensor(int lastSensor, int res[]) {
	int branch[2];
	return getNextSensorAndBranch(lastSensor, res, branch);
}

static void initialize_track(int delay, int* waitingOnDelay){
	int sw;
	for (int i = 0; i < NUM_SWITCHES; ++i){
		// middle switches must be set to S/C or C/S, setting one sets the other automatically
		if (i == 19 || i == 21) continue;
	
		sw = get_switch_num(i);
		if (!switch_broken[i])
			setSwitch(sw, 'C');
		// debug("init switch ", 12);
		// debug_int(sw);
		// debug("\r\n", 2);
	}
	sendDelay(delay);
	*waitingOnDelay = 1;
}

void TrackAdmin() {
    RegisterAs("TrackAdmin");
	
    trainProprietor = WhoIs("TrainProprietor");
    delayCourier = Create(ADMIN_PRIORITY, DelayCourier);

    int delay = 20;
    int isDelayUntil = 0;

    int tid;
    enum MessageType type;
    int args[2];
    
    // set up delay courier
    TypedReceive(&tid, &type, (char*) "", 0);
    debug_assert(type == DELAY_ARGS, "TrackAdmin setup failed\r\n", 25);
    TypedReply(tid, SUCCESS, (char*) &isDelayUntil, 4);

    TypedReceive(&tid, &type, (char*) "", 0);
    debug_assert(type == DELAY_QUERY, "TrackAdmin setup failed\r\n", 25);
 
    // set up terminal courier
    int termAdmin = WhoIs("TermAdmin");
    int termCourier = Create(CLIENT_PRIORITY_LOW, Courier);
    struct CourierArgs config;
    config.server = termAdmin;
    config.maxMsgSize = 5;
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == COURIER_ARGS, "Incorrect config request idle courier\r\n", 39);
    TypedReply(tid, SUCCESS, (char*) &config, sizeof(config));
    int courierWaiting = 0;

    int waitingOnDelay = 0;
    int shouldResetDelay = 0;
	int trackInitialized = 0;
	int waitingOnInit = -1; // currently only support one task waiting on initialization

	int lastSensor;

	int response[2];
    
    initialize_track(delay, &waitingOnDelay);
    //debug("finished track init\r\n", 16);

	struct SurroundSensors surSensors;
	struct SurroundSensors visitedLst;

    init_complete = 1;
    // handle requests
    while (1) {
        TypedReceive(&tid, &type, (char*) args, sizeof(args));
	
        switch(type) {
		case BROKEN_SWITCH:
	    case SET_SWITCH:
		;		
		int sw = args[0];
		char pos = (char) args[1];
		int i = get_switch_index(sw);
		if (switch_broken[i]) {
			TypedReply(tid, BROKEN_SWITCH, "", 0);
		} else {
			TypedReply(tid, SUCCESS, "", 0);

			// only update if there is a change
			if(switch_positions[i] != pos){
				setSwitch(sw, pos);
					// TODO: fix weird issue with switch 154 
				if(waitingOnDelay){
					shouldResetDelay = 1;
				}else{
					waitingOnDelay = 1;
					sendDelay(delay);
				}

				char msg[5];
				if (courierWaiting && get_changed_switch((int*) msg, &msg[4])) {
								TypedReply(termCourier, UPDATE_SWITCH, msg, 5);
								courierWaiting = 0;
						}
			}
			if (type == BROKEN_SWITCH) {
				// disallow future updates of this switch
				int sw2 = args[0];
				if(sw >= 153 && sw <= 156)
					sw2 = (sw % 2) ? sw + 1 : sw - 1;
				int i2 = get_switch_index(sw2);
				switch_broken[i] = 1;
				switch_broken[i2] = 1; // also set other middle switch to broken if applicable
			}
		}
		
		break;
	    case RESET_TRACK:
		TypedReply(tid, SUCCESS, "", 0);
    		initialize_track(delay, &waitingOnDelay);
		break;
	    case DELAY_QUERY:
		// if another switch command was sent, simply delay again -> max 400ms, which is ok
                //debug("got delay\r\n", 11);
		if(shouldResetDelay){
			shouldResetDelay = 0;
			sendDelay(delay);
		// otherwise, deactivate the solenoid
		}else{
			waitingOnDelay = 0;
			disengage();
		}
		break;
	    case COURIER_QUERY:
		courierWaiting = 1;
		char msg[5];
            	if (get_changed_switch((int*) msg, &msg[4])) {
                	TypedReply(termCourier, UPDATE_SWITCH, msg, 5);
                	courierWaiting = 0;
            	}

		break;
	case TRACK_INITIALIZED:
		TypedReply(tid, SUCCESS, "", 0);
		trackInitialized = 1;
		if (waitingOnInit >= 0) {
			TypedReply(waitingOnInit, SUCCESS, "", 0);
			waitingOnInit = -1;
		}
		break;
	case WAIT_ON_INIT:
		if (trackInitialized)
			TypedReply(tid, SUCCESS, "", 0); // init is done :)
		else
			waitingOnInit = tid;
		break;
	case GET_POSITION:
		// args[0] is switch number
		char res = switch_positions[get_switch_index(args[0])];
		TypedReply(tid, SUCCESS, &res, 1);
		break;
	case NEXT_SENSOR: // next sensors
		if (!trackInitialized)
			TypedReply(tid, ERROR, "", 0);
		lastSensor = args[0];
		if (getNextSensor(lastSensor, response)) {
			TypedReply(tid, SUCCESS, (char*) response, sizeof(response));
		} else {
			TypedReply(tid, ERROR, "", 0);
		}
		
		break;
	case SURROUNDING_SENSORS:
		// find all sensors that could potentially be triggered next
		lastSensor = args[0];

		/*
		term_print(termAdmin, "Surround", 8);
		term_newline(termAdmin);
		*/

		surSensors.sensors[0] = lastSensor;
		surSensors.dists[0] = 0;

		surSensors.sensors[1] = get_reverse_sensor(lastSensor);
		surSensors.dists[1] = 0;

		surSensors.num = 2;
		visitedLst.num = 0;

		track_node *node = getNode(lastSensor);
		findNextSensors(node->edge[0].dest->id, node->edge[0].dist, &surSensors, &visitedLst);
		
		node = getNode(get_reverse_sensor(lastSensor));
		findNextSensors(node->edge[0].dest->id, node->edge[0].dist, &surSensors, &visitedLst);

		// int branch[2];

		// surSensors.current = lastSensor;

		// if (getNextSensorAndBranch(lastSensor, response, branch)) {
		// 	surSensors.next[0] = response[0];
		// 	surSensors.nextDists[0] = response[1];
		// } else {
		// 	surSensors.next[0] = -1;
		// }

		// if (branch[0] != -1) {
		// 	surSensors.nextDists[1] = branch[1];
		// 	if (getNextSensor(branch[0], response)) {
		// 		// term_print(termAdmin, "Sensor after branch", 19);
		// 		// term_print_int(termAdmin, response[0]);
		// 		// term_newline(termAdmin);
		// 		surSensors.next[1] = response[0];
		// 		surSensors.nextDists[1] += response[1];
		// 	} else {
		// 		surSensors.next[1] = -1;
		// 	}
		// } else {
		// 	surSensors.next[1] = -1;
		// }

		// int reverse = get_reverse_sensor(lastSensor);
		// surSensors.reverse = reverse;

		// if (getNextSensorAndBranch(lastSensor, response, branch)) {
		// 	surSensors.prev[0] = response[0];
		// 	surSensors.prevDists[0] = response[1];
		// } else {
		// 	surSensors.prev[0] = -1;
		// }

		// if (branch[0] != -1) {
		// 	surSensors.prevDists[1] = branch[1];
		// 	if (getNextSensor(branch[0], response)) {
		// 		// term_print(termAdmin, "Sensor after branch", 19);
		// 		// term_print_int(termAdmin, response[0]);
		// 		// term_newline(termAdmin);
		// 		surSensors.prev[1] = response[0];
		// 		surSensors.prevDists[1] += response[1];
		// 	} else {
		// 		surSensors.prev[1] = -1;
		// 	}
		// } else {
		// 	surSensors.prev[1] = -1;
		// }

		TypedReply(tid, SUCCESS, (char*) &surSensors, sizeof(surSensors));
		
		break;
	    default:
                TypedReply(tid, ERROR, "", 0);
        }
    }
}
