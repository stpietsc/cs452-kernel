#include "../syscall.h"
#include "../interrupts.h"
#include "../userTasks.h"
#include "../io_helpers.h"
#include "../priorities.h"

#define MAX_MSG_SIZE 50

static void setupWorkers(int *notifier, int *relay, int *delayCourier, int uart) {
    // set up notifier
    int tid;
    enum MessageType type;
    struct NotifierArgs notifierArgs;
    *notifier = Create(UARTSERVER_PRIORITY, Notifier);
    notifierArgs.eventID = uart ? TRAIN_RECEIVE_INTERRUPT : TERMINAL_RECEIVE_INTERRUPT; 
    notifierArgs.notificationType = RECEIVE_NOTIFICATION;
    notifierArgs.shouldWaitToEnable = 1;

    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == NOTIFIER_ARGS, "Incorrect config request read notifier\r\n", 40);

    // set up relay
    struct RelayArgs relayArgs;
    *relay = Create(UARTSERVER_PRIORITY, Relay);
    relayArgs.name = uart ? "TrainReadServer" : "TermReadServer";
    relayArgs.maxMsgSize = MAX_MSG_SIZE;

    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == RELAY_ARGS, "Incorrect config request read relay\r\n", 37);

    // set up DelayCourier
    *delayCourier = Create(UARTSERVER_PRIORITY, DelayCourier);
    int isDelayUntil = 0;
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == DELAY_ARGS, "Incorrect config request read delay\r\n", 37);

    // unblock workers after all have been created so receives above are actually from new tasks
    TypedReply(*notifier, SUCCESS, (char*) &notifierArgs, sizeof(notifierArgs));
    TypedReply(*relay, SUCCESS, (char*) &relayArgs, sizeof(relayArgs));
    TypedReply(*delayCourier, SUCCESS, (char*) &isDelayUntil, sizeof(relayArgs));
}

// returns 0 if no character available
static int getCharacter(int uart, char *ch) {
	int ret = UartReadRegister(uart, UART_RHR); 
	if (ret >= 0) {
        	*ch = (char) ret;
        	return 1;
    	} 
    	return 0;
}

static void tryGetString(int *totalCharacters, int *charactersFetched, int *numDelaysBeforeTimeout, int *waitingForInterrupt, int uart, char *msg, int relay, int notifier) {
    if (*totalCharacters && !(*waitingForInterrupt)) { // there is something to be done
        for (int i = *charactersFetched; i < *totalCharacters; ++i) {
            if (getCharacter(uart, &msg[i])) {
                ++(*charactersFetched);
            } else {
                TypedReply(notifier, SUCCESS, "", 0);
                *waitingForInterrupt = 1;
                return; // STOP
            }
        }
        // wow we finished!
        TypedReply(relay, SUCCESS, msg, *charactersFetched);
        // RESET
        *totalCharacters = 0;
        *charactersFetched = 0;
        *numDelaysBeforeTimeout = 0;
    }
}

static void sendDelayRequest(int *numDelaysBeforeTimeout, int *delayCourierReady, int delayCourier, int delay) {
    TypedReply(delayCourier, DELAY_QUERY, (char*) &delay, 4);
    --(*numDelaysBeforeTimeout);
    *delayCourierReady = 0;
}

void UARTReadServer() {

    // get argument: which UART?
    struct UARTReadArgs UARTargs;
    enum MessageType type;
    int tid;
    TypedSend(MyParentTid(), UARTSERVER_ARGS, "", 0, &type, (char *) &UARTargs, sizeof(UARTargs));
    debug_assert(type == SUCCESS, "UART server could not get args\r\n", 32);

    int relay, notifier, delayCourier;
    setupWorkers(&notifier, &relay, &delayCourier, UARTargs.uart);

    // booleans for current state
    int waitingOnInterrupt = 1; // should receive a send from the notifier once initially 
    int delayCourierReady = 0;
    
    // if waiting in the middle of a getString, this saves the current state
    int numDelaysBeforeTimeout = 0;
    int totalCharacters = 0;
    int charactersFetched = 0; 

    int args[2]; 
    // needed for gets
        // args[0]: num of characters
        // args[1]: timeout

    // for holding response to getString
    char msg[MAX_MSG_SIZE];

    // handle requests
    while (1) {
        TypedReceive(&tid, &type, (char*) args, sizeof(args));

        if (type == GET_CHAR) {
            totalCharacters = 1;
            tryGetString(&totalCharacters, &charactersFetched, &numDelaysBeforeTimeout, &waitingOnInterrupt, UARTargs.uart, msg, relay, notifier);

        } else if (type == GET_STRING) {
            debug_assert(args[0] <= MAX_MSG_SIZE, "Requested too many characters\r\n", 31);
            debug_assert(args[1] > 0, "Negative timeout supplied\r\n", 27);
            totalCharacters = args[0];
            numDelaysBeforeTimeout = args[1] / UARTargs.delay;
            if (!numDelaysBeforeTimeout)
                numDelaysBeforeTimeout = 1; // always try for at least one delay
            if (delayCourierReady) { // start timing the request
                sendDelayRequest(&numDelaysBeforeTimeout, &delayCourierReady, delayCourier, UARTargs.delay);
            }
            tryGetString(&totalCharacters, &charactersFetched, &numDelaysBeforeTimeout, &waitingOnInterrupt, UARTargs.uart, msg, relay, notifier);

        } else if (type == RECEIVE_NOTIFICATION) {
            debug_assert(waitingOnInterrupt, "Receive interrupt should not be enabled\r\n", 41);
            waitingOnInterrupt = 0;
            // finish processing last request
            tryGetString(&totalCharacters, &charactersFetched, &numDelaysBeforeTimeout, &waitingOnInterrupt, UARTargs.uart, msg, relay, notifier);

        } else if (type == DELAY_QUERY) {
            debug_assert(!delayCourierReady, "Delay courier already ready\r\n", 28);
            delayCourierReady = 1;
            if (numDelaysBeforeTimeout) {
                sendDelayRequest(&numDelaysBeforeTimeout, &delayCourierReady, delayCourier, UARTargs.delay);
            } else {
                if (totalCharacters) { // we were still processing gets
                    TypedReply(relay, SUCCESS, msg, charactersFetched);
                    totalCharacters = 0;
                    charactersFetched = 0;
                }
            }

        } else {
            TypedReply(tid, ERROR, "", 0);
        }
    }
}
