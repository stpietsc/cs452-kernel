#include "../sensors.h"
#include "../userTasks.h"
#include "../priorities.h"
#include "../io_helpers.h"
#include "../syscall.h"
#include "../train_data.h"

#define NUM_SENSOR_BYTES 10

static int trackCourier;
static int termCourierReady = 0;
static int trackCourierReady = 0;
static int expectedSensor = -1;
static int registeringNewTrain = -1;

static int clockServer;
static int termAdmin;

static int awaiting_sensor[NUM_SENSORS];
static int awaiting_sensor_times[NUM_SENSORS];
static int sensor_timeouts[NUM_SENSORS];
static int last_read_time[NUM_SENSORS] = {0}; // ignore multiple triggers of the same sensors within 1 second
#define SENSOR_INTERVAL 100

static void init_awaiting_sensor(){
	for(int i = 0; i < NUM_SENSORS; ++i){
		awaiting_sensor[i] = -1;
	}
}

static void check_sensor_response(char *sensorBytes){
	int curTime = Time(clockServer);
	for(int i = 0; i < NUM_SENSOR_BYTES; ++i){
		for (int j = 0; j < 8; ++j){
			if ((sensorBytes[i] >> j) & 1){
				int sensor_num = (i * 8) + (7 - j);
                if(curTime - last_read_time[sensor_num] < SENSOR_INTERVAL) continue;
                int isAttributed = 0;
                if(awaiting_sensor[sensor_num] >= 0){
                    for (int i = 0; i < NUM_SENSORS; ++i) {
                        if (i != sensor_num && awaiting_sensor[i] == awaiting_sensor[sensor_num]) {
                            // stop waiting on all other sensors
                            awaiting_sensor[i] = -1;
                        }
                    }
                    int resp[2];
                    resp[0] = sensor_num;
                    resp[1] = curTime - awaiting_sensor_times[sensor_num];
                    TypedReply(awaiting_sensor[sensor_num], SUCCESS, (char*) resp, sizeof(resp));
                    awaiting_sensor[sensor_num] = -1;
                    isAttributed = 1;
                }
                if (!isAttributed) {
                    if (registeringNewTrain != -1) {
                        if (expectedSensor != sensor_num) {
                            assert(termAdmin, trackCourierReady, "Track courier too slow", 22);
                            TypedReply(trackCourier, NEXT_SENSOR, (char*) &sensor_num, 4);
                            add_sensor(sensor_num, -1, 0, 0); // no estimates for unattributed sensors
                        } else {
                            // Found train!
                            TypedReply(registeringNewTrain, SUCCESS, (char*) &sensor_num, 4);
                            expectedSensor = -1;
                            registeringNewTrain = -1;
                        }
                    } else {
                        add_sensor(sensor_num, -1, 0, 0); // no estimates for unattributed sensors
                    }
                }

                last_read_time[sensor_num] = curTime;
		}
		}
	}
}

void SensorAdmin() {
    RegisterAs("SensorAdmin");

    init_awaiting_sensor();

    termAdmin = WhoIs("TermAdmin");
    debug_assert(termAdmin >= 0, "WhoIs termadmin failed\r\n", 24);

    clockServer = WhoIs("ClockServer");
    debug_assert(clockServer >= 0, "WhoIs termadmin failed\r\n", 24);

    int trackAdmin = WhoIs("TrackAdmin");

    int trainProprietor = WhoIs("TrainProprietor");

    // termCourier to send information to terminal
    // send last 5 triggered sensors
    int tid;
    enum MessageType type;

    int termCourier = Create(ADMIN_PRIORITY, Courier);
    struct CourierArgs config;
    config.server = termAdmin;
    config.maxMsgSize = sizeof(struct SensorMessage); // 5 last triggered sensors
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == COURIER_ARGS, "Incorrect config request sens termCourier\r\n", 39);

    trackCourier = Create(ADMIN_PRIORITY, Courier);
    struct CourierArgs trackConfig;
    trackConfig.server = trackAdmin;
    trackConfig.maxMsgSize = 6 * 4; // 6 integers
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == COURIER_ARGS, "Incorrect config request sens courier\r\n", 39);

    int trainCourier = Create(ADMIN_PRIORITY, Courier);
    struct CourierArgs trainConfig;
    trainConfig.server = trainProprietor;
    trainConfig.maxMsgSize = 10; // sensor bytes
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == COURIER_ARGS, "Incorrect config request sens courier\r\n", 39);

    int delayCourier = Create(ADMIN_PRIORITY, DelayCourier);
    int isDelayUntil = 1;
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == DELAY_ARGS, "Incorrect config request sens delay courier\r\n", 45);
    int delayUntil = Time(clockServer) + 10;

    TypedReply(termCourier, SUCCESS, (char*) &config, sizeof(config));
    TypedReply(trackCourier, SUCCESS, (char*) &trackConfig, sizeof(trackConfig));
    TypedReply(trainCourier, SUCCESS, (char*) &trainConfig, sizeof(trainConfig));
    TypedReply(delayCourier, SUCCESS, (char*) &isDelayUntil, sizeof(isDelayUntil));

    // set reset mode
    char reset = 192;
    TypedSend(trainProprietor, TRAIN_TRACK_COMMAND, &reset, 1, &type, "", 0);
    debug_assert(type == SUCCESS, "Failed to set sensors to reset mode\r\n", 37);

    Delay(clockServer, 5); // small delay to ensure that TrackAdmin has finished setting up its workers
    // block until initialization is finished
    TypedSend(trackAdmin, WAIT_ON_INIT, "", 0, &type, "", 0);
    debug_assert(type == SUCCESS, "Failed to wait until initialization is done\r\n", 45);

    char *sensorBytes;
    int len;

    struct SensorList *lst;

    int args[sizeof(struct SensorList) / 4];
    // periodically send requests for sensor data
    while(1) {
        len = TypedReceive(&tid, &type, (char*) args, sizeof(args));

        switch(type) {
            case COURIER_QUERY:
                if (tid == termCourier) {
                    termCourierReady = 1;
                    send_to_terminal(termCourier, &termCourierReady, -1);
                } else if (tid == trackCourier) {
                    if (len) {
                        expectedSensor = args[0];
                    }
                    trackCourierReady = 1;
                } else if (tid == trainCourier) {
                    TypedReply(trainCourier, SENSOR_COMMAND, "", 0);
                    sensorBytes = (char*) args;
                    if (len == 10) {
                        check_sensor_response(sensorBytes);
                        if (termCourierReady) {
                            send_to_terminal(termCourier, &termCourierReady, -1);
                        }
                    } else {
                        // term_print(termAdmin, "Sensor timeout", 14);
                        // term_newline(termAdmin);
                    }
                }
                break;
            case AWAIT_SENSOR:
                ;
                int sensor_num = args[0];
                // TODO: should we allow multiple trains to wait on the same sensor?
                if(awaiting_sensor[sensor_num] >= 0){
                    // TODO: change this to a better error type?
                    TypedReply(tid, ERROR, "", 0);
                }
                awaiting_sensor[sensor_num] = tid; 
                awaiting_sensor_times[sensor_num] = Time(clockServer);
		sensor_timeouts[sensor_num] = 999999999; // some absurdly high timeout
                break;
            case AWAIT_SENSORS:
                lst = (struct SensorList *) args;
                int isWaiting = 0;
                for (int i = 0; i < lst->num; ++i) {
                    if (awaiting_sensor[lst->sensors[i]] == -1) {
                        // term_print(termAdmin, "Waiting on ", 11);
                        // term_print_int(termAdmin, lst->sensors[i]);
                        // term_newline(termAdmin);
                        awaiting_sensor[lst->sensors[i]] = tid;
                        awaiting_sensor_times[lst->sensors[i]] = Time(clockServer);
			sensor_timeouts[lst->sensors[i]] = lst->timeouts[i];
                        isWaiting = 1;
                    }
                }
                if (!isWaiting)
                    TypedReply(tid, ERROR, "", 0);
                break;
            case NEW_TRAIN_POS:
                registeringNewTrain = tid;
                break;
	    case DELAY_QUERY:
		TypedReply(tid, SUCCESS, (char *) &delayUntil, sizeof(delayUntil));
		for (int i = 0; i < NUM_SENSORS; ++i){
			if(awaiting_sensor[i] >= 0){
				sensor_timeouts[i] -= 10;
				if(sensor_timeouts[i] <= 0){
					int cur_tid = awaiting_sensor[i];
					int ret = -1;
					TypedReply(cur_tid, SENSOR_TIMEOUT, (char*) &ret, sizeof(ret));
					for(int j = 0; j < NUM_SENSORS; ++j){
						if(awaiting_sensor[j] == cur_tid) awaiting_sensor[j] = -1;
					}
				}
			}	
		}
		delayUntil += 10;
		break;
            default:
                TypedReply(tid, ERROR, "", 0);
        }
    }
}
