#pragma once

#include "message_types.h"

/*
 * Task creation
 */
int Create(int priority, void (*function)());
int MyTid();
int MyParentTid();
void Yield();
void Exit();

/*
 * Message passing
 */
int Send(int tid, const char *msg, int msglen, char *reply, int rplen);
int Receive(int *tid, char *msg, int msglen);
int Reply(int tid, const char *reply, int rplen);

/*
 * Typed message passing
 */
int TypedSend(int tid, enum MessageType type, const char *msg, int msglen, enum MessageType *rtype, char *reply, int rplen);
int TypedReceive(int *tid, enum MessageType *type, char *msg, int msglen);
int TypedReply(int tid, enum MessageType type, const char *reply, int rplen);

/*
 * Name server
 */
int RegisterAs(const char* name);
int WhoIs(const char* name);

/*
 * Clock server
 */
int Time(int tid);
int Delay(int tid, int ticks);
int DelayUntil(int tid, int ticks);

/*
 * Interrupts
 */
int AwaitEvent(int eventid);

/*
 * UART access
 */
int UartWriteRegister(int channel, char reg, char data);
int UartReadRegister(int channel, char reg);

static const char UART_TXLVL     = 0x08; // R
static const char UART_RXLVL     = 0x09; // R
static const char UART_RHR       = 0x00; // R
static const char UART_THR       = 0x00; // W

/*
 * UART servers
 */
int Getc(int tid);
int Gets(int tid, char *chars, int len, int timeout);
int Putc(int tid, char ch);
int Puts(int tid, char* chars, int len);

// HALT
int Halt();
