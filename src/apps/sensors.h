#include "train_data.h"

#define NUM_SENSORS 80
#define SENSOR_HISTORY_SIZE 10
#define MAX_WAIT_SENSORS 10
#define MAX_SURROUND_SENSORS 10

struct Sensor {
    char ch;
    int num;
};

struct SensorMessage {
    int num;
    struct Sensor sensors[10];
    int sensorTimeDiffs[10];
    int sensorDistances[10];
    int trainId;
};

struct SensorBuffer {
    struct Sensor buf[SENSOR_HISTORY_SIZE];
    int times[SENSOR_HISTORY_SIZE];
    int dists[SENSOR_HISTORY_SIZE];
    int write;
    int len;
    int hasChanged;
};

struct SurroundSensors {
    int sensors[MAX_SURROUND_SENSORS];
    int dists[MAX_SURROUND_SENSORS];
    int num;
};

struct SensorList {
    int sensors[MAX_WAIT_SENSORS];
    int timeouts[MAX_WAIT_SENSORS];
    int num;
};

void send_to_terminal(int termCourier, int *termCourierReady, int tId);
void add_sensor(int sensor_num, int tId, int timeDiff, int distDiff);

int get_reverse_sensor(int s);
