#include "train_data.h"
#define MAX_PATH_LEN 40
#define NUM_PATHS 2 // one for each train

#define MERGE_OFFSET 150

// these are just distance offsets, do not depend on speed
// have to fully cross merge before stopping, then travel back to the branch
#define REVERSE_DISTANCE_MERGE 2 * MERGE_OFFSET
// need to reverse before actually hitting the exit node, cuts down the travel distance
#define REVERSE_DISTANCE_EXIT -2 * MERGE_OFFSET

enum PathCommandType {
    NONE,
    SWITCH_COMMAND,
    TRAIN_COMMAND // also used for reverse
};

// path will now be a large datastructure
// save paths in static global variables
// pathmanager controls who writes to the data, ensuring only one process writes at once
struct path_node {
    int nodeId; 
    int arrivalTime;
    enum PathCommandType commandType;
    int commandArgs[2];
    int commandDelay;
    int distToNext;
    int wasExecuted;
    int isAfterReverse;
    int switchPos;
};

struct path {
    struct path_node p[MAX_PATH_LEN];
    int totalDistance;
    int length;
    enum PathCommandType beginCommand;
    int reverseAtStart;
    int commandArgs[2];
    int beginTime;
};

#define MAX_EXCLUDED 10
struct PathFinderRequest {
    int srcNode;
    int targetNode;
    int pathId;
    int excluded_nodes[MAX_EXCLUDED];
    int num_excluded;
    int num_failures;
};

struct PathPlannerRequest {
    int pathId;

    int srcOffset;
    int targetOffset;
    int beginTime;
    
    int train;
    int reverseAtStart;
};

struct ExecutePathRequest {
    int nodeId;
    int pathId;
    int train;
};


struct CollisionAvoidanceRequest {
	int handled;
	int num_trains;
	int train_ids[NUM_TRAINS];
	int positions[NUM_TRAINS];
	int offsets[NUM_TRAINS];
};


struct path_node* getPathNode(int pathID, int idx);
void setTotalDistance(int pathID, int distance);
int getTotalDistance(int pathID);
void setPathLength(int pathID, int len);
int getPathLength(int pathID);
void setBeginCommand(int pathID, enum PathCommandType type, int arg1, int arg2);
enum PathCommandType getBeginCommand(int pathID, int* arg1, int* arg2);
void setPathBeginTime(int pathID, int beginTime);
int getPathBeginTime(int pathID);
int getReverseAtStart(int pathId);
void setReverseAtStart(int pathId, int r);
