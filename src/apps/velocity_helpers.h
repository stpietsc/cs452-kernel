#pragma once

int distance_micrometers_acceleration(int termAdmin, int train, int startSpeed, int startFromAbove, int endSpeed, int startSensor, int endSensor);
int ticks_from_distance_micrometers(int termAdmin, int train, int lastCommandTime, int speedBeforeCmd, int speedBeforeFromAbove, int speedAfterCmd, int startTime, int distance, int startSensor, int endSensor);
int distance_micrometers_traveled_between(int termAdmin, int train, int lastCommandTime, int speedBeforeCmd, int speedBeforeFromAbove, int speedAfterCmd, int startSensor, int endSensor, int startTime, int endTime);
int ticks_to_stop(int termAdmin, int train, int speed, int fromAbove);