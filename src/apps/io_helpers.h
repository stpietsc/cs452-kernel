#pragma once

#include <stdint.h>
#include <stddef.h>

void int_to_str(uint64_t i, char *s);
void print_int(int tid, int i);
void print_uint64(int tid, uint64_t i);
void print_addr(int tid, uint64_t addr);
void assert(int tid, int bool, char* buf, size_t blen);
void debug(const char* buf, size_t blen);
void debug_int(int i);
void debug_assert(int bool, char* buf, size_t blen);
void debug_addr(uint64_t addr);
void term_print(int tid, char* str, int len);
void term_newline(int tid);
void term_print_int(int tid, int i);
void term_print_uint64(int tid, uint64_t i);