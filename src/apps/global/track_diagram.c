void get_diagram_info(int nodeNr, int edge, int *row, int *col, char *c) {
    switch(nodeNr) {
        case 0:
            *row = 23;
            *col = 0;
            *c = '|';
            break;
        case 1:
            *row = 25;
            *col = 0;
            *c = '|';
            break;
        case 2:
            *row = 23;
            *col = 10;
            *c = '\\';
            break;
        case 3:
            *row = 24;
            *col = 14;
            *c = '-';
            break;
        case 4:
            *row = 21;
            *col = 37;
            *c = '|';
            break;
        case 5:
            *row = 23;
            *col = 37;
            *c = '|';
            break;
        case 6:
            *row = 25;
            *col = 33;
            *c = '|';
            break;
        case 7:
            *row = 23;
            *col = 33;
            *c = '|';
            break;
        case 8:
            *row = 27;
            *col = 29;
            *c = '|';
            break;
        case 9:
            *row = 25;
            *col = 29;
            *c = '|';
            break;
        case 10:
            *row = 25;
            *col = 27;
            *c = '/';
            break;
        case 11:
            *row = 27;
            *col = 25;
            *c = '|';
            break;
        case 12:
            *row = 25;
            *col = 4;
            *c = '|';
            break;
        case 13:
            *row = 27;
            *col = 4;
            *c = '|';
            break;
        case 14:
            *row = 27;
            *col = 8;
            *c = '|';
            break;
        case 15:
            *row = 25;
            *col = 6;
            *c = '\\';
            break;
        case 16:
            *row = 15;
            *col = 29;
            *c = '|';
            break;
        case 17:
            *row = 17;
            *col = 29;
            *c = '|';
            break;
        case 18:
            *row = 15;
            *col = 23;
            *c = '\\';
            break;
        case 19:
            *row = 17;
            *col = 27;
            *c = '\\';
            break;
        case 20:
            *row = 15;
            *col = 4;
            *c = '|';
            break;
        case 21:
            *row = 17;
            *col = 4;
            *c = '|';
            break;
        case 22:
            *row = 27;
            *col = 29;
            *c = '|';
            break;
        case 23:
            *row = 29;
            *col = 29;
            *c = '|';
            break;
        case 24:
            *row = 23;
            *col = 37;
            *c = '|';
            break;
        case 25:
            *row = 25;
            *col = 37;
            *c = '|';
            break;
        case 26:
            *row = 25;
            *col = 33;
            *c = '|';
            break;
        case 27:
            *row = 27;
            *col = 33;
            *c = '|';
            break;
        case 28:
            *row = 11;
            *col = 19;
            *c = '/';
            break;
        case 29:
            *row = 9;
            *col = 23;
            *c = '/';
            break;
        case 30:
            *row = 24;
            *col = 19;
            *c = '-';
            break;
        case 31:
            *row = 23;
            *col = 23;
            *c = '/';
            break;
        case 32:
            *row = 15;
            *col = 23;
            *c = '\\';
            break;
        case 33:
            *row = 13;
            *col = 19;
            *c = '\\';
            break;
        case 34:
            *row = 5;
            *col = 37;
            *c = '|';
            break;
        case 35:
            *row = 7;
            *col = 37;
            *c = '|';
            break;
        case 36:
            *row = 19;
            *col = 33;
            *c = '|';
            break;
        case 37:
            *row = 21;
            *col = 31;
            *c = '/';
            break;
        case 38:
            *row = 17;
            *col = 37;
            *c = '|';
            break;
        case 39:
            *row = 19;
            *col = 37;
            *c = '|';
            break;
        case 40:
            *row = 21;
            *col = 27;
            *c = '/';
            break;
        case 41:
            *row = 19;
            *col = 29;
            *c = '|';
            break;
        case 42:
            *row = 19;
            *col = 4;
            *c = '|';
            break;
        case 43:
            *row = 21;
            *col = 6;
            *c = '\\';
            break;
        case 44:
            *row = 15;
            *col = 0;
            *c = '|';
            break;
        case 45:
            *row = 17;
            *col = 0;
            *c = '|';
            break;
        case 46:
            *row = 15;
            *col = 33;
            *c = '|';
            break;
        case 47:
            *row = 17;
            *col = 33;
            *c = '|';
            break;
        case 48:
            *row = 11;
            *col = 14;
            *c = '\\';
            break;
        case 49:
            *row = 9;
            *col = 10;
            *c = '\\';
            break;
        case 50:
            *row = 7;
            *col = 4;
            *c = '|';
            break;
        case 51:
            *row = 9;
            *col = 4;
            *c = '|';
            break;
        case 52:
            *row = 3;
            *col = 6;
            *c = '/';
            break;
        case 53:
            *row = 1;
            *col = 10;
            *c = '/';
            break;
        case 54:
            *row = 1;
            *col = 5;
            *c = '/';
            break;
        case 55:
            *row = 3;
            *col = 1;
            *c = '/';
            break;
        case 56:
            *row = 3;
            *col = 32;
            *c = '\\';
            break;
        case 57:
            *row = 1;
            *col = 28;
            *c = '\\';
            break;
        case 58:
            *row = 9;
            *col = 33;
            *c = '|';
            break;
        case 59:
            *row = 7;
            *col = 33;
            *c = '|';
            break;
        case 60:
            *row = 9;
            *col = 29;
            *c = '|';
            break;
        case 61:
            *row = 7;
            *col = 29;
            *c = '|';
            break;
        case 62:
            *row = 9;
            *col = 23;
            *c = '/';
            break;
        case 63:
            *row = 7;
            *col = 27;
            *c = '/';
            break;
        case 64:
            *row = 13;
            *col = 14;
            *c = '/';
            break;
        case 65:
            *row = 15;
            *col = 10;
            *c = '/';
            break;
        case 66:
            *row = 9;
            *col = 10;
            *c ='\\';
            break;
        case 67:
            *row = 7;
            *col = 6;
            *c = '\\';
            break;
        case 68:
            *row = 3;
            *col = 6;
            *c = '/';
            break;
        case 69:
            *row = 5;
            *col = 4;
            *c = '|';
            break;
        case 70:
            *row = 7;
            *col = 0;
            *c = '|';
            break;
        case 71:
            *row = 9;
            *col = 0;
            *c = '|';
            break;
        case 72:
            *row = 1;
            *col = 23;
            *c = '\\';
            break;
        case 73:
            *row = 3;
            *col = 27;
            *c = '\\';
            break;
        case 74:
            *row = 3;
            *col = 32;
            *c = '\\';
            break;
        case 75:
            *row = 5;
            *col = 33;
            *c = '|';
            break;
        case 76:
            *row = 5;
            *col = 29;
            *c = '|';
            break;
        case 77:
            *row = 3;
            *col = 27;
            *c = '\\';
            break;
        case 78:
            *row = 17;
            *col = 6;
            *c = '/';
            break;
        case 79:
            *row = 15;
            *col = 10;
            *c = '/';
            break;
        case 80: // BR1
            *row = 25;
            if (edge) {
                *col = 29;
                *c = '|';
            } else {
                *col = 27;
                *c = '/';
            }
            break;
        case 81:
            *row = 23;
            *col = 31;
            *c = '/';
            break;
        case 82: // BR 2
            *row = 23;
            if (edge) {
                *col = 33;
                *c = '|';
            } else {
                *col = 31;
                *c = '/';
            }
            break;
        case 83:
            *row = 21;
            *col = 35;
            *c = '/';
            break;
        case 84: // BR 3
            *row = 21;
            if (edge) {
                *col = 35;
                *c = '/';
            } else {
                *col = 37;
                *c = '|';
            }
            break;
        case 85:
            *row = 19;
            *col = 37;
            *c = '|';
            break;
        case 86: // BR 4
            *row = 25;
            if (edge) {
                *col = 4;
                *c = '|';
            } else {
                *col = 6;
                *c = '\\';
            }
            break;
        case 87:
            *row = 23;
            *col = 2;
            *c = '\\';
            break;
        case 88: // BR 5
            *row = 7;
            if (edge) {
                *col = 35;
                *c = '\\';
            } else {
                *col = 37;
                *c = '|';
            }
            break;
        case 89:
            *row = 9;
            *col = 37;
            *c = '|';
            break;
        case 90: // BR 6
            *row = 17;
            if (edge) {
                *col = 35;
                *c = '/';
            } else {
                *col = 33;
                *c = '|';
            }
            break;
        case 91:
            *row = 19;
            *col = 33;
            *c = '|';
            break;
        case 92: // BR 7
            *row = 7;
            if (edge) {
                *col = 35;
                *c = '\\';
            } else {
                *col = 33;
                *c = '|';
            }
            break;
        case 93:
            *row = 5;
            *col = 33;
            *c = '|';
            break;
        case 94:
            *col = 23;
            if (edge) {
                *row = 1;
                *c = '\\';
            } else {
                *row = 0;
                *c = '-';
            }
            break;
        case 95:
            *row = 0;
            *col = 19;
            *c = '-';
            break;
        case 96: // BR 9
            *col = 10;
            if (edge) {
                *row = 1;
                *c = '/';
            } else {
                *row = 0;
                *c = '-';
            }
            break;
        case 97:
            *row = 0;
            *col = 14;
            *c = '-';
            break;
        case 98: // BR 10
            *row = 7;
            if (edge) {
                *col = 6;
                *c = '\\';
            } else {
                *col = 4;
                *c = '|';
            }
            break;
        case 99:
            *row = 5;
            *col = 4;
            *c = '|';
            break;
        case 100:
            *row = 21;
            if (edge) {
                *col = 2;
                *c = '\\';
            } else {
                *col = 0;
                *c = '|';
            }
            break;
        case 101:
            *row = 19;
            *col = 0;
            *c = '|';
            break;
        case 102: // BR 12
            *row = 23;
            if (edge) {
                *col = 2;
                *c = '\\';
            } else {
                *col = 0;
                *c = '|';
            }
            break;
        case 103:
            *row = 21;
            *col = 0;
            *c = '|';
            break;
        case 104: // BR 13
            *row = 17;
            if (edge) {
                *col = 6;
                *c = '/';
            } else {
                *col = 4;
                *c = '|';
            }
            break;
        case 105:
            *row = 19;
            *col = 4;
            *c = '|';
            break;
        case 106: // BR 14
            *col = 6;
            if (edge) {
                *row = 21;
                *c = '\\';
            } else {
                *row = 22;
                *c = '-';
            }
            break;
        case 107:
            *row = 23;
            *col = 10;
            *c = '\\';
            break;
        case 108: // BR 15
            *col = 27;
            if (edge) {
                *row = 21;
                *c = '/';
            } else {
                *row = 22;
                *c = '-';
            }
            break;
        case 109:
            *row = 23;
            *col = 23;
            *c = '/';
            break;
        case 110: // BR 16
            *row = 17;
            if (edge) {
                *col = 27;
                *c = '\\';
            } else {
                *col = 29;
                *c = '|';
            }
            break;
        case 111:
            *row = 19;
            *col = 29;
            *c = '|';
            break;
        case 112: // BR 17
            *row = 7;
            if (edge) {
                *col = 27;
                *c = '/';
            } else {
                *col = 29;
                *c = '|';
            }
            break;
        case 113:
            *row = 5;
            *col = 29;
            *c = '|';
            break;
        case 114:
            *row = 17;
            if (edge) {
                *col = 35;
                *c = '/';
            } else {
                *col = 37;
                *c = '|';
            }
            break;
        case 115:
            *row = 15;
            *col = 37;
            *c = '|';
            break;
        case 116:
            if (!edge)
                break; // don't display this
            *row = 11;
            *col = 19;
            *c = '/';
            break;
        case 117:
            *row = 12;
            *col = 17;
            *c = '-';
            break;
        case 118: 
            if (!edge)
                break;
            *row = 11;
            *col = 14;
            *c = '\\';
            break;
        case 119:
            *row = 12;
            *col = 16;
            *c = '-';
            break;
        case 120:
            if (!edge)
                break;
            *row = 13;
            *col = 14;
            *c = '/';
            break;
        case 121:
            *row = 12;
            *col = 16;
            *c = '-';
            break;
        case 122:
            if (!edge)
                break;
            *row = 13;
            *col = 19;
            *c = '\\';
            break;
        case 123:
            *row = 12;
            *col = 17;
            *c = '-';
            break;
        case 128: // EN3
            *row = 5;
            *col = 37;
            *c = '|';
            break;
        case 130: // EN4
            *row = 27;
            *col = 4;
            *c = '|';
            break;
        case 132: // EN5
            *row = 25;
            *col = 0;
            *c = '|';
            break;
        case 134: // EN6
            *row = 27;
            *col = 8;
            *c = '|';
            break;
        case 136: // EN7
            *row = 29;
            *col = 29;
            *c = '|';
            break;
        case 138: // EN8
            *row = 27;
            *col = 25;
            *c = '|';
            break;
        case 140: // EN9
            *row = 25;
            *col = 37;
            *c = '|';
            break;
        case 142: // EN10
            *row = 27;
            *col = 33;
            *c = '|';
            break;
    }
}