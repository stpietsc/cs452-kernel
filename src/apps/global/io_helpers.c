#ifdef DEBUG
#include "../../debug/terminal.h"
#endif
#include <stdint.h>
#include <stddef.h>
#include "../syscall.h"

static int num_digits(uint64_t i) {
  if (!i)
    return 1;
  int count = 0;
  while (i) {
    i /= 10;
    ++count;
  }
  return count;
}

void int_to_str(uint64_t i, char *s) {
  int n = num_digits(i);
  int j;
  for (j = n - 1; j >= 0; --j, i /= 10) {
    s[j] = i % 10 + '0';
  }
}

void print_uint64(int tid, uint64_t i);

void print_int(int tid, int i) {
  if(i < 0){
  	Putc(tid, '-');
	i = -i;
  }
  print_uint64(tid, (uint64_t) i);
}

void print_uint64(int tid, uint64_t i){
  int n = num_digits(i);
  int j;
  char s[n];
  for (j = n - 1; j >= 0; --j, i /= 10) {
    s[j] = i % 10 + '0';
  }
  Puts(tid, s, n);
}

void print_addr(int tid, uint64_t addr) {
  char s[] = "0x00000000\r\n";
  int idx = 9;
  while (addr) {
    int digit = addr % 16;
    addr /= 16;
    if (digit < 10)
      s[idx] = digit + '0';
    else
      s[idx] = digit + '7';
    --idx;
  }
  Puts(tid, s, 12);
}

void term_print(int tid, char* str, int len) {
  enum MessageType type;
  TypedSend(tid, DEBUG, str, len, &type, "", 0);
}

void term_newline(int tid) {
  enum MessageType type;
  TypedSend(tid, DEBUG_NEWLINE, "", 0, &type, "", 0);
}

void assert(int tid, int bool, char* buf, size_t blen) {
  if (!bool) {
    term_print(tid, buf, blen);
    term_newline(tid);
  }
}

void term_print_uint64(int tid, uint64_t i);

void term_print_int(int tid, int i) {
  if(i < 0){
  	term_print(tid, "-", 1);
	i = -i;
  }
  term_print_uint64(tid, (uint64_t) i);
}

void term_print_uint64(int tid, uint64_t i) {
  int n = num_digits(i);
  int j;
  char s[n];
  for (j = n - 1; j >= 0; --j, i /= 10) {
    s[j] = i % 10 + '0';
  }
  term_print(tid, s, n);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void debug(const char* buf, size_t blen) {
  #ifdef DEBUG
  puts(buf, blen);
  #endif
}

void debug_uint64(uint64_t);

void debug_int(int i) {
  #ifdef DEBUG
  if(i < 0){
  	putc('-');
	i = -i;
  }
  debug_uint64((uint64_t) i);
  #endif
}

void debug_uint64(uint64_t i){
  #ifdef DEBUG
  int n = num_digits(i);
  int j;
  char s[n];
  for (j = n - 1; j >= 0; --j, i /= 10) {
    s[j] = i % 10 + '0';
  }
  puts(s, n);
  #endif
}

void debug_addr(uint64_t addr) {
  #ifdef DEBUG
  char s[] = "0x00000000\r\n";
  int idx = 9;
  while (addr) {
    int digit = addr % 16;
    addr /= 16;
    if (digit < 10)
      s[idx] = digit + '0';
    else
      s[idx] = digit + '7';
    --idx;
  }
  puts(s, 12);
  #endif
}

void debug_assert(int bool, char* buf, size_t blen) {
  #ifdef DEBUG
  if (!bool) {
    puts(buf, blen);
  }
  #endif
}
#pragma GCC diagnostic pop