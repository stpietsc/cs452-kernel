#include "../path.h"

static struct path paths[NUM_PATHS];

struct path_node* getPathNode(int pathID, int idx) {
    return &paths[pathID].p[idx];
}

void setTotalDistance(int pathID, int distance) {
    paths[pathID].totalDistance = distance;
}

int getTotalDistance(int pathID) {
    return paths[pathID].totalDistance;
}

void setPathLength(int pathID, int len) {
    paths[pathID].length = len;
}

int getPathLength(int pathID) {
    return paths[pathID].length;
}

void setBeginCommand(int pathID, enum PathCommandType type, int arg1, int arg2) {
    paths[pathID].beginCommand = type;
    paths[pathID].commandArgs[0] = arg1;
    paths[pathID].commandArgs[1] = arg2;
}

enum PathCommandType getBeginCommand(int pathID, int* arg1, int* arg2) {
    *arg1 = paths[pathID].commandArgs[0];
    *arg2 = paths[pathID].commandArgs[1];
    return paths[pathID].beginCommand;
}

void setPathBeginTime(int pathID, int beginTime) {
    paths[pathID].beginTime = beginTime;
}

int getPathBeginTime(int pathID) {
    return paths[pathID].beginTime;
}
int getReverseAtStart(int pathId) { return paths[pathId].reverseAtStart; }
void setReverseAtStart(int pathId, int r){
	paths[pathId].reverseAtStart = r;
}
