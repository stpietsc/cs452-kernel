#include "../sensors.h"
#include "../syscall.h"

static struct SensorBuffer buffer[NUM_TRAINS + 1];

static void writeBuffer(int tId, struct Sensor sens, int timeDiff, int distDiff) {
    if (tId == -1) {
        tId = NUM_TRAINS;
    }
    buffer[tId].buf[buffer[tId].write] = sens;
    buffer[tId].times[buffer[tId].write] = timeDiff;
    buffer[tId].dists[buffer[tId].write] = distDiff;
	buffer[tId].write++;
    buffer[tId].hasChanged = 1;
    if (buffer[tId].len < SENSOR_HISTORY_SIZE) buffer[tId].len++;
	if (buffer[tId].write == SENSOR_HISTORY_SIZE) buffer[tId].write = 0;
}

// returns number of sensors copied
static int getFromBuffer(int tId, struct SensorMessage *msg, int num) {
    if (tId == -1)
        tId = NUM_TRAINS;
    struct Sensor *arr = msg->sensors;
	if (num > buffer[tId].len)
        num = buffer[tId].len;
    int curIdx = buffer[tId].write - 1;
    for (int i = 0; i < num; ++i) {
        if (curIdx == -1)
            curIdx = SENSOR_HISTORY_SIZE - 1;
        arr[i] = buffer[tId].buf[curIdx];
        msg->sensorTimeDiffs[i] = buffer[tId].times[curIdx];
        msg->sensorDistances[i] = buffer[tId].dists[curIdx];
        curIdx--;
    }
	return num;
}

void send_to_terminal(int termCourier, int *termCourierReady, int tId) {
    if (tId == -1)
        tId = NUM_TRAINS;
    if (!(*termCourierReady) || !buffer[tId].hasChanged) // cannot send anything or have nothing new to send
        return;
    struct SensorMessage msg;
    msg.num = getFromBuffer(tId, &msg, 10);
    msg.trainId = tId;
    TypedReply(termCourier, SENSOR_TRIGGERED, (char*) &msg, sizeof(msg));
    *termCourierReady = 0;
    buffer[tId].hasChanged = 0;
}

void add_sensor(int sensor_num, int tId, int timeDiff, int distDiff){
    if (tId == -1)
        tId = NUM_TRAINS;
    int track_num = sensor_num / 16;
	int track_sensor_num = sensor_num % 16 + 1;
    
    struct Sensor s;
	s.ch = track_num + 'A';
	s.num = track_sensor_num;

    writeBuffer(tId, s, timeDiff, distDiff);
}

int get_reverse_sensor(int s){
        return s + ((s % 2 == 0) ? 1 : -1);
}

