#include "../track.h"

static struct set_switch broken_switches[NUM_BROKEN_SWITCHES];

void init_broken_switches() {
    // KEEP THIS UPDATED
    
    // TRACK A - 156
    broken_switches[0].track = TRACK_A;
    broken_switches[0].track_id = 122;
    broken_switches[0].num = 156;
    broken_switches[0].posn = 'C';

    // // TRACK A - 13
    // broken_switches[6].track = TRACK_A;
    // broken_switches[6].track_id = 104;
    // broken_switches[6].num = 13;
    // broken_switches[6].posn = 'S';

    // TRACK B - 5
    broken_switches[1].track = TRACK_B;
    broken_switches[1].track_id = 88;
    broken_switches[1].num = 5;
    broken_switches[1].posn = 'S';

    // TRACK B - 155
    // broken_switches[2].track = TRACK_B;
    // broken_switches[2].track_id = 120;
    // broken_switches[2].num = 155;
    // broken_switches[2].posn = 'C';

    // // TRACK B - 156
    // broken_switches[3].track = TRACK_B;
    // broken_switches[3].track_id = 122;
    // broken_switches[3].num = 156;
    // broken_switches[3].posn = 'S';
    
    // // TRACK B - 4
    // broken_switches[4].track = TRACK_B;
    // broken_switches[4].track_id = 86;
    // broken_switches[4].num = 4;
    // broken_switches[4].posn = 'S';
    
    // // TRACK B - 7
    // broken_switches[5].track = TRACK_B;
    // broken_switches[5].track_id = 92;
    // broken_switches[5].num = 7;
    // broken_switches[5].posn = 'S';
}

struct set_switch* getBrokenSwitch(int idx) {
    return &broken_switches[idx];
}
