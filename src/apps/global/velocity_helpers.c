#include "stdint.h"
#include "../train_data.h"
#include "../io_helpers.h"

// isqrt implementation taken from https://stackoverflow.com/questions/21657491/an-efficient-algorithm-to-calculate-the-integer-square-root-isqrt-of-arbitrari

static uint32_t isqrt_impl(
    uint64_t const n,
    uint64_t const xk)
{
    uint64_t const xk1 = (xk + n / xk) / 2;
    return (xk1 >= xk) ? xk : isqrt_impl(n, xk1);
}

static uint32_t isqrt(uint64_t const n)
{
    if (n == 0) return 0;
    if (n == 18446744073709551615ULL) return 4294967295U;
    return isqrt_impl(n, n);
}

// Helper function for determining distance required for acceleration
int distance_micrometers_acceleration(int termAdmin, int train, int startSpeed, int startFromAbove, int endSpeed, int startSensor, int endSensor) {
    if (startSpeed == endSpeed)
        return 0;
    const struct train_data *data = getTrainData(train, startSensor, endSensor, termAdmin);
    assert(termAdmin, data != 0, "No train data", 13);
    int acceleration = data->acceleration[startSpeed][endSpeed][startFromAbove];
    // term_print(termAdmin, "Acceleration ", 13);
    // term_print_int(termAdmin, acceleration);
    int startVelocity = data->micrometers_per_tick[startSpeed][startFromAbove];
    // term_print(termAdmin, " start vel ", 11);
    // term_print_int(termAdmin, startVelocity);

    int endFromAbove = startSpeed > endSpeed;
    int endVelocity = data->micrometers_per_tick[endSpeed][endFromAbove];

    // term_print(termAdmin, " end vel ", 9);
    // term_print_int(termAdmin, endVelocity);
    // term_newline(termAdmin);

    // accel = (endVel - startVel) / ticks
    // ticks = (endVel - startVel) / accel
    // dist = avgVel * ticks = (endVel + startVel) / 2 * (endVel - startVel) / accel
    return (endVelocity * endVelocity - startVelocity * startVelocity) / (2 * acceleration);
}

static int ticks_acceleration(int velocityBefore, int velocityAfter, int accel) {
    return (velocityAfter - velocityBefore) / accel;
}

// ticks = (endVel - startVel) / accel
int ticks_to_stop(int termAdmin, int train, int speed, int fromAbove) {
    const struct train_data *data = getTrainData(train, 0, 0, termAdmin);
    assert(termAdmin, data != 0, "No train data", 13);
    int acceleration = data->acceleration[speed][0][fromAbove];
    // term_print(termAdmin, "Acceleration ", 13);
    // term_print_int(termAdmin, acceleration);
    int startVelocity = data->micrometers_per_tick[speed][fromAbove];

    return - startVelocity / acceleration;
}

// Helper function for determining time required between nodes given last train command
// requires information about last train command (real or hypothetical)
int ticks_from_distance_micrometers(int termAdmin, int train, int lastCommandTime, int speedBeforeCmd, int speedBeforeFromAbove, int speedAfterCmd, int startTime, int distance, int startSensor, int endSensor) {
    if (distance == 0)
        return 0;
    const struct train_data *data = getTrainData(train, startSensor, endSensor, termAdmin);
    assert(termAdmin, data != 0, "No train data", 13);
    int acceleration = data->acceleration[speedBeforeCmd][speedAfterCmd][speedBeforeFromAbove];
    // term_print(termAdmin, "Acceleration ", 13);
    // term_print_int(termAdmin, acceleration);
    int startVelocity = data->micrometers_per_tick[speedBeforeCmd][speedBeforeFromAbove];
    // term_print(termAdmin, " start vel ", 11);
    // term_print_int(termAdmin, startVelocity);

    int endFromAbove = speedBeforeCmd > speedAfterCmd;
    int endVelocity = data->micrometers_per_tick[speedAfterCmd][endFromAbove];
    // term_print(termAdmin, " end vel ", 9);
    // term_print_int(termAdmin, endVelocity);

    int accelTicks = ticks_acceleration(startVelocity, endVelocity, acceleration);
    // term_print(termAdmin, " acc ticks ", 11);
    // term_print_int(termAdmin, accelTicks);
    // term_newline(termAdmin);
    if (startTime - lastCommandTime >= accelTicks) {
        // last command is irrelevant, we have reached end velocity before the startTime
        // v = d / t -> t = d / v
        return distance / endVelocity;
    }

    // we are still accelerating
    int accelTicksLeft = accelTicks - (startTime - lastCommandTime);

    // term_print(termAdmin, "Acc ticks left ", 15);
    // term_print_int(termAdmin, accelTicksLeft);

    int accelTicksDone = accelTicks - accelTicksLeft;
    // vel = accel * ticks + startVel
    int velAtStartTime = startVelocity + acceleration * accelTicksDone;

    // term_print(termAdmin, " V starttime ", 13);
    // term_print_int(termAdmin, velAtStartTime);

    // dist = avgVel * ticks = (endVel + startVel) / 2 * (endVel - startVel) / accel
    int remainingAccelDist = (endVelocity * endVelocity - velAtStartTime * velAtStartTime) / (2 * acceleration);

    // term_print(termAdmin, " Remaining dist ", 16);
    // term_print_int(termAdmin, remainingAccelDist);

    // term_print(termAdmin, " dist ", 6);
    // term_print_int(termAdmin, distance);
    // term_newline(termAdmin);

    if (remainingAccelDist > distance) {
        // we are going to be accelerating throughout whole segment
        // ticks = (sqrt(velStartTime^2 + 2 * accel * dist) - velStartTime) / accel
        uint64_t inp = velAtStartTime * velAtStartTime + 2 * acceleration * distance;
        // term_print(termAdmin, "Isqrt ", 6);
        // term_print_int(termAdmin, inp);
        int sqrt = isqrt(inp);
        // term_print(termAdmin, " = ", 3);
        // term_print_int(termAdmin, sqrt);
        int res = (sqrt - velAtStartTime) / acceleration;
        // term_print(termAdmin, " Result ", 8);
        // term_print_int(termAdmin, res);
        // term_newline(termAdmin);
        return res;
    } else {
        int constantVelDist = distance - remainingAccelDist;
        if (endVelocity == 0) {
            // just assume where we stop is good enough, we won't be going further
            // term_print(termAdmin, "Missing distance ", 17);
            // term_print_int(termAdmin, constantVelDist);
            // term_newline(termAdmin);
            return accelTicksLeft;
        }
        // going to be done accelerating before going through whole segment
        int remainingTicks = constantVelDist / endVelocity;
        return remainingTicks + accelTicksLeft;
    }
}

// Helper function for determining distance between two points in time
int distance_micrometers_traveled_between(int termAdmin, int train, int lastCommandTime, int speedBeforeCmd, int speedBeforeFromAbove, int speedAfterCmd, int startSensor, int endSensor, int startTime, int endTime) {
    if (startTime == endTime)
        return 0;
    const struct train_data *data = getTrainData(train, startSensor, endSensor, termAdmin);
    int acceleration = data->acceleration[speedBeforeCmd][speedAfterCmd][speedBeforeFromAbove];
    int startVelocity = data->micrometers_per_tick[speedBeforeCmd][speedBeforeFromAbove];

    int endFromAbove = speedBeforeCmd > speedAfterCmd;
    int endVelocity = data->micrometers_per_tick[speedAfterCmd][endFromAbove];

    int accelTicks = ticks_acceleration(startVelocity, endVelocity, acceleration);
    if (startTime - lastCommandTime > accelTicks) {
        // last command is irrelevant, we have reached end velocity before the startTime
        // v = d / t -> d = v * t
        return endVelocity * (endTime - startTime);
    }

    // we are still accelerating
    int accelTicksLeft = accelTicks - (startTime - lastCommandTime);
    int accelTicksDone = accelTicks - accelTicksLeft;
    // vel = accel * ticks + startVel
    int velAtStartTime = startVelocity + acceleration * accelTicksDone;

    if (accelTicksLeft > (endTime - startTime)) {
        // accelerating throughout whole time period
        int velAtEndTime = velAtStartTime + acceleration * (endTime - startTime);
        return (velAtStartTime + velAtEndTime) * (endTime - startTime) / 2;
    } else {
        // going to be done accelerating before doing through whole time period
        int distWhileAccel = (velAtStartTime + endVelocity) * accelTicksLeft / 2;
        int ticksLeft = endTime - startTime - accelTicksLeft;
        int remainingDist = endVelocity * ticksLeft;
        return distWhileAccel + remainingDist;
    }

}