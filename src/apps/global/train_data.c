#include "../train_data.h"
#include "../sensors.h"
#include "../io_helpers.h"

static struct train_data train_data[NUM_TRAINS][NUM_SENSORS][NUM_SENSORS] = {0};
#ifdef CALIBRATE
#include "../syscall.h"
#include "../../debug/terminal.h"
void print_velocity_c_code(int train, int s1, int s2, int speed, int velocity, int fromAbove){
        // train_data[3][70][80].micrometers_per_tick[SPEED_HIGH] = 5222;
        char msg1[] = "train_data[";
        char msg2[] = "][";
        char msg3[] = "][";
        char msg4[] = "].micrometers_per_tick[";
        char msg5[] = "][";
        char msg6[] = "] = ";
        char msg7[] = ";\r\n";

        puts(msg1, sizeof(msg1)-1);
        blocking_print_int(train);
        puts(msg2, sizeof(msg2)-1);
        blocking_print_int(s1);
        puts(msg3, sizeof(msg3)-1);
        blocking_print_int(s2);
        puts(msg4, sizeof(msg4)-1);
        blocking_print_int(speed);
        puts(msg5, sizeof(msg5)-1);
        blocking_print_int(fromAbove);
        puts(msg6, sizeof(msg6)-1);
	blocking_print_int(velocity);
        puts(msg7, sizeof(msg7)-1);
}

void print_avg_velocity_c_code(int train, int speed, int velocity, int fromAbove){
        char msg1[] = "avg_train_data[";
        char msg4[] = "].micrometers_per_tick[";
	char msg5[] = "][";
        char msg6[] = "] = ";
        char msg7[] = ";\r\n";

        puts(msg1, sizeof(msg1)-1);
        blocking_print_int(train);
        puts(msg4, sizeof(msg4)-1);
        blocking_print_int(speed);
	puts(msg5, sizeof(msg5)-1);
	blocking_print_int(fromAbove);
        puts(msg6, sizeof(msg6)-1);
        blocking_print_int(velocity);
        puts(msg7, sizeof(msg7)-1);
}

void print_accel_c_code(int train, int s1, int s2, int start_speed, int end_speed, int fromAbove, int acceleration){
        // train_data[3][70][80].micrometers_per_tick[SPEED_HIGH] = 5222;
        char msg1[] = "train_data[";
        char msg2[] = "][";
        char msg3[] = "][";
        char msg4[] = "].acceleration[";
        char msg5[] = "][";
        char msg6[] = "][";
        char msg7[] = "] = ";
        char msg8[] = ";\r\n";

        puts(msg1, sizeof(msg1)-1);
        blocking_print_int(train);
        puts(msg2, sizeof(msg2)-1);
        blocking_print_int(s1);
        puts(msg3, sizeof(msg3)-1);
        blocking_print_int(s2);
        puts(msg4, sizeof(msg4)-1);
        blocking_print_int(start_speed);
        puts(msg5, sizeof(msg5)-1);
        blocking_print_int(end_speed);
        puts(msg6, sizeof(msg6)-1);
        blocking_print_int(fromAbove);
        puts(msg7, sizeof(msg7)-1);
	blocking_print_int(acceleration);
	puts(msg8, sizeof(msg8)-1);
}

void print_avg_accel_c_code(int train, int start_speed, int end_speed, int fromAbove, int acceleration){
        char msg1[] = "avg_train_data[";
        char msg4[] = "].acceleration[";
	char msg5[] = "][";
	char msg6[] = "][";
        char msg7[] = "] = ";
        char msg8[] = ";\r\n";

        puts(msg1, sizeof(msg1)-1);
        blocking_print_int(train);
        puts(msg4, sizeof(msg4)-1);
        blocking_print_int(start_speed);
	puts(msg5, sizeof(msg5)-1);
        blocking_print_int(end_speed);
        puts(msg6, sizeof(msg6)-1);
	blocking_print_int(fromAbove);
        puts(msg7, sizeof(msg7)-1);
        blocking_print_int(acceleration);
        puts(msg8, sizeof(msg8)-1);
}

void print_train_data(){
    puts("\033[2J\033[H", 7);
    for(int train = 0; train < NUM_TRAINS; ++train){
	uint64_t low_sum_below = 0;
	uint64_t mid_sum_below = 0;
	uint64_t high_sum_below = 0;
	int low_n_below = 0;
	int mid_n_below = 0;
	int high_n_below = 0; 
        uint64_t low_sum_above = 0;
	uint64_t mid_sum_above = 0;
	uint64_t high_sum_above = 0;
	int low_n_above = 0;
	int mid_n_above = 0;
	int high_n_above = 0; 

	uint64_t accel_sum[NUM_SUPPORTED_SPEEDS][NUM_SUPPORTED_SPEEDS][2] = {0};
	int accel_n[NUM_SUPPORTED_SPEEDS][NUM_SUPPORTED_SPEEDS][2] = {0};
        for(int s1 = 0; s1 < NUM_SENSORS; ++s1){
                for(int s2 = 0; s2 < NUM_SENSORS; ++s2){
                        struct train_data* cur = &train_data[train][s1][s2];
                        if(cur->micrometers_per_tick[SPEED_LOW][0]){
				print_velocity_c_code(train, s1, s2, SPEED_LOW, cur->micrometers_per_tick[SPEED_LOW][0], 0);
				low_sum_below += cur->micrometers_per_tick[SPEED_LOW][0];
				low_n_below++;
			}
			if(cur->micrometers_per_tick[SPEED_MID][0]) {
				print_velocity_c_code(train, s1, s2, SPEED_MID, cur->micrometers_per_tick[SPEED_MID][0], 0);
				mid_sum_below += cur->micrometers_per_tick[SPEED_MID][0];
				mid_n_below++;
			}
			if(cur->micrometers_per_tick[SPEED_HIGH][0]) {
				print_velocity_c_code(train, s1, s2, SPEED_HIGH, cur->micrometers_per_tick[SPEED_HIGH][0], 0);
                		high_sum_below += cur->micrometers_per_tick[SPEED_HIGH][0];
				high_n_below++;
			}
		        if(cur->micrometers_per_tick[SPEED_LOW][1]){
				print_velocity_c_code(train, s1, s2, SPEED_LOW, cur->micrometers_per_tick[SPEED_LOW][1], 1);
				low_sum_above += cur->micrometers_per_tick[SPEED_LOW][1];
				low_n_above++;
			}
			if(cur->micrometers_per_tick[SPEED_MID][1]) {
				print_velocity_c_code(train, s1, s2, SPEED_MID, cur->micrometers_per_tick[SPEED_MID][1], 1);
				mid_sum_above += cur->micrometers_per_tick[SPEED_MID][1];
				mid_n_above++;
			}
			if(cur->micrometers_per_tick[SPEED_HIGH][1]) {
				print_velocity_c_code(train, s1, s2, SPEED_HIGH, cur->micrometers_per_tick[SPEED_HIGH][1], 1);
                		high_sum_above += cur->micrometers_per_tick[SPEED_HIGH][1];
				high_n_above++;
			}

			// acceleration
			if(cur->acceleration[0][SPEED_LOW][0]){
				print_accel_c_code(train, s1, s2, 0, SPEED_LOW, 0, cur->acceleration[0][SPEED_LOW][0]);
				low_sum_below += cur->acceleration[0][SPEED_LOW][0];
				low_n_below++;
			}
			if(cur->acceleration[0][SPEED_MID][0]) {
				print_accel_c_code(train, s1, s2, 0, SPEED_MID, 0, cur->acceleration[0][SPEED_MID][0]);
				mid_sum_below += cur->acceleration[0][SPEED_MID][0];
				mid_n_below++;
			}
			if(cur->acceleration[0][SPEED_HIGH][0]) {
				print_accel_c_code(train, s1, s2, 0, SPEED_HIGH, 0, cur->acceleration[0][SPEED_HIGH][0]);
                		high_sum_below += cur->acceleration[0][SPEED_HIGH][0];
				high_n_below++;
			}
			for(int speed1 = 0; speed1 < NUM_SUPPORTED_SPEEDS; ++speed1){
				for(int speed2 = 0; speed2 < NUM_SUPPORTED_SPEEDS; ++speed2){
					for(int fromAbove = 0; fromAbove < 2; ++fromAbove){
						if(cur->acceleration[speed1][speed2][fromAbove]){
							print_accel_c_code(train, s1, s2, speed1, speed2, fromAbove, cur->acceleration[speed1][speed2][fromAbove]);
							accel_sum[speed1][speed2][fromAbove] += cur->acceleration[speed1][speed2][fromAbove];
							accel_n[speed1][speed2][fromAbove]++;
						}
					}	
				}
			}
		}
        }
	puts("\r\n", 2);
	if(low_n_below) print_avg_velocity_c_code(train, SPEED_LOW, low_sum_below / low_n_below, 0);	
	if(mid_n_below) print_avg_velocity_c_code(train, SPEED_MID, mid_sum_below / mid_n_below, 0);	
	if(high_n_below) print_avg_velocity_c_code(train, SPEED_HIGH, high_sum_below / high_n_below, 0);	
	puts("\r\n", 2);
	if(low_n_above) print_avg_velocity_c_code(train, SPEED_LOW, low_sum_above / low_n_above, 1);	
	if(mid_n_above) print_avg_velocity_c_code(train, SPEED_MID, mid_sum_above / mid_n_above, 1);	
	if(high_n_above) print_avg_velocity_c_code(train, SPEED_HIGH, high_sum_above / high_n_above, 1);	
    	puts("\r\n", 2);

	for(int speed1 = 0; speed1 < NUM_SUPPORTED_SPEEDS; ++speed1){
		for(int speed2 = 0; speed2 < NUM_SUPPORTED_SPEEDS; ++speed2){
			for(int fromAbove = 0; fromAbove < 2; ++fromAbove){
				if(accel_n[speed1][speed2][fromAbove]){
					print_avg_accel_c_code(train, speed1, speed2, fromAbove, accel_sum[speed1][speed2][fromAbove] / accel_n[speed1][speed2][fromAbove]);
				}
			}	
		}	
    	}
	puts("\r\n", 2);
        getc();
    }
}
#endif

static struct train_data avg_train_data[NUM_TRAINS];

static int sensor_name_to_id(char* s){
	int track_num = s[0] - 'A';
	int sensor_num = s[1] - '0';
	if(s[2] != '\0'){
		sensor_num *= 10;
		sensor_num += s[2] - '0';
	}
	return (track_num * 16 + (sensor_num - 1));
}

// some of the values for micrometers_per_tick are initial values copied from a communal measurement spreadsheet
// some values are already updated using dynamic calibration
void init_train_data() {
    // TRAIN 24
    train_data[0][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_LOW][0] = 1614;
    train_data[0][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_MID][0] = 2629;
    train_data[0][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_HIGH][0] = 7094;
    //train_data[0][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_LOW][0] = ;
    //train_data[0][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_MID][0] = ;
    //train_data[0][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_HIGH][0] = ;
    train_data[0][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_LOW][0] = 1774;
    train_data[0][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_MID][0] = 3032;
    train_data[0][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_HIGH][0] = 5296;
    train_data[0][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_LOW][0] = 1648;
    train_data[0][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_MID][0] = 2786;
    train_data[0][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_HIGH][0] = 6382;
    train_data[0][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_LOW][0] = 1616;
    train_data[0][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_MID][0] = 2557;
    train_data[0][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_HIGH][0] = 5690;
    train_data[0][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_LOW][0] = 1624;
    train_data[0][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_MID][0] = 3284;
    train_data[0][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_HIGH][0] = 5352;
    train_data[0][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_LOW][0] = 1752;
    train_data[0][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_MID][0] = 2686;
    train_data[0][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_HIGH][0] = 8057;
    train_data[0][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_LOW][0] = 1606;
    train_data[0][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_MID][0] = 2757;
    train_data[0][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_HIGH][0] = 5990;
    train_data[0][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_LOW][0] = 1621;
    train_data[0][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_MID][0] = 3169;
    train_data[0][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_HIGH][0] = 5222;
    train_data[0][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_LOW][0] = 1752;
    train_data[0][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_MID][0] = 2611;
    train_data[0][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_HIGH][0] = 5222;
    train_data[0][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_LOW][0] = 1636;
    train_data[0][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_MID][0] = 2845;
    train_data[0][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_HIGH][0] = 7623;
    train_data[0][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_LOW][0] = 1701;
    train_data[0][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_MID][0] = 2919;
    train_data[0][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_HIGH][0] = 4986;

    avg_train_data[0].micrometers_per_tick[SPEED_LOW][0] = 1667;
    avg_train_data[0].micrometers_per_tick[SPEED_MID][0] = 2843;
    avg_train_data[0].micrometers_per_tick[SPEED_HIGH][0] = 6083;

    avg_train_data[0].stopping_distance_mm[SPEED_LOW][0] = 156;
    avg_train_data[0].stopping_distance_mm[SPEED_MID][0] = 481;
    avg_train_data[0].stopping_distance_mm[SPEED_HIGH][0] = 1250;

    // from above

    avg_train_data[0].micrometers_per_tick[SPEED_LOW][1] = 1867; // estimate
    avg_train_data[0].micrometers_per_tick[SPEED_MID][1] = 3043; // estimate

    avg_train_data[0].stopping_distance_mm[SPEED_LOW][1] = 156; // TBD
    avg_train_data[0].stopping_distance_mm[SPEED_MID][1] = 481; // TBD

// newdata
train_data[0][2][42].micrometers_per_tick[7][0] = 1774;
train_data[0][2][42].micrometers_per_tick[10][0] = 3032;
train_data[0][2][42].micrometers_per_tick[14][0] = 5296;
train_data[0][3][31].micrometers_per_tick[7][0] = 1634;
train_data[0][3][31].micrometers_per_tick[10][0] = 3455;
train_data[0][3][31].micrometers_per_tick[14][0] = 5867;
train_data[0][3][31].micrometers_per_tick[7][1] = 1913;
train_data[0][3][31].micrometers_per_tick[10][1] = 3652;
train_data[0][17][40].micrometers_per_tick[7][0] = 1701;
train_data[0][17][40].micrometers_per_tick[10][0] = 2919;
train_data[0][17][40].micrometers_per_tick[14][0] = 4986;
train_data[0][20][50].micrometers_per_tick[7][0] = 1616;
train_data[0][20][50].micrometers_per_tick[10][0] = 2557;
train_data[0][20][50].micrometers_per_tick[14][0] = 5690;
train_data[0][31][36].micrometers_per_tick[7][0] = 1663;
train_data[0][31][36].micrometers_per_tick[10][0] = 3425;
train_data[0][31][36].micrometers_per_tick[14][0] = 5876;
train_data[0][31][36].micrometers_per_tick[7][1] = 1972;
train_data[0][31][36].micrometers_per_tick[10][1] = 3755;
train_data[0][36][46].micrometers_per_tick[7][0] = 1673;
train_data[0][36][46].micrometers_per_tick[10][0] = 3412;
train_data[0][36][46].micrometers_per_tick[14][0] = 6002;
train_data[0][36][46].micrometers_per_tick[7][1] = 1998;
train_data[0][36][46].micrometers_per_tick[10][1] = 3818;
train_data[0][40][30].micrometers_per_tick[7][0] = 1614;
train_data[0][40][30].micrometers_per_tick[10][0] = 2629;
train_data[0][40][30].micrometers_per_tick[14][0] = 7094;
train_data[0][42][20].micrometers_per_tick[7][0] = 1648;
train_data[0][42][20].micrometers_per_tick[10][0] = 2786;
train_data[0][42][20].micrometers_per_tick[14][0] = 6382;
train_data[0][45][3].micrometers_per_tick[7][0] = 1643;
train_data[0][45][3].micrometers_per_tick[10][0] = 3399;
train_data[0][45][3].micrometers_per_tick[14][0] = 6012;
train_data[0][45][3].micrometers_per_tick[7][1] = 1923;
train_data[0][45][3].micrometers_per_tick[10][1] = 3741;
train_data[0][46][59].micrometers_per_tick[7][0] = 1587;
train_data[0][46][59].micrometers_per_tick[10][0] = 3404;
train_data[0][46][59].micrometers_per_tick[14][0] = 5915;
train_data[0][46][59].micrometers_per_tick[7][1] = 1859;
train_data[0][46][59].micrometers_per_tick[10][1] = 3569;
train_data[0][50][68].micrometers_per_tick[7][0] = 1624;
train_data[0][50][68].micrometers_per_tick[10][0] = 3284;
train_data[0][50][68].micrometers_per_tick[14][0] = 5352;
train_data[0][53][73].micrometers_per_tick[7][0] = 1606;
train_data[0][53][73].micrometers_per_tick[10][0] = 2757;
train_data[0][53][73].micrometers_per_tick[14][0] = 5990;
train_data[0][55][71].micrometers_per_tick[7][0] = 1660;
train_data[0][55][71].micrometers_per_tick[10][0] = 3408;
train_data[0][55][71].micrometers_per_tick[14][0] = 5475;
train_data[0][55][71].micrometers_per_tick[7][1] = 1949;
train_data[0][55][71].micrometers_per_tick[10][1] = 3815;
train_data[0][57][55].micrometers_per_tick[7][0] = 1652;
train_data[0][57][55].micrometers_per_tick[10][0] = 3412;
train_data[0][57][55].micrometers_per_tick[14][0] = 5891;
train_data[0][57][55].micrometers_per_tick[7][1] = 1943;
train_data[0][57][55].micrometers_per_tick[10][1] = 3709;
train_data[0][59][74].micrometers_per_tick[7][0] = 1681;
train_data[0][59][74].micrometers_per_tick[10][0] = 3660;
train_data[0][59][74].micrometers_per_tick[14][0] = 6398;
train_data[0][59][74].micrometers_per_tick[7][1] = 1977;
train_data[0][59][74].micrometers_per_tick[10][1] = 3677;
train_data[0][60][17].micrometers_per_tick[7][0] = 1636;
train_data[0][60][17].micrometers_per_tick[10][0] = 2845;
train_data[0][60][17].micrometers_per_tick[14][0] = 7623;
train_data[0][61][77].micrometers_per_tick[7][0] = 1639;
train_data[0][61][77].micrometers_per_tick[10][0] = 2843;
train_data[0][61][77].micrometers_per_tick[14][0] = 6083;
train_data[0][61][77].micrometers_per_tick[7][1] = 1867;
train_data[0][61][77].micrometers_per_tick[10][1] = 3043;
train_data[0][68][53].micrometers_per_tick[7][0] = 1752;
train_data[0][68][53].micrometers_per_tick[10][0] = 2686;
train_data[0][68][53].micrometers_per_tick[14][0] = 8057;
train_data[0][71][45].micrometers_per_tick[7][0] = 1633;
train_data[0][71][45].micrometers_per_tick[10][0] = 3441;
train_data[0][71][45].micrometers_per_tick[14][0] = 6079;
train_data[0][71][45].micrometers_per_tick[7][1] = 1933;
train_data[0][71][45].micrometers_per_tick[10][1] = 3736;
train_data[0][72][55].micrometers_per_tick[7][0] = 1660;
train_data[0][72][55].micrometers_per_tick[10][0] = 2843;
train_data[0][72][55].micrometers_per_tick[14][0] = 6083;
train_data[0][72][55].micrometers_per_tick[7][1] = 1867;
train_data[0][72][55].micrometers_per_tick[10][1] = 3043;
train_data[0][73][76].micrometers_per_tick[7][0] = 1621;
train_data[0][73][76].micrometers_per_tick[10][0] = 3169;
train_data[0][73][76].micrometers_per_tick[14][0] = 5222;
train_data[0][74][57].micrometers_per_tick[7][0] = 1648;
train_data[0][74][57].micrometers_per_tick[10][0] = 3304;
train_data[0][74][57].micrometers_per_tick[14][0] = 5878;
train_data[0][74][57].micrometers_per_tick[7][1] = 1948;
train_data[0][74][57].micrometers_per_tick[10][1] = 3775;
train_data[0][76][60].micrometers_per_tick[7][0] = 1752;
train_data[0][76][60].micrometers_per_tick[10][0] = 2611;
train_data[0][76][60].micrometers_per_tick[14][0] = 5222;
train_data[0][77][72].micrometers_per_tick[7][0] = 1669;
train_data[0][77][72].micrometers_per_tick[10][0] = 2843;
train_data[0][77][72].micrometers_per_tick[14][0] = 6083;
train_data[0][77][72].micrometers_per_tick[7][1] = 1867;
train_data[0][77][72].micrometers_per_tick[10][1] = 3043;

avg_train_data[0].micrometers_per_tick[7][0] = 1657;
avg_train_data[0].micrometers_per_tick[10][0] = 3088;
avg_train_data[0].micrometers_per_tick[14][0] = 6023;

avg_train_data[0].micrometers_per_tick[7][1] = 1924;
avg_train_data[0].micrometers_per_tick[10][1] = 3567;

// preliminary data for train 24 - should probably be recomputed with proper velocity estimates
    avg_train_data[0].acceleration[0][7][0] = 30;
    avg_train_data[0].acceleration[0][10][0] = 24;
    avg_train_data[0].acceleration[0][14][0] = 22;
    avg_train_data[0].acceleration[0][7][1] = 30;
    avg_train_data[0].acceleration[0][10][1] = 24;
    avg_train_data[0].acceleration[0][14][1] = 22;

    avg_train_data[0].acceleration[7][0][0] = -9;
    avg_train_data[0].acceleration[7][10][0] = 9;
    avg_train_data[0].acceleration[7][14][0] = 13;
    avg_train_data[0].acceleration[7][0][1] = -14;
    avg_train_data[0].acceleration[7][10][1] = 9;
    avg_train_data[0].acceleration[7][14][1] = 13;
    
    avg_train_data[0].acceleration[10][0][0] = -10;
    avg_train_data[0].acceleration[10][7][0] = -10;
    avg_train_data[0].acceleration[10][14][0] = 10;
    avg_train_data[0].acceleration[10][0][1] = -13;
    avg_train_data[0].acceleration[10][7][1] = -10;
    avg_train_data[0].acceleration[10][14][1] = 10;
 
    avg_train_data[0].acceleration[14][0][0] = -13;
    avg_train_data[0].acceleration[14][7][0] = -10;
    avg_train_data[0].acceleration[14][10][0] = -6;

    // TRAIN 58
    
    train_data[1][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_LOW][0] = 1528;
    train_data[1][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_MID][0] = 3057;
    train_data[1][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_HIGH][0] = 5222;
    //train_data[1][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_LOW][0] = ;
    //train_data[1][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_MID][0] = ;
    //train_data[1][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_HIGH][0] = ;
    train_data[1][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_LOW][0] = 1504;
    train_data[1][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_MID][0] = 2961;
    train_data[1][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_HIGH][0] = 5371;
    train_data[1][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_LOW][0] = 1513;
    train_data[1][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_MID][0] = 3311;
    train_data[1][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_HIGH][0] = 5014;
    train_data[1][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_LOW][0] = 1428;
    train_data[1][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_MID][0] = 2806;
    train_data[1][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_HIGH][0] = 5771;
    train_data[1][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_LOW][0] = 1624;
    train_data[1][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_MID][0] = 3284;
    train_data[1][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_HIGH][0] = 5453;
    train_data[1][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_LOW][0] = 1431;
    train_data[1][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_MID][0] = 3133;
    train_data[1][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_HIGH][0] = 5127;
    train_data[1][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_LOW][0] = 1527;
    train_data[1][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_MID][0] = 3262;
    train_data[1][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_HIGH][0] = 5877;
    train_data[1][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_LOW][0] = 1461;
    train_data[1][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_MID][0] = 3065;
    train_data[1][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_HIGH][0] = 5222;
    train_data[1][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_LOW][0] = 1567;
    train_data[1][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_MID][0] = 3099;
    train_data[1][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_HIGH][0] = 5423;
    //train_data[1][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_LOW][0] = ;
    train_data[1][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_MID][0] = 2845;
    train_data[1][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_HIGH][0] = 5771;
    train_data[1][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_LOW][0] = 1448;
    train_data[1][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_MID][0] = 2943;
    train_data[1][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_HIGH][0] = 5203;
    
    avg_train_data[1].micrometers_per_tick[SPEED_LOW][0] = 1505;
    avg_train_data[1].micrometers_per_tick[SPEED_MID][0] = 3070;
    avg_train_data[1].micrometers_per_tick[SPEED_HIGH][0] = 5405;

    avg_train_data[1].stopping_distance_mm[SPEED_LOW][0] = 132;
    avg_train_data[1].stopping_distance_mm[SPEED_MID][0] = 429;
    avg_train_data[1].stopping_distance_mm[SPEED_HIGH][0] = 1242;

    // from above
    
    avg_train_data[1].micrometers_per_tick[SPEED_LOW][1] = 1705; // estimate
    avg_train_data[1].micrometers_per_tick[SPEED_MID][1] = 3270; // estimate

    avg_train_data[1].stopping_distance_mm[SPEED_LOW][1] = 173;
    avg_train_data[1].stopping_distance_mm[SPEED_MID][1] = 497;

    avg_train_data[1].acceleration[0][7][0] = 12;
    avg_train_data[1].acceleration[0][10][0] = 23;
    avg_train_data[1].acceleration[0][14][0] = 19;
    avg_train_data[1].acceleration[0][7][1] = 12;
    avg_train_data[1].acceleration[0][10][1] = 23;
    avg_train_data[1].acceleration[0][14][1] = 19;
   

    avg_train_data[1].acceleration[7][0][0] = -9;
    avg_train_data[1].acceleration[7][10][0] = 7;
    avg_train_data[1].acceleration[7][14][0] = 8;
    avg_train_data[1].acceleration[7][0][1] = -8;
    avg_train_data[1].acceleration[7][10][1] = 7;
    avg_train_data[1].acceleration[7][14][1] = 8;
    
    avg_train_data[1].acceleration[10][0][0] = -11;
    avg_train_data[1].acceleration[10][7][0] = -6;
    avg_train_data[1].acceleration[10][14][0] = 10;
    avg_train_data[1].acceleration[10][0][1] = -11;
    avg_train_data[1].acceleration[10][7][1] = -6;
    avg_train_data[1].acceleration[10][14][1] = 10;
 
    avg_train_data[1].acceleration[14][0][0] = -12;
    avg_train_data[1].acceleration[14][7][0] = -7;
    avg_train_data[1].acceleration[14][10][0] = -10;

/*
 * data does not have delay after accel, probably inaccurate
    // HERE
train_data[1][2][42].micrometers_per_tick[7][0] = 1454;
train_data[1][2][42].micrometers_per_tick[10][0] = 2969;
train_data[1][2][42].micrometers_per_tick[14][0] = 5656;
train_data[1][2][42].micrometers_per_tick[7][1] = 1682;
train_data[1][2][42].micrometers_per_tick[10][1] = 3269;
train_data[1][2][44].micrometers_per_tick[7][0] = 1479;
train_data[1][2][44].micrometers_per_tick[10][0] = 3099;
train_data[1][2][44].micrometers_per_tick[14][0] = 6146;
train_data[1][2][44].micrometers_per_tick[7][1] = 1711;
train_data[1][2][44].micrometers_per_tick[10][1] = 3285;
train_data[1][3][31].micrometers_per_tick[7][0] = 1428;
train_data[1][3][31].micrometers_per_tick[10][0] = 3064;
train_data[1][3][31].micrometers_per_tick[14][0] = 5958;
train_data[1][3][31].micrometers_per_tick[7][1] = 1720;
train_data[1][3][31].micrometers_per_tick[10][1] = 3452;
train_data[1][16][61].micrometers_per_tick[7][0] = 1481;
train_data[1][16][61].micrometers_per_tick[10][0] = 3117;
train_data[1][16][61].micrometers_per_tick[14][0] = 5874;
train_data[1][16][61].micrometers_per_tick[7][1] = 1690;
train_data[1][16][61].micrometers_per_tick[10][1] = 3396;
train_data[1][17][40].micrometers_per_tick[7][0] = 1447;
train_data[1][17][40].micrometers_per_tick[10][0] = 3017;
train_data[1][17][40].micrometers_per_tick[14][0] = 5792;
train_data[1][17][40].micrometers_per_tick[7][1] = 1698;
train_data[1][17][40].micrometers_per_tick[10][1] = 3227;
train_data[1][18][33].micrometers_per_tick[7][0] = 1442;
train_data[1][18][33].micrometers_per_tick[10][0] = 3174;
train_data[1][18][33].micrometers_per_tick[14][0] = 5684;
train_data[1][18][33].micrometers_per_tick[7][1] = 1630;
train_data[1][18][33].micrometers_per_tick[10][1] = 3411;
train_data[1][19][40].micrometers_per_tick[7][0] = 1438;
train_data[1][19][40].micrometers_per_tick[10][0] = 2962;
train_data[1][19][40].micrometers_per_tick[14][0] = 5873;
train_data[1][19][40].micrometers_per_tick[7][1] = 1726;
train_data[1][19][40].micrometers_per_tick[10][1] = 3365;
train_data[1][20][50].micrometers_per_tick[7][0] = 1438;
train_data[1][20][50].micrometers_per_tick[10][0] = 2981;
train_data[1][20][50].micrometers_per_tick[14][0] = 5906;
train_data[1][20][50].micrometers_per_tick[7][1] = 1714;
train_data[1][20][50].micrometers_per_tick[10][1] = 3430;
train_data[1][21][43].micrometers_per_tick[7][0] = 1463;
train_data[1][21][43].micrometers_per_tick[10][0] = 3021;
train_data[1][21][43].micrometers_per_tick[14][0] = 6083;
train_data[1][21][43].micrometers_per_tick[7][1] = 1684;
train_data[1][21][43].micrometers_per_tick[10][1] = 3316;
train_data[1][25][54].micrometers_per_tick[7][1] = 1702;
train_data[1][25][56].micrometers_per_tick[7][1] = 1723;
train_data[1][28][65].micrometers_per_tick[7][0] = 1505;
train_data[1][28][65].micrometers_per_tick[10][0] = 3070;
train_data[1][28][65].micrometers_per_tick[14][0] = 5449;
train_data[1][28][65].micrometers_per_tick[7][1] = 1705;
train_data[1][28][65].micrometers_per_tick[10][1] = 3270;
train_data[1][29][63].micrometers_per_tick[7][0] = 1456;
train_data[1][29][63].micrometers_per_tick[10][0] = 3198;
train_data[1][29][63].micrometers_per_tick[14][0] = 6216;
train_data[1][29][63].micrometers_per_tick[7][1] = 1710;
train_data[1][29][63].micrometers_per_tick[10][1] = 3248;
train_data[1][30][2].micrometers_per_tick[7][0] = 1470;
train_data[1][30][2].micrometers_per_tick[10][0] = 3188;
train_data[1][30][2].micrometers_per_tick[14][0] = 5817;
train_data[1][30][2].micrometers_per_tick[7][1] = 1715;
train_data[1][30][2].micrometers_per_tick[10][1] = 3462;
train_data[1][31][36].micrometers_per_tick[7][0] = 1453;
train_data[1][31][36].micrometers_per_tick[10][0] = 3115;
train_data[1][31][36].micrometers_per_tick[14][0] = 5972;
train_data[1][31][36].micrometers_per_tick[7][1] = 1712;
train_data[1][31][36].micrometers_per_tick[10][1] = 3508;
train_data[1][31][41].micrometers_per_tick[7][0] = 1443;
train_data[1][31][41].micrometers_per_tick[10][0] = 3051;
train_data[1][31][41].micrometers_per_tick[14][0] = 6039;
train_data[1][31][41].micrometers_per_tick[7][1] = 1683;
train_data[1][31][41].micrometers_per_tick[10][1] = 3410;
train_data[1][32][19].micrometers_per_tick[7][0] = 1473;
train_data[1][32][19].micrometers_per_tick[10][0] = 3237;
train_data[1][32][19].micrometers_per_tick[14][0] = 5624;
train_data[1][32][19].micrometers_per_tick[7][1] = 1720;
train_data[1][32][19].micrometers_per_tick[10][1] = 3395;
train_data[1][33][65].micrometers_per_tick[7][0] = 1467;
train_data[1][33][65].micrometers_per_tick[10][0] = 3013;
train_data[1][33][65].micrometers_per_tick[14][0] = 5541;
train_data[1][33][65].micrometers_per_tick[7][1] = 1659;
train_data[1][33][65].micrometers_per_tick[10][1] = 3241;
train_data[1][36][46].micrometers_per_tick[7][0] = 1506;
train_data[1][36][46].micrometers_per_tick[10][0] = 3040;
train_data[1][36][46].micrometers_per_tick[14][0] = 6365;
train_data[1][36][46].micrometers_per_tick[7][1] = 1719;
train_data[1][36][46].micrometers_per_tick[10][1] = 3342;
train_data[1][36][74].micrometers_per_tick[7][0] = 1474;
train_data[1][36][74].micrometers_per_tick[10][0] = 3057;
train_data[1][36][74].micrometers_per_tick[14][0] = 5887;
train_data[1][36][74].micrometers_per_tick[7][1] = 1696;
train_data[1][36][74].micrometers_per_tick[10][1] = 3316;
train_data[1][37][30].micrometers_per_tick[7][0] = 1486;
train_data[1][37][30].micrometers_per_tick[10][0] = 3115;
train_data[1][37][30].micrometers_per_tick[14][0] = 6005;
train_data[1][37][30].micrometers_per_tick[7][1] = 1694;
train_data[1][37][30].micrometers_per_tick[10][1] = 3362;
train_data[1][40][30].micrometers_per_tick[7][0] = 1462;
train_data[1][40][30].micrometers_per_tick[10][0] = 3032;
train_data[1][40][30].micrometers_per_tick[14][0] = 5686;
train_data[1][40][30].micrometers_per_tick[7][1] = 1682;
train_data[1][40][30].micrometers_per_tick[10][1] = 3263;
train_data[1][41][16].micrometers_per_tick[7][0] = 1455;
train_data[1][41][16].micrometers_per_tick[10][0] = 3025;
train_data[1][41][16].micrometers_per_tick[14][0] = 5765;
train_data[1][41][16].micrometers_per_tick[7][1] = 1703;
train_data[1][41][16].micrometers_per_tick[10][1] = 3235;
train_data[1][41][18].micrometers_per_tick[7][0] = 1500;
train_data[1][41][18].micrometers_per_tick[10][0] = 3074;
train_data[1][41][18].micrometers_per_tick[14][0] = 5826;
train_data[1][41][18].micrometers_per_tick[7][1] = 1734;
train_data[1][41][18].micrometers_per_tick[10][1] = 3357;
train_data[1][42][20].micrometers_per_tick[7][0] = 1493;
train_data[1][42][20].micrometers_per_tick[10][0] = 3229;
train_data[1][42][20].micrometers_per_tick[14][0] = 5741;
train_data[1][42][20].micrometers_per_tick[7][1] = 1715;
train_data[1][42][20].micrometers_per_tick[10][1] = 3320;
train_data[1][42][79].micrometers_per_tick[7][0] = 1479;
train_data[1][42][79].micrometers_per_tick[10][0] = 3100;
train_data[1][42][79].micrometers_per_tick[14][0] = 6201;
train_data[1][42][79].micrometers_per_tick[7][1] = 1690;
train_data[1][42][79].micrometers_per_tick[10][1] = 3390;
train_data[1][43][3].micrometers_per_tick[7][0] = 1451;
train_data[1][43][3].micrometers_per_tick[10][0] = 3112;
train_data[1][43][3].micrometers_per_tick[14][0] = 5791;
train_data[1][43][3].micrometers_per_tick[7][1] = 1703;
train_data[1][43][3].micrometers_per_tick[10][1] = 3362;
train_data[1][44][70].micrometers_per_tick[7][0] = 1485;
train_data[1][44][70].micrometers_per_tick[10][0] = 3080;
train_data[1][44][70].micrometers_per_tick[14][0] = 6052;
train_data[1][44][70].micrometers_per_tick[7][1] = 1706;
train_data[1][44][70].micrometers_per_tick[10][1] = 3419;
train_data[1][45][3].micrometers_per_tick[7][0] = 1456;
train_data[1][45][3].micrometers_per_tick[10][0] = 3028;
train_data[1][45][3].micrometers_per_tick[14][0] = 5679;
train_data[1][45][3].micrometers_per_tick[7][1] = 1674;
train_data[1][45][3].micrometers_per_tick[10][1] = 3318;
train_data[1][46][59].micrometers_per_tick[7][0] = 1451;
train_data[1][46][59].micrometers_per_tick[10][0] = 3029;
train_data[1][46][59].micrometers_per_tick[14][0] = 5852;
train_data[1][46][59].micrometers_per_tick[7][1] = 1678;
train_data[1][46][59].micrometers_per_tick[10][1] = 3297;
train_data[1][47][37].micrometers_per_tick[7][0] = 1506;
train_data[1][47][37].micrometers_per_tick[10][0] = 3132;
train_data[1][47][37].micrometers_per_tick[14][0] = 5925;
train_data[1][47][37].micrometers_per_tick[7][1] = 1721;
train_data[1][47][37].micrometers_per_tick[10][1] = 3452;
train_data[1][48][29].micrometers_per_tick[7][0] = 1461;
train_data[1][48][29].micrometers_per_tick[10][0] = 3017;
train_data[1][48][29].micrometers_per_tick[14][0] = 5526;
train_data[1][48][29].micrometers_per_tick[7][1] = 1668;
train_data[1][48][29].micrometers_per_tick[10][1] = 3372;
train_data[1][50][68].micrometers_per_tick[7][0] = 1538;
train_data[1][50][68].micrometers_per_tick[10][0] = 2793;
train_data[1][50][68].micrometers_per_tick[14][0] = 5715;
train_data[1][50][68].micrometers_per_tick[7][1] = 1662;
train_data[1][50][68].micrometers_per_tick[10][1] = 3363;
train_data[1][51][21].micrometers_per_tick[7][0] = 1460;
train_data[1][51][21].micrometers_per_tick[10][0] = 3122;
train_data[1][51][21].micrometers_per_tick[14][0] = 5874;
train_data[1][51][21].micrometers_per_tick[7][1] = 1708;
train_data[1][51][21].micrometers_per_tick[10][1] = 3392;
train_data[1][52][69].micrometers_per_tick[7][0] = 1476;
train_data[1][52][69].micrometers_per_tick[10][0] = 3198;
train_data[1][52][69].micrometers_per_tick[14][0] = 5564;
train_data[1][52][69].micrometers_per_tick[7][1] = 1725;
train_data[1][52][69].micrometers_per_tick[10][1] = 3571;
train_data[1][53][73].micrometers_per_tick[7][0] = 1501;
train_data[1][53][73].micrometers_per_tick[10][0] = 3130;
train_data[1][53][73].micrometers_per_tick[14][0] = 5695;
train_data[1][53][73].micrometers_per_tick[7][1] = 1701;
train_data[1][53][73].micrometers_per_tick[10][1] = 3385;
train_data[1][54][56].micrometers_per_tick[7][0] = 1486;
train_data[1][54][56].micrometers_per_tick[10][0] = 3128;
train_data[1][54][56].micrometers_per_tick[14][0] = 5912;
train_data[1][54][56].micrometers_per_tick[7][1] = 1666;
train_data[1][54][56].micrometers_per_tick[10][1] = 3650;
train_data[1][55][71].micrometers_per_tick[7][0] = 1477;
train_data[1][55][71].micrometers_per_tick[10][0] = 3105;
train_data[1][55][71].micrometers_per_tick[14][0] = 5857;
train_data[1][55][71].micrometers_per_tick[7][1] = 1716;
train_data[1][55][71].micrometers_per_tick[10][1] = 3510;
train_data[1][56][75].micrometers_per_tick[7][0] = 1472;
train_data[1][56][75].micrometers_per_tick[10][0] = 3126;
train_data[1][56][75].micrometers_per_tick[14][0] = 6087;
train_data[1][56][75].micrometers_per_tick[7][1] = 1699;
train_data[1][56][75].micrometers_per_tick[10][1] = 3403;
train_data[1][57][55].micrometers_per_tick[7][0] = 1469;
train_data[1][57][55].micrometers_per_tick[10][0] = 3107;
train_data[1][57][55].micrometers_per_tick[14][0] = 6057;
train_data[1][57][55].micrometers_per_tick[7][1] = 1692;
train_data[1][57][55].micrometers_per_tick[10][1] = 3326;
train_data[1][58][47].micrometers_per_tick[7][0] = 1461;
train_data[1][58][47].micrometers_per_tick[10][0] = 3033;
train_data[1][58][47].micrometers_per_tick[14][0] = 5874;
train_data[1][58][47].micrometers_per_tick[7][1] = 1678;
train_data[1][58][47].micrometers_per_tick[10][1] = 3318;
train_data[1][59][74].micrometers_per_tick[7][0] = 1484;
train_data[1][59][74].micrometers_per_tick[10][0] = 3137;
train_data[1][59][74].micrometers_per_tick[14][0] = 5621;
train_data[1][59][74].micrometers_per_tick[7][1] = 1687;
train_data[1][59][74].micrometers_per_tick[10][1] = 3389;
train_data[1][60][17].micrometers_per_tick[7][0] = 1470;
train_data[1][60][17].micrometers_per_tick[10][0] = 3041;
train_data[1][60][17].micrometers_per_tick[14][0] = 5689;
train_data[1][60][17].micrometers_per_tick[7][1] = 1700;
train_data[1][60][17].micrometers_per_tick[10][1] = 3413;
train_data[1][61][77].micrometers_per_tick[7][0] = 1432;
train_data[1][61][77].micrometers_per_tick[10][0] = 3011;
train_data[1][61][77].micrometers_per_tick[14][0] = 5639;
train_data[1][61][77].micrometers_per_tick[7][1] = 1692;
train_data[1][61][77].micrometers_per_tick[10][1] = 3876;
train_data[1][62][28].micrometers_per_tick[7][0] = 1505;
train_data[1][62][28].micrometers_per_tick[10][0] = 3070;
train_data[1][62][28].micrometers_per_tick[14][0] = 5418;
train_data[1][62][28].micrometers_per_tick[7][1] = 1705;
train_data[1][62][28].micrometers_per_tick[10][1] = 3270;
train_data[1][63][77].micrometers_per_tick[7][0] = 1455;
train_data[1][63][77].micrometers_per_tick[10][0] = 2949;
train_data[1][63][77].micrometers_per_tick[14][0] = 5559;
train_data[1][63][77].micrometers_per_tick[7][1] = 1671;
train_data[1][63][77].micrometers_per_tick[10][1] = 3254;
train_data[1][64][32].micrometers_per_tick[7][0] = 1454;
train_data[1][64][32].micrometers_per_tick[10][0] = 3030;
train_data[1][64][32].micrometers_per_tick[14][0] = 5881;
train_data[1][64][32].micrometers_per_tick[7][1] = 1700;
train_data[1][64][32].micrometers_per_tick[10][1] = 3295;
train_data[1][65][78].micrometers_per_tick[7][0] = 1479;
train_data[1][65][78].micrometers_per_tick[10][0] = 3148;
train_data[1][65][78].micrometers_per_tick[14][0] = 5792;
train_data[1][65][78].micrometers_per_tick[7][1] = 1699;
train_data[1][65][78].micrometers_per_tick[10][1] = 3375;
train_data[1][66][48].micrometers_per_tick[7][0] = 1491;
train_data[1][66][48].micrometers_per_tick[10][0] = 3198;
train_data[1][66][48].micrometers_per_tick[14][0] = 6117;
train_data[1][66][48].micrometers_per_tick[7][1] = 1704;
train_data[1][66][48].micrometers_per_tick[10][1] = 3451;
train_data[1][68][53].micrometers_per_tick[7][0] = 1438;
train_data[1][68][53].micrometers_per_tick[10][0] = 3147;
train_data[1][68][53].micrometers_per_tick[14][0] = 5595;
train_data[1][68][53].micrometers_per_tick[7][1] = 1728;
train_data[1][68][53].micrometers_per_tick[10][1] = 3326;
train_data[1][69][51].micrometers_per_tick[7][0] = 1456;
train_data[1][69][51].micrometers_per_tick[10][0] = 2932;
train_data[1][69][51].micrometers_per_tick[14][0] = 5762;
train_data[1][69][51].micrometers_per_tick[7][1] = 1682;
train_data[1][69][51].micrometers_per_tick[10][1] = 3308;
train_data[1][69][66].micrometers_per_tick[7][0] = 1482;
train_data[1][69][66].micrometers_per_tick[10][0] = 3097;
train_data[1][69][66].micrometers_per_tick[14][0] = 5790;
train_data[1][69][66].micrometers_per_tick[7][1] = 1717;
train_data[1][69][66].micrometers_per_tick[10][1] = 3314;
train_data[1][70][54].micrometers_per_tick[7][0] = 1480;
train_data[1][70][54].micrometers_per_tick[10][0] = 3061;
train_data[1][70][54].micrometers_per_tick[14][0] = 5969;
train_data[1][70][54].micrometers_per_tick[7][1] = 1715;
train_data[1][70][54].micrometers_per_tick[10][1] = 3360;
train_data[1][71][45].micrometers_per_tick[7][0] = 1465;
train_data[1][71][45].micrometers_per_tick[10][0] = 3161;
train_data[1][71][45].micrometers_per_tick[14][0] = 6247;
train_data[1][71][45].micrometers_per_tick[7][1] = 1699;
train_data[1][71][45].micrometers_per_tick[10][1] = 3385;
train_data[1][72][52].micrometers_per_tick[7][0] = 1510;
train_data[1][72][52].micrometers_per_tick[10][0] = 3155;
train_data[1][72][52].micrometers_per_tick[14][0] = 5816;
train_data[1][72][52].micrometers_per_tick[7][1] = 1720;
train_data[1][72][52].micrometers_per_tick[10][1] = 3347;
train_data[1][72][55].micrometers_per_tick[7][0] = 1478;
train_data[1][72][55].micrometers_per_tick[10][0] = 3070;
train_data[1][72][55].micrometers_per_tick[14][0] = 5405;
train_data[1][72][55].micrometers_per_tick[7][1] = 1705;
train_data[1][72][55].micrometers_per_tick[10][1] = 3270;
train_data[1][73][76].micrometers_per_tick[7][0] = 1484;
train_data[1][73][76].micrometers_per_tick[10][0] = 3162;
train_data[1][73][76].micrometers_per_tick[14][0] = 5772;
train_data[1][73][76].micrometers_per_tick[7][1] = 1728;
train_data[1][73][76].micrometers_per_tick[10][1] = 3361;
train_data[1][74][57].micrometers_per_tick[7][0] = 1450;
train_data[1][74][57].micrometers_per_tick[10][0] = 3013;
train_data[1][74][57].micrometers_per_tick[14][0] = 5904;
train_data[1][74][57].micrometers_per_tick[7][1] = 1699;
train_data[1][74][57].micrometers_per_tick[10][1] = 3290;
train_data[1][75][37].micrometers_per_tick[7][0] = 1483;
train_data[1][75][37].micrometers_per_tick[10][0] = 3063;
train_data[1][75][37].micrometers_per_tick[14][0] = 5900;
train_data[1][75][37].micrometers_per_tick[7][1] = 1694;
train_data[1][75][37].micrometers_per_tick[10][1] = 3303;
train_data[1][75][58].micrometers_per_tick[7][0] = 1484;
train_data[1][75][58].micrometers_per_tick[10][0] = 3027;
train_data[1][75][58].micrometers_per_tick[14][0] = 6037;
train_data[1][75][58].micrometers_per_tick[7][1] = 1685;
train_data[1][75][58].micrometers_per_tick[10][1] = 3390;
train_data[1][76][60].micrometers_per_tick[7][0] = 1480;
train_data[1][76][60].micrometers_per_tick[10][0] = 3040;
train_data[1][76][60].micrometers_per_tick[14][0] = 5729;
train_data[1][76][60].micrometers_per_tick[7][1] = 1699;
train_data[1][76][60].micrometers_per_tick[10][1] = 3353;
train_data[1][76][62].micrometers_per_tick[7][0] = 1505;
train_data[1][76][62].micrometers_per_tick[10][0] = 3070;
train_data[1][76][62].micrometers_per_tick[14][0] = 5592;
train_data[1][76][62].micrometers_per_tick[7][1] = 1705;
train_data[1][76][62].micrometers_per_tick[10][1] = 3270;
train_data[1][77][72].micrometers_per_tick[7][0] = 1478;
train_data[1][77][72].micrometers_per_tick[10][0] = 3187;
train_data[1][77][72].micrometers_per_tick[14][0] = 5594;
train_data[1][77][72].micrometers_per_tick[7][1] = 1731;
train_data[1][77][72].micrometers_per_tick[10][1] = 3502;
train_data[1][78][43].micrometers_per_tick[7][0] = 1466;
train_data[1][78][43].micrometers_per_tick[10][0] = 3030;
train_data[1][78][43].micrometers_per_tick[14][0] = 5838;
train_data[1][78][43].micrometers_per_tick[7][1] = 1689;
train_data[1][78][43].micrometers_per_tick[10][1] = 3274;
train_data[1][79][64].micrometers_per_tick[7][0] = 1417;
train_data[1][79][64].micrometers_per_tick[10][0] = 3085;
train_data[1][79][64].micrometers_per_tick[14][0] = 5912;
train_data[1][79][64].micrometers_per_tick[7][1] = 1682;
train_data[1][79][64].micrometers_per_tick[10][1] = 3252;

avg_train_data[1].micrometers_per_tick[7][0] = 1470;
avg_train_data[1].micrometers_per_tick[10][0] = 3078;
avg_train_data[1].micrometers_per_tick[14][0] = 5827;

avg_train_data[1].micrometers_per_tick[7][1] = 1699;
avg_train_data[1].micrometers_per_tick[10][1] = 3365;
*/
    // TRAIN 74
    train_data[2][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_LOW][0] = 3581;
    train_data[2][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_MID][0] = 4424;
    train_data[2][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_HIGH][0] = 5371;
    train_data[2][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_LOW][0] = 3077;
    //train_data[2][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_MID][0] = ;
    //train_data[2][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_HIGH][0] = ;
    train_data[2][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_LOW][0] = 3008;
    train_data[2][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_MID][0] = 4225;
    train_data[2][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_HIGH][0] = 5371;
    train_data[2][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_LOW][0] = 3250;
    train_data[2][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_MID][0] = 3944;
    train_data[2][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_HIGH][0] = 5014;
    train_data[2][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_LOW][0] = 3258;
    train_data[2][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_MID][0] = 5611;
    train_data[2][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_HIGH][0] = 5771;
    train_data[2][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_LOW][0] = 4070;
    train_data[2][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_MID][0] = 4070;
    train_data[2][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_HIGH][0] = 5453;
    train_data[2][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_LOW][0] = 3133;
    train_data[2][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_MID][0] = 5222;
    train_data[2][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_HIGH][0] = 5222;
    //train_data[2][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_LOW][0] = ;
    train_data[2][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_MID][0] = 4357;
    train_data[2][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_HIGH][0] = 5107;
    train_data[2][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_LOW][0] = 3205;
    train_data[2][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_MID][0] = 3917;
    train_data[2][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_HIGH][0] = 3972;
    train_data[2][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_LOW][0] = 3279;
    train_data[2][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_MID][0] = 5222;
    train_data[2][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_HIGH][0] = 5423;
    train_data[2][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_LOW][0] = 3285;
    train_data[2][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_MID][0] = 4753;
    train_data[2][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_HIGH][0] = 5771;
    train_data[2][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_LOW][0] = 2919;
    train_data[2][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_MID][0] = 4986;
    train_data[2][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_HIGH][0] = 5129;
    
    avg_train_data[2].micrometers_per_tick[SPEED_LOW][0] = 3279;
    avg_train_data[2].micrometers_per_tick[SPEED_MID][0] = 4612;
    avg_train_data[2].micrometers_per_tick[SPEED_HIGH][0] = 5237;

    avg_train_data[2].stopping_distance_mm[SPEED_LOW][0] = 431;
    avg_train_data[2].stopping_distance_mm[SPEED_MID][0] = 607;
    avg_train_data[2].stopping_distance_mm[SPEED_HIGH][0] = 785;

    // from above

    avg_train_data[2].micrometers_per_tick[SPEED_LOW][1] = 3479; // est
    avg_train_data[2].micrometers_per_tick[SPEED_MID][1] = 4812; // est

    avg_train_data[2].stopping_distance_mm[SPEED_LOW][1] = 431; // tbd
    avg_train_data[2].stopping_distance_mm[SPEED_MID][1] = 607; // tbd

    // for unmeasured trains use some random acceleration data
    // NOT actual data for this train
    avg_train_data[2].acceleration[0][7][0] = 10;
    avg_train_data[2].acceleration[0][10][0] = 30;
    avg_train_data[2].acceleration[0][14][0] = 20;
    avg_train_data[2].acceleration[0][7][1] = 10;
    avg_train_data[2].acceleration[0][10][1] = 30;
    avg_train_data[2].acceleration[0][14][1] = 20;

    avg_train_data[2].acceleration[7][0][0] = -8;
    avg_train_data[2].acceleration[7][10][0] = 4;
    avg_train_data[2].acceleration[7][14][0] = 6;
    avg_train_data[2].acceleration[7][0][1] = -7;
    avg_train_data[2].acceleration[7][10][1] = 4;
    avg_train_data[2].acceleration[7][14][1] = 6;
    
    avg_train_data[2].acceleration[10][0][0] = -12;
    avg_train_data[2].acceleration[10][7][0] = -7;
    avg_train_data[2].acceleration[10][14][0] = 5;
    avg_train_data[2].acceleration[10][0][1] = -9;
    avg_train_data[2].acceleration[10][7][1] = -7;
    avg_train_data[2].acceleration[10][14][1] = 5;
 
    avg_train_data[2].acceleration[14][0][0] = -13;
    avg_train_data[2].acceleration[14][7][0] = -10;
    avg_train_data[2].acceleration[14][10][0] = -10;

    // TRAIN 78
    train_data[3][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_LOW][0] = 1266;
    train_data[3][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_MID][0] = 3008;
    train_data[3][sensor_name_to_id("C9")][sensor_name_to_id("B15")].micrometers_per_tick[SPEED_HIGH][0] = 5296;
    //train_data[3][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_LOW][0] = ;
    //train_data[3][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_MID][0] = ;
    //train_data[3][sensor_name_to_id("B15")][sensor_name_to_id("A3")].micrometers_per_tick[SPEED_HIGH][0] = ;
    train_data[3][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_LOW][0] = 1266;
    train_data[3][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_MID][0] = 2705;
    train_data[3][sensor_name_to_id("A3")][sensor_name_to_id("C11")].micrometers_per_tick[SPEED_HIGH][0] = 5296;
    train_data[3][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_LOW][0] = 1240;
    train_data[3][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_MID][0] = 2877;
    train_data[3][sensor_name_to_id("C11")][sensor_name_to_id("B5")].micrometers_per_tick[SPEED_HIGH][0] = 4875;
    train_data[3][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_LOW][0] = 1270;
    train_data[3][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_MID][0] = 2525;
    train_data[3][sensor_name_to_id("B5")][sensor_name_to_id("D3")].micrometers_per_tick[SPEED_HIGH][0] = 4539;
    train_data[3][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_LOW][0] = 1332;
    train_data[3][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_MID][0] = 2676;
    train_data[3][sensor_name_to_id("D3")][sensor_name_to_id("E5")].micrometers_per_tick[SPEED_HIGH][0] = 5352;
    train_data[3][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_LOW][0] = 1324;
    train_data[3][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_MID][0] = 3169;
    train_data[3][sensor_name_to_id("E5")][sensor_name_to_id("D6")].micrometers_per_tick[SPEED_HIGH][0] = 5222;
    train_data[3][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_LOW][0] = 1248;
    train_data[3][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_MID][0] = 2640;
    train_data[3][sensor_name_to_id("D6")][sensor_name_to_id("E10")].micrometers_per_tick[SPEED_HIGH][0] = 5107;
    train_data[3][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_LOW][0] = 1318;
    train_data[3][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_MID][0] = 2636;
    train_data[3][sensor_name_to_id("E10")][sensor_name_to_id("E13")].micrometers_per_tick[SPEED_HIGH][0] = 5321;
    train_data[3][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_LOW][0] = 1312;
    train_data[3][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_MID][0] = 3169;
    train_data[3][sensor_name_to_id("E13")][sensor_name_to_id("D13")].micrometers_per_tick[SPEED_HIGH][0] = 5127;
    train_data[3][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_LOW][0] = 1255;
    train_data[3][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_MID][0] = 2806;
    train_data[3][sensor_name_to_id("D13")][sensor_name_to_id("B2")].micrometers_per_tick[SPEED_HIGH][0] = 4539;
    train_data[3][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_LOW][0] = 1255;
    train_data[3][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_MID][0] = 2493;
    train_data[3][sensor_name_to_id("B2")][sensor_name_to_id("C9")].micrometers_per_tick[SPEED_HIGH][0] = 4986;
    
    avg_train_data[3].micrometers_per_tick[SPEED_LOW][0] = 1281;
    avg_train_data[3].micrometers_per_tick[SPEED_MID][0] = 2791;
    avg_train_data[3].micrometers_per_tick[SPEED_HIGH][0] = 5060;

    avg_train_data[3].stopping_distance_mm[SPEED_LOW][0] = 104;
    avg_train_data[3].stopping_distance_mm[SPEED_MID][0] = 333;
    avg_train_data[3].stopping_distance_mm[SPEED_HIGH][0] = 964;

    // from above

    avg_train_data[3].micrometers_per_tick[SPEED_LOW][1] = 1481; // est
    avg_train_data[3].micrometers_per_tick[SPEED_MID][1] = 2991; // est

    avg_train_data[3].stopping_distance_mm[SPEED_LOW][1] = 104; // tbd
    avg_train_data[3].stopping_distance_mm[SPEED_MID][1] = 333; // tbd
   
    avg_train_data[3].acceleration[0][7][0] = 12;
    avg_train_data[3].acceleration[0][10][0] = 37;
    avg_train_data[3].acceleration[0][14][0] = 18;
    avg_train_data[3].acceleration[0][7][1] = 12;
    avg_train_data[3].acceleration[0][10][1] = 37;
    avg_train_data[3].acceleration[0][14][1] = 18;
   

    avg_train_data[3].acceleration[7][0][0] = -8;
    avg_train_data[3].acceleration[7][10][0] = 4;
    avg_train_data[3].acceleration[7][14][0] = 6;
    avg_train_data[3].acceleration[7][0][1] = -7;
    avg_train_data[3].acceleration[7][10][1] = 4;
    avg_train_data[3].acceleration[7][14][1] = 6;
    
    avg_train_data[3].acceleration[10][0][0] = -12;
    avg_train_data[3].acceleration[10][7][0] = -7;
    avg_train_data[3].acceleration[10][14][0] = 5;
    avg_train_data[3].acceleration[10][0][1] = -9;
    avg_train_data[3].acceleration[10][7][1] = -7;
    avg_train_data[3].acceleration[10][14][1] = 5;
 
    avg_train_data[3].acceleration[14][0][0] = -13;
    avg_train_data[3].acceleration[14][7][0] = -10;
    avg_train_data[3].acceleration[14][10][0] = -10;

    // TRAIN 2
// commented out so we can demo dynamic recalibration
/*
train_data[4][2][42].micrometers_per_tick[7] = 1000;
train_data[4][2][42].micrometers_per_tick[10] = 4310;
train_data[4][2][42].micrometers_per_tick[14] = 4000;
train_data[4][19][40].micrometers_per_tick[7] = 1000;
train_data[4][19][40].micrometers_per_tick[10] = 4722;
train_data[4][19][40].micrometers_per_tick[14] = 4000;
train_data[4][30][2].micrometers_per_tick[7] = 1000;
train_data[4][30][2].micrometers_per_tick[10] = 4731;
train_data[4][30][2].micrometers_per_tick[14] = 4000;
train_data[4][32][19].micrometers_per_tick[7] = 1000;
train_data[4][32][19].micrometers_per_tick[10] = 4554;
train_data[4][32][19].micrometers_per_tick[14] = 4000;
train_data[4][40][30].micrometers_per_tick[7] = 1000;
train_data[4][40][30].micrometers_per_tick[10] = 4620;
train_data[4][40][30].micrometers_per_tick[14] = 4000;
train_data[4][42][79].micrometers_per_tick[7] = 1000;
train_data[4][42][79].micrometers_per_tick[10] = 4568;
train_data[4][42][79].micrometers_per_tick[14] = 4000;
train_data[4][48][32].micrometers_per_tick[7] = 1000;
train_data[4][48][32].micrometers_per_tick[10] = 3342;
train_data[4][48][32].micrometers_per_tick[14] = 4000;
train_data[4][52][69].micrometers_per_tick[7] = 1000;
train_data[4][52][69].micrometers_per_tick[10] = 3540;
train_data[4][52][69].micrometers_per_tick[14] = 4000;
train_data[4][61][77].micrometers_per_tick[7] = 1000;
train_data[4][61][77].micrometers_per_tick[10] = 2084;
train_data[4][61][77].micrometers_per_tick[14] = 4000;
train_data[4][64][32].micrometers_per_tick[7] = 1000;
train_data[4][64][32].micrometers_per_tick[10] = 4733;
train_data[4][64][32].micrometers_per_tick[14] = 4000;
train_data[4][66][48].micrometers_per_tick[7] = 1000;
train_data[4][66][48].micrometers_per_tick[10] = 3644;
train_data[4][66][48].micrometers_per_tick[14] = 4000;
train_data[4][69][66].micrometers_per_tick[7] = 1000;
train_data[4][69][66].micrometers_per_tick[10] = 3330;
train_data[4][69][66].micrometers_per_tick[14] = 4000;
train_data[4][72][52].micrometers_per_tick[7] = 1000;
train_data[4][72][52].micrometers_per_tick[10] = 3434;
train_data[4][72][52].micrometers_per_tick[14] = 4000;
train_data[4][77][72].micrometers_per_tick[7] = 1000;
train_data[4][77][72].micrometers_per_tick[10] = 3320;
train_data[4][77][72].micrometers_per_tick[14] = 4000;
train_data[4][79][64].micrometers_per_tick[7] = 1000;
train_data[4][79][64].micrometers_per_tick[10] = 4260;
train_data[4][79][64].micrometers_per_tick[14] = 4000;
*/
    avg_train_data[4].micrometers_per_tick[SPEED_LOW][0] = 1000; // ESTIMATE
    avg_train_data[4].micrometers_per_tick[SPEED_MID][0] = 2000; // ESTIMATE
    avg_train_data[4].micrometers_per_tick[SPEED_HIGH][0] = 4000; // ESTIMATE

    avg_train_data[4].stopping_distance_mm[SPEED_LOW][0] = 484;
    avg_train_data[4].stopping_distance_mm[SPEED_MID][0] = 682;
    avg_train_data[4].stopping_distance_mm[SPEED_HIGH][0] = 951;

    // from above

    avg_train_data[4].micrometers_per_tick[SPEED_LOW][1] = 1200; // ESTIMATE
    avg_train_data[4].micrometers_per_tick[SPEED_MID][1] = 2200; // ESTIMATE

    avg_train_data[4].stopping_distance_mm[SPEED_LOW][1] = 484; // tbd
    avg_train_data[4].stopping_distance_mm[SPEED_MID][1] = 682; // tbd

    // for unmeasured trains use some random acceleration data
    // NOT actual data for this train
    avg_train_data[4].acceleration[0][7][0] = 10;
    avg_train_data[4].acceleration[0][10][0] = 30;
    avg_train_data[4].acceleration[0][14][0] = 20;
    avg_train_data[4].acceleration[0][7][1] = 10;
    avg_train_data[4].acceleration[0][10][1] = 30;
    avg_train_data[4].acceleration[0][14][1] = 20;

    avg_train_data[4].acceleration[7][0][0] = -8;
    avg_train_data[4].acceleration[7][10][0] = 4;
    avg_train_data[4].acceleration[7][14][0] = 6;
    avg_train_data[4].acceleration[7][0][1] = -7;
    avg_train_data[4].acceleration[7][10][1] = 4;
    avg_train_data[4].acceleration[7][14][1] = 6;
    
    avg_train_data[4].acceleration[10][0][0] = -12;
    avg_train_data[4].acceleration[10][7][0] = -7;
    avg_train_data[4].acceleration[10][14][0] = 5;
    avg_train_data[4].acceleration[10][0][1] = -9;
    avg_train_data[4].acceleration[10][7][1] = -7;
    avg_train_data[4].acceleration[10][14][1] = 5;
 
    avg_train_data[4].acceleration[14][0][0] = -13;
    avg_train_data[4].acceleration[14][7][0] = -10;
    avg_train_data[4].acceleration[14][10][0] = -10;
}

static int last_update_time[NUM_TRAINS] = {0};

int get_train_index(int train){
	int i = -1;
    switch(train) {
        case 24:
            	i = 0;
		break;
        case 58:
        	i = 1;
		break;
	case 74:
        	i = 2;
		break;
	case 78:
    		i = 3;
		break;
	case 2:
		i = 4;
		break;	
    }
   return i; 
}


static void init_velocity(struct train_data* data, struct train_data* reverse_data, int speed, int fromAbove, int i){
	if(data->micrometers_per_tick[speed][fromAbove] == 0){
		data->micrometers_per_tick[speed][fromAbove] = reverse_data->micrometers_per_tick[speed][fromAbove];
	}
	if(data->micrometers_per_tick[speed][fromAbove] == 0){
                data->micrometers_per_tick[speed][fromAbove] = avg_train_data[i].micrometers_per_tick[speed][fromAbove];
        }
}

static void init_acceleration(struct train_data* data, struct train_data* reverse_data, int start_speed, int end_speed, int fromAbove, int i){
	if(data->acceleration[start_speed][end_speed][fromAbove] == 0){
		data->acceleration[start_speed][end_speed][fromAbove] = reverse_data->acceleration[start_speed][end_speed][fromAbove];
	}

	if(data->acceleration[start_speed][end_speed][fromAbove] == 0){
                data->acceleration[start_speed][end_speed][fromAbove] = avg_train_data[i].acceleration[start_speed][end_speed][fromAbove];
        }
}

static void init_stop_distances(struct train_data* data, struct train_data* reverse_data, int start_speed, int fromAbove, int i){
	if(data->stopping_distance_mm[start_speed][fromAbove] == 0){
		data->stopping_distance_mm[start_speed][fromAbove] = reverse_data->stopping_distance_mm[start_speed][fromAbove];
	}

	if(data->stopping_distance_mm[start_speed][fromAbove] == 0){
                data->stopping_distance_mm[start_speed][fromAbove] = avg_train_data[i].stopping_distance_mm[start_speed][fromAbove];
        }
}

const struct train_data *getTrainData(int train, int cur_sensor, int next_sensor, int termAdmin) {
	int i = get_train_index(train);
	if(i < 0){
		assert(termAdmin, i >= 0, "unsupported train", 17);
		return 0;
	}
    struct train_data* data = &train_data[i][cur_sensor][next_sensor];
    struct train_data* reverse_data = &train_data[i][get_reverse_sensor(next_sensor)][get_reverse_sensor(cur_sensor)];

    // if the data is not initialized, set it to the avg data
    init_velocity(data, reverse_data, SPEED_LOW, 0, i);
    init_velocity(data, reverse_data, SPEED_MID, 0, i);
    init_velocity(data, reverse_data, SPEED_HIGH, 0, i);
    init_velocity(data, reverse_data, SPEED_LOW, 1, i);
    init_velocity(data, reverse_data, SPEED_MID, 1, i);
    init_velocity(data, reverse_data, SPEED_HIGH, 1, i);

    init_acceleration(data, reverse_data, SPEED_LOW, SPEED_HIGH, 0, i);
    init_acceleration(data, reverse_data, SPEED_LOW, SPEED_MID, 0, i);
    init_acceleration(data, reverse_data, SPEED_LOW, 0, 0, i);
    init_acceleration(data, reverse_data, SPEED_MID, SPEED_HIGH, 0, i);
    init_acceleration(data, reverse_data, SPEED_MID, SPEED_LOW, 0, i);
    init_acceleration(data, reverse_data, SPEED_MID, 0, 0, i);
    init_acceleration(data, reverse_data, SPEED_HIGH, SPEED_MID, 0, i);
    init_acceleration(data, reverse_data, SPEED_HIGH, SPEED_LOW, 0, i);
    init_acceleration(data, reverse_data, SPEED_HIGH, 0, 0, i);
    init_acceleration(data, reverse_data, 0, SPEED_HIGH, 0, i);
    init_acceleration(data, reverse_data, 0, SPEED_MID, 0, i);
    init_acceleration(data, reverse_data, 0, SPEED_LOW, 0, i);

    init_acceleration(data, reverse_data, SPEED_LOW, SPEED_HIGH, 1, i);
    init_acceleration(data, reverse_data, SPEED_LOW, SPEED_MID, 1, i);
    init_acceleration(data, reverse_data, SPEED_LOW, 0, 1, i);
    init_acceleration(data, reverse_data, SPEED_MID, SPEED_HIGH, 1, i);
    init_acceleration(data, reverse_data, SPEED_MID, SPEED_LOW, 1, i);
    init_acceleration(data, reverse_data, SPEED_MID, 0, 1, i);
    init_acceleration(data, reverse_data, 0, SPEED_HIGH, 1, i);
    init_acceleration(data, reverse_data, 0, SPEED_MID, 1, i);
    init_acceleration(data, reverse_data, 0, SPEED_LOW, 1, i);

    init_stop_distances(data, reverse_data, SPEED_LOW, 0, i);
    init_stop_distances(data, reverse_data, SPEED_MID, 0, i);
    init_stop_distances(data, reverse_data, SPEED_HIGH, 0, i);
    init_stop_distances(data, reverse_data, SPEED_LOW, 1, i);
    init_stop_distances(data, reverse_data, SPEED_MID, 1, i);
    init_stop_distances(data, reverse_data, SPEED_HIGH, 1, i);
    return data;
}

// update the train's velocity using the difference between expected and actual times
void updateTrainVelocity(int train, int cur_sensor, int next_sensor, int dist, int speed, int fromAbove, int time_diff, int curTime, int termAdmin){
	int i = get_train_index(train);
	if(i < 0){
		assert(termAdmin, i >= 0, "unsupported train ", 18);
		term_print_int(termAdmin, train);
		return;
	}

	// TODO: maybe change this once we have some accel data
	if(curTime - last_update_time[i] < 500){
		//debug_assert(termAdmin, 0, "train speed recently updated", 28);
		return;
	}
	
	if(speed != 0 && speed != SPEED_LOW && speed != SPEED_MID && speed != SPEED_HIGH){
		assert(termAdmin, 0, "unsupported speed", 17);
		return;
	}
	struct train_data* data = &train_data[i][cur_sensor][next_sensor];
    	struct train_data* reverse_data = &train_data[i][get_reverse_sensor(next_sensor)][get_reverse_sensor(cur_sensor)];

	// initialize if not initialized
	init_velocity(data, reverse_data, speed, fromAbove, i);

	int expected_velocity = data->micrometers_per_tick[speed][fromAbove];
	int expected_time = dist * 1000 / expected_velocity;
	int actual_time = expected_time + time_diff;
	int actual_velocity = dist * 1000 / actual_time;

	// exponential weighted average
	int new_velocity = (expected_velocity + actual_velocity) / 2;
	data->micrometers_per_tick[speed][fromAbove] = new_velocity;
	reverse_data->micrometers_per_tick[speed][fromAbove] = new_velocity;
}


void updateAcceleration(int train, int cur_sensor, int next_sensor, int start_speed, int end_speed, int fromAbove, int acceleration, int termAdmin){
	int i = get_train_index(train);
	if(i < 0){
		assert(termAdmin, i >= 0, "unsupported train", 17);
		return;
	}

	if(start_speed != 0 && start_speed != SPEED_LOW && start_speed != SPEED_MID && start_speed != SPEED_HIGH){
		assert(termAdmin, 0, "unsupported start_speed", 17);
		return;
	}
	
	if(end_speed != 0 && end_speed != SPEED_LOW && end_speed != SPEED_MID && end_speed != SPEED_HIGH){
		assert(termAdmin, 0, "unsupported end_speed", 17);
		return;
	}
	
	struct train_data* data = &train_data[i][cur_sensor][next_sensor];
    	struct train_data* reverse_data = &train_data[i][get_reverse_sensor(next_sensor)][get_reverse_sensor(cur_sensor)];

	// initialize if not initialized
	init_acceleration(data, reverse_data, start_speed, end_speed, fromAbove, i);

	int new_acceleration;
	if(data->acceleration[start_speed][end_speed][fromAbove] != 0){
		// exponential weighted average
		new_acceleration = (data->acceleration[start_speed][end_speed][fromAbove] + acceleration) / 2;
	} else {
		new_acceleration = acceleration;
	}

	data->acceleration[start_speed][end_speed][fromAbove] = new_acceleration;
	reverse_data->acceleration[start_speed][end_speed][fromAbove] = new_acceleration;
}

void setUpdateTime(int train, int curTime) {
        last_update_time[get_train_index(train)] = curTime;
}
