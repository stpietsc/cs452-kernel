#include "../track_data.h"
#include "../track.h"
#include "../syscall.h"

static track_node track_graph[TRACK_MAX];

// should be called once after the track type is determined in CommandBuffer
void setupTrack(enum Track track, int trackAdmin) {
    if (track == TRACK_A) {
        init_tracka(track_graph);
    } else { // must be TRACK_B
        init_trackb(track_graph);
    }
    init_broken_switches();
    struct set_switch *sw;
    enum MessageType type;
    for (int i = 0; i < NUM_BROKEN_SWITCHES; ++i) {
        sw = getBrokenSwitch(i);
        if (track == sw->track) {
            // ensure Dijkstra knows about the broken switch
            track_graph[sw->track_id].brokenPosn = sw->posn;
            // for each broken switch, explicitly set it to the broken position
            // this ensures that the terminal screen shows the correct position 
            // and the middle switches are not in an invalid position
            int cmd[2];
            cmd[0] = sw->num;
            cmd[1] = sw->posn;
            TypedSend(trackAdmin, BROKEN_SWITCH, (char*) cmd, sizeof(cmd), &type, "", 0);
        }
    }
}

track_node getLoopNode() {
    return track_graph[2]; // TEMPORARY, returns A3. may or not be correct depending on what loop we choose
}

// only used by PathServer
void setDist(int idx, int dist) {
    track_graph[idx].distance = dist;
}

int getDist(int idx) {
    return track_graph[idx].distance;
}

track_node *getNode(int idx) {
    return &track_graph[idx];
}
