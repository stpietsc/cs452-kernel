#pragma once

#define LARGEST_TRAIN_NUM 99
#define NUM_SUPPORTED_SPEEDS 15
#define SPEED_LOW 7
#define SPEED_MID 10
#define SPEED_HIGH 14

#define NUM_TRAINS 5

struct train_position {
    int nodeId;
    int time;
};

struct train_data {
    int micrometers_per_tick[NUM_SUPPORTED_SPEEDS][2];
    int stopping_distance_mm[NUM_SUPPORTED_SPEEDS][2];
    int acceleration[NUM_SUPPORTED_SPEEDS][NUM_SUPPORTED_SPEEDS][2]; // currently um/ticks^2, TODO: improve granularity?
};

void init_train_data();
#ifdef CALIBRATE
void print_train_data();
#endif

int get_train_index(int train);
const struct train_data *getTrainData(int train, int cur_sensor, int next_sensor, int sendServer);
void updateTrainVelocity(int train, int cur_sensor, int next_sensor, int dist, int speed, int fromAbove, int time_diff, int curTime, int termAdmin);
void updateAcceleration(int train, int cur_sensor, int next_sensor, int start_speed, int end_speed, int fromAbove, int acceleration, int termAdmin);
void setUpdateTime(int train, int curTime);

