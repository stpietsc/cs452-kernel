#include "io_helpers.h"
#include "syscall.h"
#include "userTasks.h"
#include "interrupts.h"
#include "priorities.h"

static void createUartServers() {
    int tid;
    enum MessageType type;
    struct UARTReadArgs args;

    Create(UARTSERVER_PRIORITY, TrainReadServer);
    // train server
    args.uart = 1;
    args.delay = 1;
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == UARTSERVER_ARGS, "Incorrect config request\r\n", 26);
    TypedReply(tid, SUCCESS, (char*) &args, sizeof(args));

    Create(UARTSERVER_PRIORITY, UARTReadServer);
    // terminal server
    args.uart = 0;
    args.delay = 2;
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == UARTSERVER_ARGS, "Incorrect config request\r\n", 26);
    TypedReply(tid, SUCCESS, (char*) &args, sizeof(args));

    int uart = 0;
    Create(UARTSERVER_PRIORITY, UARTSendServer);
    // terminal server
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == UARTSERVER_ARGS, "Incorrect config request\r\n", 26);
    TypedReply(tid, SUCCESS, (char*) &uart, sizeof(uart));

    uart = 1;
    Create(UARTSERVER_PRIORITY, UARTSendServer);
    // train server
    TypedReceive(&tid, &type, "", 0);
    debug_assert(type == UARTSERVER_ARGS, "Incorrect config request\r\n", 26);
    TypedReply(tid, SUCCESS, (char*) &uart, sizeof(uart));
}

void firstUserTask() {
    #ifdef TIMING
    Create(0, run_performance_tests); 
    #else
    Create(NAMESERVER_PRIORITY, NameServer); // 1
    Create(CLOCKSERVER_PRIORITY, ClockServer);
    createUartServers();

    Create(PROPRIETOR_PRIORITY, TerminalAdmin);
    Create(PROPRIETOR_PRIORITY, TrainProprietor);

    // create TrainAdmin, TrackAdmin, SensorAdmin
    Create(ADMIN_PRIORITY, TrainAdmin);
    Create(ADMIN_PRIORITY, TrackAdmin);
    Create(ADMIN_PRIORITY, SensorAdmin);
    Create(CLIENT_PRIORITY, ReservationManager);
    
    Create(CLIENT_PRIORITY_HIGH, PathManager);

    Create(CLIENT_PRIORITY, CommandBuffer);

    Create(CLIENT_PRIORITY_LOW, IdleServer);
    
    Create(IDLE_PRIORITY, IdleTask);

    #endif
}

