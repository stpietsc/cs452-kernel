#pragma once

#include "message_types.h"

void firstUserTask();

// servers
void RPSServer();
void NameServer();
void ClockServer();
void UARTReadServer();
void TrainReadServer();
void UARTSendServer();
void IdleServer();
// admins/proprietors
void TerminalAdmin();
void TrainProprietor();
void TrainAdmin();
void SensorAdmin();
void TrackAdmin();
// managers
void ReservationManager();
void PathManager();

// clients
void CommandBuffer();
void PathFinder();
void PathPlanner();
void ExecutePath();
void CollisionAvoidance();

void IdleTask();

// workers
void Notifier();
void DelayCourier();
void Courier();
void Relay();
void AwaitSensor();

void stopOnSensor();
void accelOnSensor();

// previous assignments
void testTask();
void RPSClient_Standard();
void RPSClient_MultipleSignups();
void RPSClient_PlayWithoutSignup();
void RPSClient_InvalidPlayRequests();
void A();
void B();
void C();
void DelayTask();

#ifdef TIMING
void run_performance_tests();
#endif


// argument structures

struct NotifierArgs {
    int eventID;
    enum MessageType notificationType;
    int shouldWaitToEnable;
};

struct RelayArgs {
    const char* name;
    int maxMsgSize;
};

struct UARTReadArgs {
    int uart;
    int delay;
};

struct CourierArgs {
    int server;
    int maxMsgSize;
};

#define MAX_NUM_RESERVATIONS 60

struct Reservations {
    char trainNum[MAX_NUM_RESERVATIONS];
    char nodeIds[MAX_NUM_RESERVATIONS];
    char edgeIds[MAX_NUM_RESERVATIONS];
    char isReserving[MAX_NUM_RESERVATIONS];
    char len;
};
