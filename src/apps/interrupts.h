#pragma once

// Custom interrupt numbers
#define TERMINAL_RECEIVE_INTERRUPT 0
#define TERMINAL_TRANSMIT_INTERRUPT 1

#define TRAIN_RECEIVE_INTERRUPT 10
#define TRAIN_TRANSMIT_INTERRUPT 11
#define TRAIN_CTS_INTERRUPT 13 // actually a modem interrupt + CTS bit change, but oh well

#define enable_on_await(n) (n < 13)

// Hardware-defined interrupt numbers
#define TIMER_INTERRUPT 99
#define GPIO_INTERRUPT 145
