#include "../syscall.h"
#include "../userTasks.h"
#include "../io_helpers.h"

void Notifier(){
	struct NotifierArgs config;
	enum MessageType type;
	int server = MyParentTid();
	TypedSend(server, NOTIFIER_ARGS, "", 0, &type, (char *) &config, sizeof(config));
	debug_assert(type == SUCCESS, "Notifier failed to get arguments\r\n", 34);

	int missed;
	char resp[1];

	if (config.shouldWaitToEnable) {
		TypedSend(server, config.notificationType, "", 0, &type, resp, 1);
		debug_assert(type == SUCCESS, "Failed to notify server\r\n", 25);
	}

	while(1){
		missed = AwaitEvent(config.eventID);
		debug_assert(missed == 0, "Missed tick\r\n", 13);
		//if (config.eventID < 20) // is uart event
		//	debug("INTERRUPT\r\n", 11); 
		TypedSend(server, config.notificationType, "", 0, &type, resp, 1);
		debug_assert(type == SUCCESS, "Failed to notify server\r\n", 25);
	}
}
