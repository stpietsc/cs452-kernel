#include "../syscall.h"
#include "../io_helpers.h"

// should be created by the server requiring the courier
void DelayCourier() {
	int server = MyParentTid();

	int isDelayUntil;
	enum MessageType type;
	TypedSend(server, DELAY_ARGS, "", 0, &type, (char *) &isDelayUntil, 4);
	debug_assert(type == SUCCESS, "Courier failed to get arguments\r\n", 33);
	
	int clockServer = WhoIs("ClockServer");
	debug_assert(clockServer >= 0, "WhoIs clock failed\r\n", 20);

	int delay, time = 0;
	while(1){
		TypedSend(server, DELAY_QUERY, (char*) &time, 4, &type, (char *) &delay, 4);
		debug_assert (type == SUCCESS, "Failed to get delay\r\n", 21);
		if (isDelayUntil) {
			time = DelayUntil(clockServer, delay);
		} else {
			time = Delay(clockServer, delay);
		}
	}
}