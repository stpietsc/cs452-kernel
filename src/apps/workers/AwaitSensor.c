#include "../syscall.h"
#include "../sensors.h"
#include "../io_helpers.h"

static int getDistFromSurrounding(struct SurroundSensors *sSurround, int sensor) {
    for (int i = 0; i < sSurround->num; ++i) {
        if (sSurround->sensors[i] == sensor)
            return sSurround->dists[i];
    }
    return 0;
}

#define TIMEOUT_VAL 10
static int waitOnSurrounding(struct SurroundSensors *sSurround, int trackAdmin, int sensorAdmin, int termAdmin, int trainAdmin, int clockServer, int train, int node, int timeout) {
    enum MessageType type;
    struct SensorList sLst;
    int isStopped = 0;

    int cmdTime;
    int speed[2];
    int resp[3];
    // determine if train is stopped
    TypedSend(trainAdmin, GET_SPEED, (char*) &train, sizeof(int), &type, (char*) speed, sizeof(speed));
    if (speed[0] == 0) {
        TypedSend(trainAdmin, GET_LAST_CMD, (char*) &train, sizeof(int), &type, (char*) &resp, sizeof(resp));
        int curTime = Time(clockServer);
        if (curTime - cmdTime > ticks_to_stop(termAdmin, train, resp[1], resp[2])) {
            // Wait only on current and next sensor if stopped
            sLst.sensors[0] = node;
            int res[2];
            TypedSend(trackAdmin, NEXT_SENSOR, (char*) &node, sizeof(int), &type, (char*) &sLst.sensors[1], sizeof(int));
            sLst.num = 2;
            isStopped = 1;
        }
    }

    if (!isStopped) {
        TypedSend(trackAdmin, SURROUNDING_SENSORS, (char*) &node, 4, &type, (char*) sSurround, sizeof(struct SurroundSensors));
        assert(termAdmin, type == SUCCESS, "Next sensor failed", 18);

        // term_print(termAdmin, "SurSensors returned", 19);

        sLst.num = sSurround->num;
        for (int i = 0; i < sSurround->num; ++i) {
            sLst.sensors[i] = sSurround->sensors[i];
        }
    }
    
    // int num = 2;
    // sLst.sensors[0] = sSurround->current;
    // sLst.sensors[1] = sSurround->reverse;
    // if (sSurround->next[0] != -1) {
    //     sLst.sensors[num] = sSurround->next[0];
    //     ++num;
    // }
    // if (sSurround->next[1] != -1) {
    //     sLst.sensors[num] = sSurround->next[1];
    //     ++num;
    // }
    // if (sSurround->prev[0] != -1) {
    //     sLst.sensors[num] = sSurround->prev[0];
    //     ++num;
    // }
    // if (sSurround->prev[1] != -1) {
    //     sLst.sensors[num] = sSurround->prev[1];
    //     ++num;
    // }
    // sLst.num = num;
    for(int i = 0; i < sLst.num; ++i){
    	sLst.timeouts[i] = timeout;
    }

    // term_print(termAdmin, "Setup sLst", 10);
    // term_newline(termAdmin);
    // term_print(termAdmin, "Waiting on ", 11);
    // for (int i = 0; i < sLst.num; ++i) {
    //     term_print_int(termAdmin, sLst.sensors[i]);
    //     term_print(termAdmin, " ", 1);
    // }
    // term_newline(termAdmin);

    int res;
    // wait for it
    TypedSend(sensorAdmin, AWAIT_SENSORS, (char*) &sLst, sizeof(sLst), &type, (char*) &res, 4);
    if (type == ERROR)
        return -2;
    //assert(termAdmin, type == SUCCESS || type == SENSOR_TIMEOUT, "Await failed", 12);
    return res;
}

void AwaitSensor() {
    int trackAdmin = WhoIs("TrackAdmin");
    int sensorAdmin = WhoIs("SensorAdmin");
    int termAdmin = WhoIs("TermAdmin");
    int trainAdmin = WhoIs("TrainAdmin");
    int clockServer = WhoIs("ClockServer");

    static struct SurroundSensors sSurround;

    // term_print(termAdmin, "Set up await", 12);
    // term_newline(termAdmin);

    int lastSensor;
    int pathExec = MyParentTid();
    int sensor[2];
    enum MessageType type;

    int init[2];
    TypedSend(pathExec, AWAIT_SENSOR_INIT, "", 0, &type, (char*) init, sizeof(init));
    lastSensor = init[0];
    int train = init[1];

    while (1) {
        int res = waitOnSurrounding(&sSurround, trackAdmin, sensorAdmin, termAdmin, trainAdmin, clockServer, train, lastSensor, TIMEOUT_VAL);
        if(res == -1 || res == lastSensor) {
            //term_print(termAdmin, "sensor timeout", 14);
            //term_newline(termAdmin);
            continue;
        } else if (res == -2) {
            TypedSend(pathExec, SENSOR_RESERVE_FAIL, "", 0, &type, "", 0);
            continue;
        } else {
            // term_print_int(termAdmin, lastSensor);
            // term_print(termAdmin, " ", 1);
            // term_print_int(termAdmin, res);
            // term_newline(termAdmin);
            lastSensor = res;
        }

	/*
        term_print(termAdmin, "Received ", 9);
        term_print_int(termAdmin, lastSensor);
        term_newline(termAdmin);
	*/
        sensor[0] = lastSensor;
        sensor[1] = getDistFromSurrounding(&sSurround, lastSensor);

        // term_print(termAdmin, "blah blah", 9);
        // term_newline(termAdmin);

        TypedSend(pathExec, SENSOR_TRIGGERED, (char*) sensor, sizeof(sensor), &type, "", 0);
    }
}
