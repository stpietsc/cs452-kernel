#include "../syscall.h"
#include "../io_helpers.h"
#include "../userTasks.h"

// one-way non-blocking message transfer
void Courier() {
	int parent = MyParentTid();
    int termAdmin = WhoIs("TermAdmin");

    struct CourierArgs config;
	enum MessageType type;
	TypedSend(parent, COURIER_ARGS, "", 0, &type, (char *) &config, sizeof(config));
	debug_assert(type == SUCCESS, "Courier failed to get arguments\r\n", 33);

    char msg[config.maxMsgSize+1];
    char resp[config.maxMsgSize+1];
    int len;
    int rplen = 1;

    // use non-typed send here to reduce overhead
    // can still be accepted as typed messages on either end
	while(1){
        resp[0] = COURIER_QUERY;
		len = Send(parent, resp, rplen, msg, config.maxMsgSize+1);
        assert(termAdmin, len <= config.maxMsgSize+1, "Message too long", 16);
        rplen = Send(config.server, msg, len, resp, config.maxMsgSize+1);
        assert(termAdmin, rplen <= config.maxMsgSize+1, "Message too long", 16);
        if (rplen <= 0)
            rplen = 1;
	}
}