#include "../syscall.h"
#include "../userTasks.h"
#include "../io_helpers.h"

// public interface for a server
// allows the server to control when client requests are being accepted
void Relay() {
    struct RelayArgs config;
	enum MessageType type;
	int server = MyParentTid();
	TypedSend(server, RELAY_ARGS, "", 0, &type, (char *) &config, sizeof(config));
	debug_assert(type == SUCCESS, "Relay failed to get arguments\r\n", 31);

    int success = RegisterAs(config.name);
    debug_assert(success == 0, "RegisterAs failed\r\n", 19);

	int tid, len;
	char msg[config.maxMsgSize+1];
    char resp[config.maxMsgSize+1];

	// use non-typed send here to reduce overhead
    // can still be accepted as typed messages on either end
	while(1){
		len = Receive(&tid, msg, config.maxMsgSize+1);
		debug_assert(len <= config.maxMsgSize+1, "Message too long\r\n", 18);
        len = Send(server, msg, len, resp, config.maxMsgSize+1);
		debug_assert(len <= config.maxMsgSize+1, "Message too long\r\n", 18);
        Reply(tid, resp, len);
	}
}