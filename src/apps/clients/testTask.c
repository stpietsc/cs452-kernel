#include "../io_helpers.h"
#include "../syscall.h"

static void print_tids(){
	int tid = MyTid();
	int parentTid = MyParentTid();
	char msg1[] = "My task id: ";
	char msg2[] = ", My parent's task id: ";
	int writeServer = WhoIs("TermSendServer");
	Puts(writeServer, msg1, sizeof(msg1) - 1);
	print_int(writeServer, tid);
	Puts(writeServer, msg2, sizeof(msg2) - 1);
	print_int(writeServer, parentTid);
	Puts(writeServer, "\r\n", 2);
}

void testTask() {
	print_tids();
	Yield();
	print_tids();
	Exit();
}