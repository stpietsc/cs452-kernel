#include "../syscall.h"
#include "../io_helpers.h"

void DelayTask() {
    int args[2];
    int parent = MyParentTid();
    Send(parent, "", 0, (char*) args, 8);
    int clockServer = WhoIs("ClockServer");
    debug_assert(clockServer >= 0, "WhoIs clock failed\r\n", 20);
    int writeServer = WhoIs("TermSendServer");
    debug_assert(writeServer >= 0, "WhoIs termsend failed\r\n", 23);
    int myTid = MyTid();
    for (int i = 0; i < args[1]; ++i) {
        Delay(clockServer, args[0]);
        Puts(writeServer, "Task ID: ", 9);
        print_int(writeServer, myTid);
        Puts(writeServer, " | Delay interval: ", 19);
        print_int(writeServer, args[0]);
        Puts(writeServer, " | ", 3);
        print_int(writeServer, i + 1);
        Puts(writeServer, " delays completed\r\n", 19);
    }
}
