#include "../syscall.h"
#include "../io_helpers.h"

void stopOnSensor() {
	int trainAdmin = WhoIs("TrainAdmin");
	int sensorAdmin = WhoIs("SensorAdmin");

	int args[2];
	enum MessageType type;
	TypedSend(trainAdmin, STOP_ON_SENSOR_ARGS, "", 0, &type, (char *) args, sizeof(args));
	
	TypedSend(sensorAdmin, AWAIT_SENSOR, (char*) &args[1], 4, &type, "", 0);
	args[1] = 0;
	TypedSend(trainAdmin, SET_SPEED, (char*) args, 8, &type, "", 0);
}
