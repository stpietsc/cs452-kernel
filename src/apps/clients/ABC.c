#include "../io_helpers.h"
#include "../syscall.h"

// tid 2
void A(){
	int clockServer = WhoIs("ClockServer");
	Delay(clockServer, 10);
	// int writeServer = WhoIs("TrainSendServer");
	// char s = 0x21;
	// char c = 0x22;
	// char stop = 0;
	// char go = 10;
    //     char commands[4];
	// commands[0] = stop;
	// commands[1] = 78;
	// commands[2] = stop;
	// commands[3] = 58;
    
	// Puts(writeServer, commands, 4);

	// Delay(clockServer, 300);
	// commands[0] = go;
	// commands[2] = go;
	
	// Puts(writeServer, commands, 4);

	// Delay(clockServer, 300);
	// commands[0] = stop;
	// commands[2] = stop;
	
	// Puts(writeServer, commands, 4);


	int writeServer = WhoIs("TermSendServer");
	debug_assert(writeServer>=0, "Uh oh\r\n", 7);
	int readServer = WhoIs("TermReadServer");
    //char longmsg[] = "a b c d e f g h i j k l m n o p q r s t u v w x y za b c d e f g h i j k l m n o p q r s t u v w x y za b c d e f g h i j k l m n o p q r s t u v w x y za b c d e f g h i j k l m n o p q r s t u v w x y za b c d e f g h i j k l m n o p q r s t u v w x y z\r\n";
    for (;;) {
		char msg[10];
	 	int len = Gets(readServer, msg, 10, 100);
		print_int(writeServer, len);
		Puts(writeServer, msg, 10);
		Puts(writeServer, "\r\n", 2);
	    //	Puts(writeServer, "got ", 4);
		//Puts(writeServer, &c, 1);
		//Puts(writeServer, "\r\n", 2);
		//Puts(writeServer, longmsg, sizeof(longmsg) - 1);
		//Delay(clockServer, 1);
	}

	// // wait until name is registered
	// int tid = -1;
	// while (tid < 0)
	// 	tid = WhoIs("C");
	
	// int writeServer = WhoIs("TermSendServer");
	// debug_assert(writeServer >= 0, "WhoIs termsend failed\r\n", 23);

	// Puts(writeServer, "C: ", 3);
	// print_int(writeServer, tid);

	// RegisterAs("A");
	
	// char reply[10];
	// Send(tid, "", 0, reply, 10);

	// Puts(writeServer, "A sent\r\n", 8);

	// char msg[10];
	// int tid1;
	// Receive(&tid1, msg, 10);

	// Puts(writeServer, "A received\r\n", 12);

	// Reply(tid1, "", 0);

	// Puts(writeServer, "A done", 6);
}

// tid 3
void B(){

	int tid = -1;
	while (tid < 0)
		tid = WhoIs("A");

	int writeServer = WhoIs("TermSendServer");
	debug_assert(writeServer >= 0, "WhoIs termsend failed\r\n", 23);

	Puts(writeServer, "A: ", 3);
	print_int(writeServer, tid);

	char reply[10];
	Send(tid, "", 0, reply, 10);
	Puts(writeServer, "B done", 6);
}

// tid 4
void C(){

	RegisterAs("C");

	int writeServer = WhoIs("TermSendServer");
	debug_assert(writeServer >= 0, "WhoIs termsend failed\r\n", 23);

	char msg[10];
	int tid1;
	Receive(&tid1, msg, 10);

	Puts(writeServer, "C received\r\n", 12);

	Reply(tid1, "", 0);
	Puts(writeServer, "C done", 6);
}
