#include "../path.h"
#include "../train_data.h"
#include "../io_helpers.h"
#include "../track.h"
#include "../syscall.h"
#include "../velocity_helpers.h"

#define SWITCH_DELAY -100

#define START_DELAY 0
#define REVERSE_DELAY 1000

// returns 0 if no suitable speed was found (path too short)
static int determineSpeeds(int termAdmin, int totalDist, int train, int startSpeed, int startFromAbove, int startSensor, int nextSensor, int* targetSpeed, int* targetFromAbove) {
    // term_print(termAdmin, "Start: ", 7);
    // term_print_int(termAdmin, startSpeed);
    // term_print(termAdmin, " tot dist ", 10);
    // term_print_int(termAdmin, totalDist);
    // term_newline(termAdmin);
    // TODO: handle case where path isn't long enough for starting speed
    // try to go speed 14 first
    // int accelDist_micrometers = distance_micrometers_acceleration(termAdmin, train, startSpeed, startFromAbove, SPEED_HIGH, startSensor, nextSensor);
    // int slowDownDist_micrometers = distance_micrometers_acceleration(termAdmin, train, SPEED_HIGH, 0, SPEED_LOW, startSensor, nextSensor);
    // int stopDistance_mm = getTrainData(train, startSensor, nextSensor, termAdmin)->stopping_distance_mm[SPEED_LOW][1];
    
    // // term_print_int(termAdmin, (accelDist_micrometers + slowDownDist_micrometers) / 1000 + stopDistance_mm);
    // // term_newline(termAdmin);
    // if ((accelDist_micrometers + slowDownDist_micrometers) / 1000 + stopDistance_mm < totalDist) {
    //     *targetSpeed = SPEED_HIGH;
    //     *targetFromAbove = 0;
    //     return 1;
    // }

    // try to go speed 10
    int accelDist_micrometers = distance_micrometers_acceleration(termAdmin, train, startSpeed, startFromAbove, SPEED_MID, startSensor, nextSensor);
    int fromAbove = (startSpeed > SPEED_MID) || (startSpeed == SPEED_MID && startFromAbove);
    int stopDistance_mm = getTrainData(train, startSensor, nextSensor, termAdmin)->stopping_distance_mm[SPEED_MID][fromAbove];
    // term_print(termAdmin, "10: ", 4);
    // term_print_int(termAdmin, (accelDist_micrometers) / 1000 + stopDistance_mm);
    // term_newline(termAdmin);
    if ((accelDist_micrometers) / 1000 + stopDistance_mm < totalDist) {
        *targetSpeed = SPEED_MID;
        *targetFromAbove = fromAbove;
        return 1;
    }

    // try to go speed 7
    accelDist_micrometers = distance_micrometers_acceleration(termAdmin, train, startSpeed, startFromAbove, SPEED_LOW, startSensor, nextSensor);
    fromAbove = (startSpeed > SPEED_LOW) || (startSpeed == SPEED_LOW && startFromAbove);
    stopDistance_mm = getTrainData(train, startSensor, nextSensor, termAdmin)->stopping_distance_mm[SPEED_LOW][fromAbove];
    // term_print(termAdmin, "7: ", 3);
    // term_print_int(termAdmin, (accelDist_micrometers) / 1000 + stopDistance_mm);
    // term_newline(termAdmin);
    if ((accelDist_micrometers) / 1000 + stopDistance_mm < totalDist) {
        *targetSpeed = SPEED_LOW;
        *targetFromAbove = fromAbove;
        return 1;
    }

    *targetSpeed = SPEED_LOW;
    *targetFromAbove = fromAbove;
    return 1;

    // TODO: fail pathfinding here, so we try some other way of getting to target
    // term_print(termAdmin, "No speed good", 13);
    // term_newline(termAdmin);
    //return 0;
}

// recusive function
// recurses when path contains a reverse
// returns speed of train before stopping at end of path
// returns -1 on error
static int planSegment(int pathId, int beginIdx, int beginOffset, int endIdx, int endOffset, int train, int startSpeed, int startFromAbove, int segmentLength, int isAfterReverse, int isBeforeReverse) {
    // determine target speed based on current speed and total distance
    // target speed is the speed we want to travel at to reach the destination
    // currently assume always want to STOP at the destination

    int termAdmin = WhoIs("TermAdmin");
    int trackAdmin = WhoIs("TrackAdmin");

    // term_print(termAdmin, "Plan segment", 12);
    // term_newline(termAdmin);
    // term_print_int(termAdmin, beginIdx);
    // term_print(termAdmin, " to ", 4);
    // term_print_int(termAdmin, endIdx);
    // term_newline(termAdmin);

    enum MessageType type;
    struct path_node* pnode;

    /* DETERMINE AND SET SPEED FOR TRIP */
    int targetSpeed = 0;
    int targetFromAbove = 0;
    int found = determineSpeeds(termAdmin, segmentLength + endOffset - beginOffset, train, startSpeed, startFromAbove, 0, 0, &targetSpeed, &targetFromAbove);
    if (!found)
        return -1;


    // term_print(termAdmin, "Target speed ", 13);
    // term_print_int(termAdmin, targetSpeed);
    // term_newline(termAdmin);

    if (isAfterReverse) {
        getPathNode(pathId, beginIdx)->isAfterReverse = 1;
    }

    // to begin path execution, set correct train speed
    if (targetSpeed != startSpeed) {
        if (!isAfterReverse) {
            setBeginCommand(pathId, TRAIN_COMMAND, train, targetSpeed);
        } else {
            // can set speed while the train is reversing
            struct path_node* node = getPathNode(pathId, beginIdx-1);
            node->commandType = TRAIN_COMMAND;
            node->commandArgs[0] = train;
            node->commandArgs[1] = targetSpeed;
            node->commandDelay = 0;
        }
    }

    /* DETERMINE WHETHER SLOWDOWN IS REQUIRED BEFORE STOPPING */
    int slowdownDistance = 0;
    int slowDownFound = 0;
    int slowDownSpeed, slowDownFromAbove;
    // we want to slow down before stopping
    if (targetSpeed == SPEED_HIGH) {
        const struct train_data* data = getTrainData(train, 0, 0, termAdmin);
        assert(termAdmin, data != 0, "no data for train", 17);
        // TODO: use sensor magic here
        slowdownDistance = distance_micrometers_acceleration(termAdmin, train, SPEED_HIGH, 0, SPEED_LOW, 0, 0) / 1000;
        slowDownSpeed = SPEED_LOW;
        slowDownFromAbove = 1;
    }

    /* SET UP */
    // the next sensor ahead of path[idx] below, used to determine the velocity on that segment
    // stops updating when we find a sensor such that, based on that segment's velocity and stopping time,
    // we can stop exactly on the destination (with a small delay)
    int lastSensor;
    if(getNode(getPathNode(pathId, endIdx)->nodeId)->type == NODE_SENSOR){
        lastSensor = getPathNode(pathId, endIdx)->nodeId;
    }else if (getNode(getPathNode(pathId, endIdx)->nodeId)->type == NODE_EXIT) {
        // we know the previous node is a sensor
        lastSensor = getPathNode(pathId,endIdx-1)->nodeId;
    } else {	
        int repl[6];
        TypedSend(trackAdmin, NEXT_SENSOR, (char*) &(getPathNode(pathId,endIdx)->nodeId), 4, &type, (char*) repl, sizeof(repl));
        assert(termAdmin, type == SUCCESS, "Next sensor request failed", 26);
        lastSensor = repl[0];
    }
    int curSensor = lastSensor;
    // the distance of the sensor from the destination
    int distToDest = endOffset;
    // whether we have already found the stop command sensor
    int foundStop = 0;

    track_node *curNode;

    int trainSpeed = slowDownSpeed; // update to appropriate speed as traversing
    int fromAbove = slowDownFromAbove;
    if (!slowdownDistance) {
        trainSpeed = targetSpeed;
        fromAbove = targetFromAbove;
    }        

    int ret = trainSpeed;

    /* BACKWARDS PASS - DETERMINE WHEN COMMANDS ARE SENT AS DELAYS FROM NODES */
    // start at second-last node
    for (int idx = endIdx - 1; idx >= beginIdx; --idx) {
        // term_print(termAdmin, "Idx ", 4);
        // term_print_int(termAdmin, idx);
        // term_newline(termAdmin);
        pnode = getPathNode(pathId, idx);
        // reset command type
        pnode->commandType = NONE;
        curNode = getNode(getPathNode(pathId, idx)->nodeId);
        if (curNode->type == NODE_BRANCH) {
            // send switch command
            // worst case delay until switch command can be sent is 150 ms (15 ticks)
            // so set up the switch command to be send 20 ticks ahead of expected traversal time
            pnode->commandType = SWITCH_COMMAND;
            // using negative offset allows branch to be first node in the path
            // execute task can figure out when to send using expected arrival time at node
            pnode->commandDelay = SWITCH_DELAY;
            pnode->commandArgs[0] = curNode->num;
            if (curNode->edge[1].dest->id == getPathNode(pathId,idx+1)->nodeId){
                pnode->commandArgs[1] = 'C';
                pnode->switchPos = DIR_CURVED;
                pnode->distToNext = curNode->edge[1].dist;
                distToDest += curNode->edge[1].dist;
            } else {
                pnode->commandArgs[1] = 'S';
                pnode->switchPos = DIR_AHEAD;
                pnode->distToNext = curNode->edge[0].dist;
                distToDest += curNode->edge[0].dist;
            }
        } else {
            // check for reversing
            if ((curNode->type == NODE_MERGE || curNode->type == NODE_EXIT) && curNode->reverse->id == getPathNode(pathId, idx+1)->nodeId) {
                // can stop doing the current calculation. Instead we need two recusive calls to planSegment
                int eOffset = MERGE_OFFSET;
                int bOffset = -MERGE_OFFSET;
                if (curNode->type == NODE_EXIT) {
                    endOffset *= -2;
                    beginOffset *= -2;
                    pnode->distToNext = 0;
                    getPathNode(pathId, idx -1)->distToNext -= 200; // need total distance of 200 less
                } else {
                    pnode->distToNext = 200; // need total distance of 200 more
                }
                
                trainSpeed = planSegment(pathId, beginIdx, beginOffset, idx, eOffset, train, startSpeed, startFromAbove, segmentLength - distToDest, isAfterReverse, 1);
                if (trainSpeed < 0)
                    return -1;
                trainSpeed = planSegment(pathId, idx+1, bOffset, endIdx, endOffset, train, 0, 0, distToDest, 1, isBeforeReverse);
                return trainSpeed;
            }

            // always update distToDest so we don't get confused
            distToDest += curNode->edge[0].dist;
            pnode->distToNext = curNode->edge[0].dist;

            if((foundStop == 0 || (slowdownDistance && slowDownFound == 0)) && curNode->type != NODE_BRANCH){
                // check if we have enough room to stop or slow down
                if (curNode->type == NODE_SENSOR) {
                    lastSensor = curSensor;
                    curSensor = curNode->num;
                }

                const struct train_data* data = getTrainData(train, curSensor, lastSensor, termAdmin);
                assert(termAdmin, data != 0, "no data for train", 17);
                int stoppingDistanceMM = data->stopping_distance_mm[trainSpeed][fromAbove];
                // term_print(termAdmin, "Stopping ", 9);
                // term_print_int(termAdmin, stoppingDistanceMM);
                // term_newline(termAdmin);
                int velocityUM = data->micrometers_per_tick[trainSpeed][fromAbove];
                if (foundStop == 0) {
                    //lastSensor = curSensor;

                    // term_print_int(termAdmin, distToDest);
                    // term_print(termAdmin, " ", 1);
                    // term_print_int(termAdmin, stoppingDistanceMM);
                    // term_newline(termAdmin);

                    if(distToDest >= stoppingDistanceMM){
                        /*
                        * Q: Should we account for the case where we cross multiple sensors while stopping?
                        * A: No, since this code only runs when we need the full distance between the latter 2 sensors to stop
                        * 	-> only need to consider the velocity between the first 2 sensors
                        */
                        int remainingDist = distToDest - stoppingDistanceMM;
                        pnode->commandType = TRAIN_COMMAND;
                        pnode->commandArgs[0] = train;
                        pnode->commandArgs[1] = isBeforeReverse ? 15 : 0;
                        int delay = (remainingDist * 1000) / velocityUM ;
                        pnode->commandDelay = delay; //isBeforeReverse ? delay / 3 : delay;	
                        if (pnode->nodeId == 72 && isBeforeReverse) // Idk always overshoot by a lot on this one
                            pnode->commandDelay = 0;
                        // int id = getNode(endIdx)->id;
                        // if (id == 112 || id == 113 || id == 114 || id == 115) { // idk these always overshoot
                        //     delay = 0;
                        // }

                        // term_print(termAdmin, "Found stopping sensor ", 22);
                        // term_print_int(termAdmin, lastSensor);
                        // term_print(termAdmin, " at dist ", 9);
                        // term_print_int(termAdmin, distToDest);
                        // term_newline(termAdmin);

                        foundStop = 1;
                    }
                } else if (distToDest >= stoppingDistanceMM + slowdownDistance) {
                    // set up slowdown command after triggering this sensor
                    pnode->commandType = TRAIN_COMMAND;
                    pnode->commandArgs[0] = train;
                    pnode->commandArgs[1] = slowDownSpeed;
                    pnode->commandDelay = 0;
                    // for previous nodes, speed is target speed
                    trainSpeed = targetSpeed;
                    fromAbove = targetFromAbove;

                    slowDownFound = 1;
                }
            }
        }
    }

    // return train speed before stopping at end of path
    return ret;
}

static void timePath(int pathId, int train, int beginTime, int srcOffset, int startSpeed, int startFromAbove) {
    int termAdmin = WhoIs("TermAdmin");
    
    int curSpeed = startSpeed;
    int curFromAbove = startFromAbove;
    int prevSpeed = 0;
    int prevFromAbove = 0;
    // assumes only one train command is relevant at a time
    // this will not work for alternating I guess
    // TODO: more general solution in the future
    int lastCmdTime = 0;
    int nextCmdTime = 99999999; // very large number
    int nextSpeed = 0;
    int nextFromAbove = 0;

    // term_print(termAdmin, "Begin time ", 11);
    // term_print_int(termAdmin, beginTime);
    // term_newline(termAdmin);

    setPathBeginTime(pathId, beginTime);

    // begin command
    int args[2];
    enum PathCommandType cmd = getBeginCommand(pathId, &args[0], &args[1]);
    if (cmd == TRAIN_COMMAND) { // currently don't ever have switch commands as begin commands
        lastCmdTime = beginTime;
        prevSpeed = curSpeed;
        prevFromAbove = curFromAbove;
        
        curSpeed = args[1];
        curFromAbove = curSpeed < prevSpeed;
    }

    int lastTime = beginTime + START_DELAY;

    struct path_node *curNode;

    int timeArrive;

    // find begin position
    // don't care about segments before begin position
    int idx = 0;
    int totalDist = 0;
    while(totalDist < srcOffset) {
        curNode = getPathNode(pathId, idx);
        curNode->arrivalTime = -1;
        totalDist += curNode->distToNext; 
        ++idx;
    }

    int distFromPrev = totalDist - srcOffset;

    // time to first node
    for (int i = idx; i < getPathLength(pathId); ++i) {
        curNode = getPathNode(pathId, i);
        // TODO: use sensors here for specific velocity measurements

        // assume next command will not be executed before hit this node
        timeArrive = lastTime + ticks_from_distance_micrometers(termAdmin, train, lastCmdTime, prevSpeed, prevFromAbove, curSpeed, lastTime, distFromPrev * 1000, 0, 0);
        // is assumption correct
        if (nextCmdTime <= timeArrive) {
            lastCmdTime = nextCmdTime;
            nextCmdTime = 99999999; // very large number
            prevSpeed = curSpeed;
            prevFromAbove = curFromAbove;
            curSpeed = nextSpeed;
            curFromAbove = nextFromAbove;

            // redo calculation
            timeArrive = lastTime + ticks_from_distance_micrometers(termAdmin, train, lastCmdTime, prevSpeed, prevFromAbove, curSpeed, lastTime, distFromPrev * 1000, 0, 0);
        }

        if (curNode->isAfterReverse)
            timeArrive += REVERSE_DELAY;

        curNode->arrivalTime = timeArrive;
        lastTime = timeArrive;

        // check if next command
        if (curNode->commandType == TRAIN_COMMAND) { // switch commands don't impact timing
            assert(termAdmin, nextCmdTime == 99999999, "Multiple commands queued before execution", 41);
            nextCmdTime = timeArrive + curNode->commandDelay;
            // term_print(termAdmin, "Arrive ", 7);
            // term_print_int(termAdmin, timeArrive);
            // term_print(termAdmin, " nextCmd ", 9);
            // term_print_int(termAdmin, nextCmdTime);
            // term_newline(termAdmin);
            nextSpeed = curNode->commandArgs[1];
            if (nextSpeed == 15)
                nextSpeed = 0;
            
            nextFromAbove = nextSpeed < curSpeed;
        }

        // retrieve distance to next
        distFromPrev = curNode->distToNext;
    }
}

// reworks previous executepath code
void PathPlanner() {
    int tid = MyParentTid();
    struct PathPlannerRequest request;
    enum MessageType type;
    int trainAdmin = WhoIs("TrainAdmin");

    // await first request
    TypedSend(tid, PLAN_PATH_UNSUCCESSFUL, "", 0, &type, (char*) &request, sizeof(request));

    while(1) {
        int totalDist = getTotalDistance(request.pathId);
        int plen = getPathLength(request.pathId);

        int resp[2];
        TypedSend(trainAdmin, GET_SPEED, (char*) &request.train, 4, &type, (char*) resp, sizeof(resp));
	// for now we assume train is stationary, only need to reverse
	// TODO: do we need to do anything else?
	setReverseAtStart(request.pathId, request.reverseAtStart);

        // TODO: very short distances
            // can stop on non-sensor nodes
            // but then we can get multiple commands for one path node
        int spd = planSegment(request.pathId, 0, request.srcOffset, plen - 1, request.targetOffset, request.train, resp[0], resp[1], totalDist, 0, 0);
        if (spd < 0) {
            TypedSend(tid, PLAN_PATH_UNSUCCESSFUL, (char*) &request.pathId, 4, &type, (char*) &request, sizeof(request));
        } else {
            /* FORWARD PASS - SET ARRIVAL TIMES FOR EACH NODE */
            timePath(request.pathId, request.train, request.beginTime, request.srcOffset, resp[0], resp[1]);

            // next request
            TypedSend(tid, PLAN_PATH, (char*) &request.pathId, 4, &type, (char*) &request, sizeof(request));
        }
        
    }
    
}
