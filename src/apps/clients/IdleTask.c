#include "../syscall.h"
#include "../io_helpers.h"

// keep track of past 3 seconds only so changes in idle time are immediately visible even when running for a long time
#define NUM_PAST_TICKS 300 

// keeps track of which ticks out of the last 3 seconds were seen
// allows easy updating of numTicksSeen
// essentially a ring buffer of the ticks seen in the last 3 seconds
static char wasSeen[NUM_PAST_TICKS] = { 0 };

static int lastTick = 0;
static int numTicksSeen = 0; // out of all ticks in the last 3 seconds
static int lastPrinted = 0;

void IdleTask() {
    // initialize
    int clockServer = WhoIs("ClockServer");
    debug_assert(clockServer >= 0, "WhoIs clock failed\r\n", 20);
    int idleServer = WhoIs("IdleServer");
    int ticks, percent;
    enum MessageType type;
    while(1) {
        ticks = Time(clockServer);
        if (ticks > lastTick) {
            for (int i = lastTick + 1; i < ticks; ++i) {
                if (wasSeen[i % NUM_PAST_TICKS]) {
                    // tick recorded here is no longer in the 3 second period
                    // the corresponding tick in the 3 second period was not observed
                    // need to decrement numTicksSeen
                    wasSeen[i % NUM_PAST_TICKS] = 0;
                    --numTicksSeen;
                }
            }
            if (!wasSeen[ticks % NUM_PAST_TICKS]) {
                // tick recorded here is no longer in the 3 second period
                // the previous tick was not seen, but now we have seen the current tick
                // need to increment numTicksSeen
                wasSeen[ticks % NUM_PAST_TICKS] = 1;
                ++numTicksSeen;
            }
            lastTick = ticks;
            percent = (100 * numTicksSeen);
            if (ticks < NUM_PAST_TICKS) // less than 3 seconds of runtime
                percent /= ticks;
            else // running for 3 seconds or more
                percent /= NUM_PAST_TICKS;
            if (percent != lastPrinted) {
                lastPrinted = percent;
                TypedSend(idleServer, UPDATE_IDLETIME, (char*) &percent, 4, &type, "", 0);
                debug_assert(type == SUCCESS, "Updating idle time failed\r\n", 26);
            }
        }
        asm("wfi"); // go into low power mode
    }
}