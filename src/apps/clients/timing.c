#include "../io_helpers.h"
#include "../syscall.h"

/* NOTE: This task is currently broken. Has not been updated to use new I/O or debug I/O only */

#ifdef TIMING
#include "../../kernel/exception_handlers.h"

// format of each entry is sender priority (receiver priority is always 5), message size
#define NUM_TESTS 6
int tests[NUM_TESTS][2] = {
	{4, 4},
	{4, 64},
	{4, 256},
	{6, 4},
	{6, 64},
	{6, 256}
};

int sender_priority = 0;
int message_size = 0;
void set_params(int j){
	sender_priority = tests[j][0];
	message_size = tests[j][1];
}

char msg[256];
char msg_buf[256];
char rpl[256];
char rpl_buf[256];

int receiver_tid;
void sender(){
	int tid = receiver_tid;
	for(int i = 0; i < NUM_RUNS; ++i){
		Send(tid, msg, message_size, rpl_buf, message_size);
	}
	Exit();
}

void receiver(){
	int tid;
	for(int i = 0; i < NUM_RUNS; ++i){
		Receive(&tid, msg_buf, message_size);
		Reply(tid, rpl, message_size);
	}
	Exit();
}

void test(){
	Create(sender_priority, sender);
	receiver_tid = Create(5, receiver);
	Exit();
}

void print_test(uint64_t test_time){
	#ifdef NOOPT
	puts("noopt ", 6);
	#else
	puts("opt ", 4);
	#endif


	int dcache = 1;
	int icache = 1;
	#ifdef NODCACHE
	dcache = 0;
	#endif
	#ifdef NOICACHE
	icache = 0;
	#endif

	if(icache && dcache){
		puts("bcache ", 7);	
	}else if(icache){
		puts("icache ", 7);		
	}else if(dcache){
		puts("dcache ", 7);	
	}else{
		puts("nocache ", 8);
	}


	if(sender_priority >= 5){
		puts("S ", 2);
	}else{
		puts("R ", 2);
	}
	print_int(message_size);
	putc(' ');
	print_uint64(test_time);
	puts("\r\n", 2);
}

void run_performance_tests() {
	for(int j = 0; j < NUM_TESTS; ++j){
		set_params(j);
		Create(9, test);
		uint64_t total_test_time = get_total_test_time();
		uint64_t avg_test_time = total_test_time / NUM_RUNS;
		print_test(avg_test_time);
		reset_test();
	}
}
#endif
