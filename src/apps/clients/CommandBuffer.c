#include "../syscall.h"
#include "../io_helpers.h"
#include <ctype.h>
#include "../track.h"

#define MAX_CMD_LEN 20
#define NUM_COMMANDS 11
#define MAX_ARGS_NUM 3

enum ArgType {
    TRAIN_NUMBER,
    SWITCH_NUMBER,
    SPEED,
    SWITCH_POSITION,
    NODE_NUMBER,
    OFFSET,
    SENSOR_NUM
};

struct Command {
    char name[2];
    int length;
    int admin;
    int numArgs;
    enum ArgType argTypes[MAX_ARGS_NUM];
    enum MessageType type;
};

static struct Command commands[NUM_COMMANDS];

static int trainAdmin;
static int trackAdmin;
static int pathManager;

static void setupCommands() {
    trainAdmin = WhoIs("TrainAdmin");
    trackAdmin = WhoIs("TrackAdmin");
    int termAdmin = WhoIs("TermAdmin");
    pathManager = WhoIs("PathManager");

    // train command
    struct Command trainCommand;
    trainCommand.name[0] = 't';
    trainCommand.name[1] = 'r';
    trainCommand.length = 2;

    trainCommand.numArgs = 2;
    trainCommand.argTypes[0] = TRAIN_NUMBER;
    trainCommand.argTypes[1] = SPEED;

    trainCommand.admin = trainAdmin;
    trainCommand.type = SET_SPEED;
    commands[0] = trainCommand;

    // switch command
    struct Command switchCommand;
    switchCommand.name[0] = 's';
    switchCommand.name[1] = 'w';
    switchCommand.length = 2;

    switchCommand.numArgs = 2;
    switchCommand.argTypes[0] = SWITCH_NUMBER;
    switchCommand.argTypes[1] = SWITCH_POSITION;

    switchCommand.admin = trackAdmin;
    switchCommand.type = SET_SWITCH;
    commands[1] = switchCommand;

    // reverse command
    struct Command reverseCommand;
    reverseCommand.name[0] = 'r';
    reverseCommand.name[1] = 'v';
    reverseCommand.length = 2;

    reverseCommand.numArgs = 1;
    reverseCommand.argTypes[0] = TRAIN_NUMBER;

    reverseCommand.admin = trainAdmin;
    reverseCommand.type = REVERSE_TRAIN;
    commands[2] = reverseCommand;

    // pathfinding command
    struct Command pathCommand;
    pathCommand.name[0] = 'p';
    pathCommand.name[1] = 'a';
    pathCommand.length = 2;

    pathCommand.numArgs = 3;
    pathCommand.argTypes[0] = TRAIN_NUMBER;
    // Q: What exactly is a "node number"? I just used ID in the track node array, but that is a bit unintuitive... Discuss
    pathCommand.argTypes[1] = NODE_NUMBER; 
    pathCommand.argTypes[2] = OFFSET;

    pathCommand.admin = pathManager;
    pathCommand.type = GOTO_NODE;
    commands[3] = pathCommand;

    // stop on sensor command
    struct Command stopCommand;
    stopCommand.name[0] = 's';
    stopCommand.name[1] = 't';
    stopCommand.length = 2;

    stopCommand.numArgs = 2;
    stopCommand.argTypes[0] = TRAIN_NUMBER;
    stopCommand.argTypes[1] = SENSOR_NUM;

    stopCommand.admin = trainAdmin;
    stopCommand.type = STOP_ON_SENSOR;
    commands[4] = stopCommand;

    // reset track to initial state command
    struct Command resetCommand;
    resetCommand.name[0] = 'r';
    resetCommand.name[1] = 's';
    resetCommand.length = 2;
    
    resetCommand.numArgs = 0;

    resetCommand.admin = trackAdmin;
    resetCommand.type = RESET_TRACK;
    commands[5] = resetCommand;

    // basic command for testing
    struct Command reserveCommand;
    reserveCommand.name[0] = 'r';
    reserveCommand.length = 1;

    reserveCommand.numArgs = 1;

    reserveCommand.argTypes[0] = NODE_NUMBER;

    reserveCommand.admin = termAdmin;
    reserveCommand.type = RESERVE_TEST;
    commands[6] = reserveCommand;

    // register trains comand
    struct Command regCommand;
    regCommand.name[0] = 'r';
    regCommand.name[1] = 'g';
    regCommand.length = 2;

    regCommand.numArgs = 1;
    regCommand.argTypes[0] = TRAIN_NUMBER;

    regCommand.admin = pathManager;
    regCommand.type = REGISTER_TRAIN;
    commands[7] = regCommand;
    
    // accel on sensor command
    struct Command accelCommand;
    accelCommand.name[0] = 'a';
    accelCommand.name[1] = 'c';
    accelCommand.length = 2;

    accelCommand.numArgs = 3;
    accelCommand.argTypes[0] = TRAIN_NUMBER;
    accelCommand.argTypes[1] = SENSOR_NUM;
    accelCommand.argTypes[2] = SPEED;

    accelCommand.admin = trainAdmin;
    accelCommand.type = ACCEL_ON_SENSOR;
    commands[8] = accelCommand;

    struct Command clearCommand;
    clearCommand.name[0] = 'c';
    clearCommand.name[1] = 'l';
    clearCommand.length = 2;

    clearCommand.numArgs = 0;

    clearCommand.admin = termAdmin;
    clearCommand.type = DEBUG_CLEAR;
    commands[9] = clearCommand;

    struct Command stopPathing;
    stopPathing.name[0] = 's';
    stopPathing.name[1] = 'p';
    stopPathing.length = 2;

    stopPathing.numArgs = 1;
    stopPathing.argTypes[0] = TRAIN_NUMBER;

    stopPathing.admin = pathManager;
    stopPathing.type = STOP_PATHING;
    commands[10] = stopPathing;
}    

// returns position of first argument, or -1 if unsuccessful
static int tryParseCommandName(struct Command type, char *command, int len) {
    if (len >= type.length) {
        for (int i = 0; i < type.length; ++i)
            if (command[i] != type.name[i])
                return -1;
        if (command[type.length] == ' ' || type.length == len)
            return type.length; // re-check space with first argument check, if applicable
    }
    return -1;
}

static void addNextDigit(char *command, int len, int *argPos, int *num) {
    if (*argPos < len && isdigit((int)command[*argPos])) {
        *num *= 10;
        *num += (int) command[*argPos] - '0';
        ++(*argPos);
    }
}

static int tryParseArg(enum ArgType type, char *command, int *arg, int argPos, int len) {
    // check for space before argument
    if (argPos >= len || command[argPos] != ' ')
        return -1;
    ++argPos;
    int num;
    switch(type) {
        case TRAIN_NUMBER:
            if (argPos >= len || !isdigit((int)command[argPos]))
                return -1;
            num = (int) command[argPos] - '0';
            ++argPos;
            addNextDigit(command, len, &argPos, &num);
            *arg = num;
            return argPos;
        case SWITCH_NUMBER:
            if (argPos >= len || !isdigit((int)command[argPos]))
                return -1;
            num = (int) command[argPos] - '0';
            ++argPos;
            addNextDigit(command, len, &argPos, &num);
            addNextDigit(command, len, &argPos, &num);
            if (num < 1 || (num > 18 && num < 153) || num > 156)
                return -1;
            *arg = num;
            return argPos;
        case SPEED:
            if (argPos >= len || !isdigit((int)command[argPos]))
                return -1;
            num = (int) command[argPos] - '0';
            ++argPos;
            addNextDigit(command, len, &argPos, &num);
            if (num > 14)
                return -1;
            *arg = num;
            return argPos;
        case SWITCH_POSITION:
            if (argPos < len) {
                if (command[argPos] == 'C' || command[argPos] == 'c') {
                    *arg = 'C';
                    return argPos + 1;
                } else if (command[argPos] == 'S' || command[argPos] == 's') {
                    *arg = 'S';
                    return argPos + 1;
                }
            }
            return -1;
        case SENSOR_NUM:
            if (argPos >= len || !isdigit((int)command[argPos]))
                return -1;
            num = (int) command[argPos] - '0';
            ++argPos;
            addNextDigit(command, len, &argPos, &num);
            *arg = num;
            return argPos;
        case NODE_NUMBER:
            if (argPos >= len)
                return -1;
	    int sensor_letter = -1;
	    if(command[argPos] >= 'A' && command[argPos] <= 'E'){
	    	num = 0;
		sensor_letter = command[argPos] - 'A';
	    }else{
		if(!isdigit((int)command[argPos])) return -1;
            	num = (int) command[argPos] - '0';
	    }
            ++argPos;
            addNextDigit(command, len, &argPos, &num);
            addNextDigit(command, len, &argPos, &num);
            if (num > 143) // invalid node number
                return -1;

	    if(sensor_letter >= 0){
	    	num = sensor_letter * 16 + (num - 1);
	    }

            *arg = num;
            return argPos;
        case OFFSET: // distances between two nodes are at most three-digit numbers. can be negative
            ;
	    int isNegative = 0;
            if (argPos < len && command[argPos] == '-') {
                isNegative = 1;
                ++argPos;
            }
            if (argPos >= len || !isdigit((int)command[argPos]))
                return -1;
            num = (int) command[argPos] - '0';
            ++argPos;
            addNextDigit(command, len, &argPos, &num);
            addNextDigit(command, len, &argPos, &num);
            if (isNegative)
                num = -num;
            *arg = num;
            return argPos;
    }
    return -1;
}

static void parseAndExecute(int termAdmin, int readServer, char *command, int len) {
    enum MessageType type;
    
    if(command[0] == 'q' && len == 1){
        TypedSend(trainAdmin, STOP_ALL_TRAINS, "", 0, &type, "", 0);
        assert(termAdmin, type==SUCCESS, "Failed to send command", 22);	
        
        int clockServer = WhoIs("ClockServer");
        Delay(clockServer, 100);
        
        Halt();	    	
    }
    for (int i = 0; i < NUM_COMMANDS; ++i) {
        int argPos = tryParseCommandName(commands[i], command, len);
        if (argPos < 0)
            continue;

        int args[commands[i].numArgs]; // assume all commands are integers
        for (int j = 0; j < commands[i].numArgs; ++j) {
            argPos = tryParseArg(commands[i].argTypes[j], command, &args[j], argPos, len);
            if (argPos < 0)
                break;

        }
        if (argPos < 0)
            continue;

        // managed to parse full command!

        // register command, need to do some extra fancy UI stuff
        if (command[0] == 'r' && command[1] == 'g') {
            int trCmd[2];
            trCmd[0] = args[0];
            trCmd[1] = 16; 
            // turn on lights
            TypedSend(trainAdmin, SET_SPEED, (char*) trCmd, sizeof(trCmd), &type, "", 0);
            assert(termAdmin, type==SUCCESS, "Failed to send command", 22);

            // confirm train position manually
            TypedSend(termAdmin, CONFIRM_POSITION, (char*) &args[0], sizeof(args[0]), &type, "", 0);
            if (type == SUCCESS) { // fails if train is already registered
                Getc(readServer);

                TypedSend(termAdmin, POSITION_CONFIRMED, "", 0, &type, "", 0);
                assert(termAdmin, type==SUCCESS, "Failed to send command", 22);

                trCmd[1] = 10;

                // start train
                TypedSend(trainAdmin, SET_SPEED, (char*) trCmd, sizeof(trCmd), &type, "", 0);
                assert(termAdmin, type==SUCCESS, "Failed to send command", 22);

                // determine train position
                int id;

                TypedSend(commands[i].admin, commands[i].type, (char*) args, sizeof(args), &type, (char*) &id, 4);
                assert(termAdmin, type==SUCCESS, "Failed to send command", 22);

                trCmd[1] = id;

                // tell TermAdmin about completed registration
                TypedSend(termAdmin, REGISTRATION, (char*) &trCmd, sizeof(trCmd), &type, "", 0);
                assert(termAdmin, type==SUCCESS, "Failed to send command", 22);
            }

            
        } else {
            TypedSend(commands[i].admin, commands[i].type, (char*) args, sizeof(args), &type, "", 0);
            assert(termAdmin, type==SUCCESS, "Failed to send command", 22);
        }
        
        return;
    }
}

void CommandBuffer() {
    // init
    int readServer = WhoIs("TermReadServer");
    int termAdmin = WhoIs("TermAdmin");

    setupCommands();

    char command[20];
    int idx = 0;
    enum MessageType type;

    // initialize track
    int isInit = 0;
    char c;
    while (!isInit) {
        c = Getc(readServer);
        if (c == 'A' || c == 'a') {
            setupTrack(TRACK_A, trackAdmin);
            isInit = 1;
        } else if (c == 'B' || c == 'b') {
            setupTrack(TRACK_B, trackAdmin);
            isInit = 1;
        }
    }

    TypedSend(termAdmin, TRACK_INITIALIZED, &c, 1, &type, "", 0);
    assert(termAdmin, type == SUCCESS, "Train init confirm failed", 25);

    TypedSend(trackAdmin, TRACK_INITIALIZED, "", 0, &type, "", 0);
    assert(termAdmin, type == SUCCESS, "Train init confirm failed", 25);

    // start parsing commands
    while (1) {
        command[idx] = Getc(readServer);
        if (command[idx] == '\r') {
            TypedSend(termAdmin, COMMAND_CLEAR, "", 0, &type, "", 0);
            assert(termAdmin, type == SUCCESS, "Clearing cmd failed", 19);
            parseAndExecute(termAdmin, readServer, command, idx);
            idx = 0;
        } else if (command[idx] == '\b') {
            if (idx > 0) {
                TypedSend(termAdmin, COMMAND_BACKSPACE, "", 0, &type, "", 0);
                assert(termAdmin, type == SUCCESS, "Backspace failed", 16);
                --idx;
            }
        }else {
            if (idx < MAX_CMD_LEN) {
                TypedSend(termAdmin, COMMAND_CHARACTER, &command[idx], 1, &type, "", 0);
                assert(termAdmin, type == SUCCESS, "Printing char failed", 20);
                ++idx;
            }
        }
    }
}
