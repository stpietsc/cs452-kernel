#include "../syscall.h"
#include "../io_helpers.h"
#include "../train_data.h"

static int print_accel_time(int v1, int v2, int ticks, int dist, int termAdmin){
	int expected_avg_v = (v1 + v2) / 2;
	int actual_avg_v = dist / ticks;
	
	term_print_int(termAdmin, v1);
	term_newline(termAdmin);
	term_print_int(termAdmin, v2);
	term_newline(termAdmin);
	term_print_int(termAdmin, ticks);
	term_newline(termAdmin);
	term_print_int(termAdmin, dist);
	term_newline(termAdmin);

	int accel, ticks_to_accel;
	if((actual_avg_v <= expected_avg_v) == (v1 < v2)){
		term_print(termAdmin, "not done accel", 14);
		term_newline(termAdmin);
		// train has not finished accelerating
		int v_at_sensor = 2 * actual_avg_v - v1; // rearrange average velocity formula
		accel = (v_at_sensor - v1) / ticks;
		ticks_to_accel = (v2 - v1) / accel;
	} else {
		// train has finished accelerating
		term_print(termAdmin, "done accel", 10);
		term_newline(termAdmin);
		/*
		 * Split into accelerating and non-accelerating parts, form system of equations:
		 * ticks = t1 + t2
		 * dist = expected_avg_v * t1 + v2 * t2
		 * 	= expected_avg_v * t1 + v2 * (ticks - t1)
		 * 	= (expected_avg_v - v2) * t1 + v2 * ticks
		 * t1 = (dist - v2 * ticks) / (expected_avg_v - v2) = ticks to accel
		 */
		ticks_to_accel = (dist - v2 * ticks) / (expected_avg_v - v2);
		accel = (v2 - v1) / ticks_to_accel;
	}
	term_print(termAdmin, "accel: ", 7);
	term_print_int(termAdmin, accel);
	term_newline(termAdmin);
	term_print(termAdmin, "ticks: ", 7);
	term_print_int(termAdmin, ticks_to_accel);
	term_newline(termAdmin);
	term_newline(termAdmin);

	return accel;
}

void accelOnSensor() {
	int trainAdmin = WhoIs("TrainAdmin");
	int sensorAdmin = WhoIs("SensorAdmin");
	int termAdmin = WhoIs("TermAdmin");
	int trackAdmin = WhoIs("TrackAdmin");

	int args[3];
	enum MessageType type;
	TypedSend(trainAdmin, ACCEL_ON_SENSOR_ARGS, "", 0, &type, (char *) args, sizeof(args));

	int repl[2];
	
	int train = args[0];
	int sensor1 = args[1];
	
	// get the next sensors and dists
	TypedSend(trackAdmin, NEXT_SENSOR, (char*) &sensor1, sizeof(sensor1), &type, (char*) &repl, sizeof(repl));
	int sensor2 = repl[0];
	int dist = repl[1] * 1000;
	TypedSend(trackAdmin, NEXT_SENSOR, (char*) &sensor2, sizeof(sensor2), &type, (char*) &repl, sizeof(repl));
	int sensor3 = repl[0];
	dist += repl[1] * 1000;
	TypedSend(trackAdmin, NEXT_SENSOR, (char*) &sensor3, sizeof(sensor3), &type, (char*) &repl, sizeof(repl));
	int sensor4 = repl[0];
	dist += repl[1] * 1000;

	// get the current and target train speed
	TypedSend(trainAdmin, GET_SPEED, (char*) &train, sizeof(train), &type, (char*) &repl, sizeof(repl));
	int speed1 = repl[0];
	int fromAbove1 = repl[1];
	int speed2 = args[2];
	int fromAbove2 = (speed1 > speed2);

	// get the current and target train velocity
	const struct train_data* data1 = getTrainData(train, sensor1, sensor2, termAdmin);
	int v1 = data1->micrometers_per_tick[speed1][fromAbove1];
	const struct train_data* data2 = getTrainData(train, sensor3, sensor4, termAdmin);
	int v2 = data2->micrometers_per_tick[speed2][fromAbove2];

	// print details about the current test
	term_print(termAdmin, "testing speed ", 14);
	term_print_int(termAdmin, speed1);
	term_print(termAdmin, " (", 2);
	term_print(termAdmin, fromAbove1 ? "above" : "below", 5);
	term_print(termAdmin, ") to speed ", 11);
	term_print_int(termAdmin, speed2);
	term_newline(termAdmin);

	// wait until we hit the first sensor
	TypedSend(sensorAdmin, AWAIT_SENSOR, (char*) &sensor1, sizeof(sensor1), &type, "", 0);
	
	// set the new speed
	int train_args[2];
	train_args[0] = train;
	train_args[1] = speed2;
	TypedSend(trainAdmin, SET_SPEED, (char*) train_args, sizeof(train_args), &type, "", 0);

	// wait until we hit the fourth sensor
	int resp[2];
	TypedSend(sensorAdmin, AWAIT_SENSOR, (char*) &sensor4, sizeof(sensor4), &type, (char*) resp, sizeof(resp));
	int timeDiff = resp[1];

	// compute and print the accel/time to accel	
	int accel = print_accel_time(v1, v2, timeDiff, dist, termAdmin);

	updateAcceleration(train, sensor1, sensor2, speed1, speed2, fromAbove1, accel, termAdmin);
	updateAcceleration(train, sensor2, sensor3, speed1, speed2, fromAbove1, accel, termAdmin);
	updateAcceleration(train, sensor3, sensor4, speed1, speed2, fromAbove1, accel, termAdmin);
}
