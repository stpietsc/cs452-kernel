#include "../io_helpers.h"
#include "../syscall.h"
// call to kernel is temporary: only until clock server is implemented in K3
#include "../../kernel/rpi.h"

static char get_random_play() {
	int random = get_ticks() % 3; // change to use ticks here instead
	if (random == 0)
		return 'R';
	if (random == 1)
		return 'P';
	return 'S';
}

void RPSClient_Standard() {
	int tid = WhoIs("RPSServer");
	int writeServer = WhoIs("TermSendServer");

	char reply[1];
	enum MessageType type;
	TypedSend(tid, SIGNUP_RPS, "", 0, &type, reply, 1);
	assert(writeServer, type == SUCCESS, "Didn't get signed up\r\n", 22);

	for (int i = 0; i < 4; ++i) {
		char msg[1];
		msg[0] = get_random_play();
		TypedSend(tid, PLAY_RPS, msg, 1, &type, reply, 1);
		if (type == QUIT_RPS) { // if other task quit, re-signup
			TypedSend(tid, SIGNUP_RPS, "", 0, &type, reply, 1);
			assert(writeServer, type == SUCCESS, "Didn't get signed up\r\n", 22);
		}
		assert(writeServer, type != ERROR, "Couldn't play\r\n", 15);
	}

	TypedSend(tid, QUIT_RPS, "", 0, &type, reply, 1);
	assert(writeServer, type == SUCCESS, "Couldn't quit\r\n", 15);
}

void RPSClient_MultipleSignups() {
	int tid = WhoIs("RPSServer");
	int writeServer = WhoIs("TermSendServer");
	// try to sign up multiple times in a row
	char reply[1];
	enum MessageType type;
	TypedSend(tid, SIGNUP_RPS, "", 0, &type, reply, 1);
	assert(writeServer, type == SUCCESS, "Didn't get signed up\r\n", 22);

	TypedSend(tid, SIGNUP_RPS, "", 0, &type, reply, 1);
	assert(writeServer, type == ERROR, "Allowed multiple signups\r\n", 26);

	// check that can still play
	char msg[1];
	msg[0] = get_random_play();
	TypedSend(tid, PLAY_RPS, msg, 1, &type, reply, 1);
	assert(writeServer, type != ERROR, "Couldn't play\r\n", 15);

	// quit the game
	if (type != QUIT_RPS) {
		TypedSend(tid, QUIT_RPS, "", 0, &type, reply, 1);
		assert(writeServer, type == SUCCESS, "Couldn't quit\r\n", 15);
	}
}

void RPSClient_PlayWithoutSignup() {
	int tid = WhoIs("RPSServer");
	int writeServer = WhoIs("TermSendServer");
	char reply[1];
	enum MessageType type;

	// try to play without signing up
	char msg[1];
	msg[0] = get_random_play();
	TypedSend(tid, PLAY_RPS, msg, 1, &type, reply, 1);
	assert(writeServer, type == ERROR, "Play without signup\r\n", 21);
}

void RPSClient_InvalidPlayRequests() {
	int tid = WhoIs("RPSServer");
	int writeServer = WhoIs("TermSendServer");
	// sign up
	char reply[1];
	enum MessageType type;
	TypedSend(tid, SIGNUP_RPS, "", 0, &type, reply, 1);
	assert(writeServer, type == SUCCESS, "Didn't get signed up\r\n", 22);

	// invalid play requests
	char msg[1];
	msg[0] = 'L';
	TypedSend(tid, PLAY_RPS, "", 0, &type, reply, 1);
	assert(writeServer, type == ERROR, "Empty play\r\n", 12);
	TypedSend(tid, PLAY_RPS, msg, 1, &type, reply, 1);
	assert(writeServer, type == ERROR, "Incorrect play\r\n", 16);

	// should always be able to quit - no valid play yet
	TypedSend(tid, QUIT_RPS, "", 0, &type, reply, 1);
	assert(writeServer, type == SUCCESS, "Couldn't quit\r\n", 15);
}