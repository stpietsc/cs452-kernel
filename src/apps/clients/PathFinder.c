#include "../path.h"
#include "../track.h"
#include "../syscall.h"
#include "../io_helpers.h"

#define QUEUE_SIZE 30

static const int NUM_NEIGHBOURS[] = {
    0, // NODE_NONE
    1, // NODE_SENSOR
    2, // NODE_BRANCH
    1, // NODE_MERGE
    1, // NODE_ENTER
    0 // NODE_EXIT
};

// IMPORTANT: should only have ONE instance of this
// since uses intrusive data inside the track graph

#define REVERSE_PENALTY 3500
#define REVERSE_DISTANCE_FROM_START 700

static int queue[QUEUE_SIZE];

static int nextNode(int termAdmin) {
    int next = -1;
    int minDist = -1;
    int dist;
    for (int i = 0; i < QUEUE_SIZE; ++i) {
        if (queue[i] >= 0) {
            dist = getDist(queue[i]);
            if (dist >= 0 && (dist < minDist || minDist == -1)) {
                minDist = dist;
                next = i;
            }
        }
    }
    // TODO: remove this before submitting
    //assert(termAdmin, next != -1, "Pathfinding queue empty", 23);
    if (next == -1)
        return -1;
    int ret = queue[next];
    queue[next] = -1; // remove this from the queue
    return ret;
}

// only adds if not already in queue
static void addQueue(int termAdmin, int node) {
    int freeIdx = -1;
    for (int i = 0; i < QUEUE_SIZE; ++i) {
        if (queue[i] == node) {
            //Puts(sendServer, "In queue!\r\n", 11);
            return; // already in the queue
        }
            
        if (freeIdx < 0 && queue[i] == -1)
            freeIdx = i;
    }
    assert(termAdmin, freeIdx != -1, "Pathfinding queue full", 22);
    queue[freeIdx] = node;
}


// will store path in path static global
// also determines commands necessary along path
static void populatePath(int termAdmin, int last, int pathId) {
    track_node *cur = getNode(last);
    assert(termAdmin, cur->pathlen + 1 < MAX_PATH_LEN, "Path too long", 13);

    setTotalDistance(pathId, cur->distance);
    // term_print(termAdmin, "Plen: ", 6);
    // term_print_int(termAdmin, cur->pathlen);
    // term_newline(termAdmin);
    setPathLength(pathId, cur->pathlen + 1);
    // populate node IDs
    int idx = cur->pathlen + 1;
    getPathNode(pathId, idx)->nodeId = -1;
    --idx;
    while (idx >= 0) {
        getPathNode(pathId, idx)->nodeId = cur->id;
        // term_print_int(termAdmin, cur->id);
        // term_newline(termAdmin);
        cur = cur->prev;
        --idx;
    }
}

// this includes reversed nodes
static int excluded_nodes[2 * MAX_EXCLUDED];
static int num_excluded;
static void exclude_nodes(struct PathFinderRequest* req){
	for(int i = 0; i < req->num_excluded; ++i){
		int cur = req->excluded_nodes[i];
		excluded_nodes[2 * i] = cur;
		excluded_nodes[2 * i + 1] = getNode(cur)->reverse->id;
	}
    // excluded_nodes[2 * req->num_excluded + 1] = 104;
    // excluded_nodes[2 * req->num_excluded] = 105; // currently don't use BR 13 or MR 13 on track A
	num_excluded = 2 * req->num_excluded; //+ 2;
}

static int node_excluded(int node){
	if(node == 104 || node == 105) return 1; // broken on track A
	for(int i = 0; i < num_excluded; ++i){
		if(excluded_nodes[i] == node) return 1;
	}
	return 0;
}
// uses Dijkstras algorithm using arrays and internal graph data
// returns 0 if unsuccessful, length of path if successful
static int findPath(int termAdmin, int src, int target) {
    for (int i = 0; i < TRACK_MAX; ++i)
        setDist(i, -1);

    // init queue for Dijkstra's
    for (int i = 0; i < QUEUE_SIZE; ++i)
        queue[i] = -1;

    queue[0] = src;
    setDist(queue[0], 0);

    int cur = nextNode(termAdmin);
    track_node *curNode = getNode(cur);
    assert(termAdmin, curNode->id == cur, "Wrong ID", 8);
    curNode->pathlen = 0;
    int dist;
    while (target != cur && cur >= 0) {
        // only allow reversing once a minimum distance has been covered, to allow gaining speed first
        // also disallow reversing that will go onto an excluded node
        if (curNode->type == NODE_MERGE && curNode->distance > REVERSE_DISTANCE_FROM_START && !node_excluded(curNode->edge[0].dest->id)) {
            // allow reversing of trains after merges
            dist = curNode->distance + REVERSE_DISTANCE_MERGE + REVERSE_PENALTY;
            if (dist < curNode->reverse->distance || curNode->reverse->distance == -1) {
                curNode->reverse->distance = dist;
                curNode->reverse->prev = curNode;
                curNode->reverse->pathlen = curNode->pathlen + 1;
                addQueue(termAdmin, curNode->reverse->id);
            }
        }
        if (curNode->type == NODE_EXIT) {
            // allow reversing of trains before exits
            dist = curNode->distance + REVERSE_DISTANCE_EXIT + REVERSE_PENALTY;
            if (dist < curNode->reverse->distance || curNode->reverse->distance == -1) {
                curNode->reverse->distance = dist;
                curNode->reverse->prev = curNode;
                curNode->reverse->pathlen = curNode->pathlen + 1;
                addQueue(termAdmin, curNode->reverse->id);
            }
        }
        for (int i = 0; i < NUM_NEIGHBOURS[curNode->type]; ++i) {
            // don't use edges that are inaccessible due to broken switches
            if (curNode->brokenPosn == 'C' && i != 1)
                continue;
            if (curNode->brokenPosn == 'S' && i != 0)
                continue;
            track_edge e = curNode->edge[i];
	    if(node_excluded(e.dest->id)) continue;

            assert(termAdmin, e.src->id == cur, "Wrong edge", 10);
            dist = curNode->distance + e.dist;
            if (dist < e.dest->distance || e.dest->distance == -1) {
                e.dest->distance = dist;
                e.dest->prev = curNode;
                e.dest->pathlen = curNode->pathlen + 1;
                addQueue(termAdmin, e.dest->id);
            }
        }
        cur = nextNode(termAdmin);
        curNode = getNode(cur);
    }
    return cur;
}

void PathFinder() {
    // init
    int termAdmin = WhoIs("TermAdmin");
    struct PathFinderRequest request;
    int tid = MyParentTid();
    enum MessageType type;

    TypedSend(tid, FIND_PATH_INIT, "", 0, &type, (char*) &request, sizeof(request)); // wait for first request

    while (1) {
	exclude_nodes(&request);
        // Dijkstras
        int targetId = findPath(termAdmin, request.srcNode, request.targetNode);
        if(targetId < 0) {
            	// pathfinding error
            	TypedSend(tid, FIND_PATH_UNSUCCESSFUL, (char*) &request.pathId, 4, &type, (char*) &request, sizeof(request));   
        } else {
        	populatePath(termAdmin, targetId, request.pathId);
        	TypedSend(tid, FOUND_PATH, (char*) &request.pathId, 4, &type, (char*) &request, sizeof(request));
	}
    }
}
