#include "../path.h"
#include "../syscall.h"
#include "../track.h"
#include "../train_data.h"
#include "../io_helpers.h"
#include "../sensors.h"
#include "../userTasks.h"
#include "../priorities.h"

#define MAX_DELAY 30
#define RESERVE_BUFFER 400

static int getSensorIdx(int pathId, int idx, int plen, int sensor) {
    if (sensor == -1)
        return -1;
    while (idx < plen) {
        if (getPathNode(pathId, idx)->nodeId == sensor)
            return idx;
        ++idx;
    }
    return -1;
}

static void sendTrainCmd(int trainAdmin, int pathId, int idx, int *curSpeed, int *curFromAbove, int termAdmin) {
    enum MessageType type;
    struct path_node *curNode = getPathNode(pathId, idx);
    if (curNode->commandArgs[1] == 15) { // reverse
        // term_print(termAdmin, "REVERSE", 7);
        // term_newline(termAdmin);
        *curSpeed = 0;
        *curFromAbove = 1;
        TypedSend(trainAdmin, REVERSE_TRAIN, (char*) curNode->commandArgs, sizeof(curNode->commandArgs), &type, "", 0);
        assert(termAdmin, type == SUCCESS, "Set speed failed", 16);
    } else {
        *curFromAbove = curNode->commandArgs[1] < *curSpeed;
        *curSpeed = curNode->commandArgs[1];
        TypedSend(trainAdmin, SET_SPEED, (char*) curNode->commandArgs, sizeof(curNode->commandArgs), &type, "", 0);
        assert(termAdmin, type == SUCCESS, "Set speed failed", 16);
    }
}

static void sendSwitchCmd(int trackAdmin, int pathId, int idx, int termAdmin) {
    enum MessageType type;
    struct path_node *curNode = getPathNode(pathId, idx);

    TypedSend(trackAdmin, SET_SWITCH, (char*) curNode->commandArgs, sizeof(curNode->commandArgs), &type, "", 0);
    assert(termAdmin, type == SUCCESS, "Set speed failed", 16);
}

static void sendCmd(int trackAdmin, int trainAdmin, int pathId, int idx, int *curSpeed, int *curFromAbove, int termAdmin) {
    struct path_node *curNode = getPathNode(pathId, idx);
    if (curNode->wasExecuted)
        return;
    if (curNode->commandType == TRAIN_COMMAND)
        sendTrainCmd(trainAdmin, pathId, idx, curSpeed, curFromAbove, termAdmin);
    if (curNode->commandType == SWITCH_COMMAND)
        sendSwitchCmd(trackAdmin, pathId, idx, termAdmin);
    curNode->wasExecuted = 1;
}

// returns node id if unsuccessful
static int reserveAlongPath(
    int train, int pathId, // general info
    int *reservIdx, int *haveReserved, int reservDist, // reservation info
    int reservManager // tasks
) {
    // initialize reservations based on current speed
    int termAdmin = WhoIs("TermAdmin");

    int reservation[3];
    reservation[2] = train;
    struct path_node *curNode;
    enum MessageType type;
    // don't reserve past end of the path
    while (*haveReserved < reservDist && *reservIdx < getPathLength(pathId)) {
        // term_print_int(termAdmin, *reservIdx);
        // term_print(termAdmin, " ", 1);
        // term_print_int(termAdmin, *haveReserved);
        // term_print(termAdmin, " ", 1);
        curNode = getPathNode(pathId, *reservIdx);
        reservation[0] = curNode->nodeId;
        reservation[1] = curNode->switchPos;

        // term_print(termAdmin, "R ", 2);
        // term_print_int(termAdmin, curNode->nodeId);
        // term_print(termAdmin, " ", 1);
        // term_print_int(termAdmin, curNode->switchPos);
        // term_newline(termAdmin);

        // ensure current segment is reserved
        TypedSend(reservManager, RESERVE_SEGMENT, (char*) reservation, sizeof(reservation), &type, "", 0);
        if (type != SUCCESS) {
            term_print(termAdmin, "res error", 9);
            term_newline(termAdmin);
            return curNode->nodeId;
        }
        // term_print(termAdmin, " r ", 3);

        *haveReserved += curNode->distToNext;
        ++(*reservIdx);
    }
    // term_print(termAdmin, "res done", 8);
    return -1;
}

static void releaseAlongPath(
    int train, int pathId, // general info
    int *firstReservIdx, int lastIdx, int reservIdx, int *haveReserved, // reservation info 
    int reservManager // tasks
) {
    int reservation[3];
    reservation[2] = train;
    struct path_node *curNode;
    enum MessageType type;
    while (*firstReservIdx <= lastIdx && *firstReservIdx < reservIdx - 1) {
        curNode = getPathNode(pathId, *firstReservIdx);
        reservation[0] = curNode->nodeId;
        reservation[1] = curNode->switchPos;

        // term_print(termAdmin, "L ", 2);
        // term_print_int(termAdmin, curNode->nodeId);
        // term_newline(termAdmin);

        // ensure current segment is reserved
        TypedSend(reservManager, RELEASE_SEGMENT, (char*) reservation, sizeof(reservation), &type, "", 0);

        *haveReserved -= curNode->distToNext;
        ++(*firstReservIdx);
    }
}

// returns node id if unsuccessful
static int reserveOnDemand(int train, int node, int edge, int reservManager, int *haveReserved, int termAdmin) {
    // term_print_int(termAdmin, node);
    // term_print(termAdmin, " r ", 3);
    // term_print_int(termAdmin, edge);
    // term_newline(termAdmin);
    int reservation[3];
    reservation[2] = train;
    reservation[0] = node;
    reservation[1] = edge;

    enum MessageType type;

    TypedSend(reservManager, RESERVE_SEGMENT, (char*) reservation, sizeof(reservation), &type, "", 0);
    if (type != SUCCESS) {
        return node;
    }
    
    *haveReserved += getNode(node)->edge[edge].dist;
    return -1;
}

static int updateOnDemandReservations(
    int position, int train, // general status
    int reservDist, int *haveReserved, int *nextResNode, // reservation status
    int trackAdmin, int reservManager, int termAdmin // tasks
) {
    // reserve does not count towards reserve distance
    int dummyHasReserved;
    int reservation = reserveOnDemand(train, get_reverse_sensor(position), 0, reservManager, &dummyHasReserved, termAdmin);
    if (reservation != -1) {
        return reservation;
    }
    track_node *node;
    int eType;
    enum MessageType type;

    // term_print_int(termAdmin, *haveReserved);
    // term_print(termAdmin, " ", 1);
    // term_print_int(termAdmin, reservDist);
    // term_newline(termAdmin);

    // prevent infinite loop if not enough space to reserve
    int first = *nextResNode;

    while (*haveReserved < reservDist && 0 <= *nextResNode && *nextResNode <= 143) {
        node = getNode(*nextResNode);

        // term_print_int(termAdmin, *haveReserved);
        // term_print(termAdmin, " ", 1);
        // term_print_int(termAdmin, *nextResNode);
        // term_newline(termAdmin);

        if (node->type == NODE_BRANCH) {
            // ask track admin about position
            char pos;
            TypedSend(trackAdmin, GET_POSITION, (char*) &node->num, sizeof(node->num), &type, &pos, 1);
	    assert(termAdmin, type == SUCCESS, "failed to get switch pos", 24);
            if (pos == 'C') {
                eType = DIR_CURVED;
            } else {
                eType = DIR_STRAIGHT;
            }
        } else {
            if (node->type == NODE_EXIT) // special case: reserve less if hit exit
                break;
            eType = DIR_AHEAD;
        }
        int reservation = reserveOnDemand(train, *nextResNode, eType, reservManager, haveReserved, termAdmin);
        if (reservation != -1) {
            return reservation;
        }
        *nextResNode = node->edge[eType].dest->id;
        if (*nextResNode == first)
            break;
    }
    return -1;
}

// returns 1 if execution is completely done
static int findDelay(
    int pathId, int curIdx, int plen, // path info
    int curTime, int pathDelay, int *curSpeed, int *curFromAbove, // timing info
    int delayCourier, int *delayCourierReady, int trackAdmin, int trainAdmin, int termAdmin // tasks
) {
    // set next switches, if necessary
    int nextCommand = curIdx;
    struct path_node *commandNode;
    int minDelayTime = -1;
    // term_print(termAdmin, "Path delay ", 11);
    // term_print_int(termAdmin, pathDelay);
    // term_newline(termAdmin);
    while(1) {
        commandNode = getPathNode(pathId, nextCommand);
        while (nextCommand < plen && (commandNode->commandType == NONE || commandNode->wasExecuted)) {
            ++nextCommand;
            commandNode = getPathNode(pathId, nextCommand);
        }

        // term_print(termAdmin, "Idx ", 4);
        // term_print_int(termAdmin, nextCommand);
        // term_print(termAdmin, " ", 1);
        
        if (nextCommand == plen)
            break;
        
        int timeToCommand = pathDelay + commandNode->arrivalTime + commandNode->commandDelay - curTime;
        // term_print_int(termAdmin, timeToCommand);
        // term_print(termAdmin, " ", 1);
        if (timeToCommand > 0) {
            if (timeToCommand < minDelayTime || minDelayTime == -1)
                minDelayTime = timeToCommand;
        } else {
            sendCmd(trackAdmin, trainAdmin, pathId, nextCommand, curSpeed, curFromAbove, termAdmin);
        }
            
        ++nextCommand;
    }
    if (minDelayTime > MAX_DELAY)
        minDelayTime = MAX_DELAY;
    // term_print(termAdmin, "Min delay ", 10);
    // term_print_int(termAdmin, minDelayTime);
    // term_newline(termAdmin);
    if (minDelayTime > 0) {
        if (delayCourierReady) {
            delayCourierReady = 0;
            TypedReply(delayCourier, SUCCESS, (char*) &minDelayTime, sizeof(minDelayTime));
        }
        return 0;
    }
    return 1;
}

void ExecutePath() {
    // other tasks
    int pathManager = MyParentTid();
    int trackAdmin = WhoIs("TrackAdmin");
    int trainAdmin = WhoIs("TrainAdmin");
    int termAdmin = WhoIs("TermAdmin");
    int reservManager = WhoIs("ReservManager");
    int clockServer = WhoIs("ClockServer");
    // SSR
    enum MessageType type, type2;
    int tid;
    int speed[2];
    int beginArgs[2];
    int args[2];
    struct ExecutePathRequest request;

    // setup workers
    int delayCourier = Create(CLIENT_PRIORITY_HIGH, DelayCourier);
    int isDelayUntil = 0;
    TypedReceive(&tid, &type, "", 0);
    assert(termAdmin, type == DELAY_ARGS, "Incorrect config request delay courier", 38);
    int delayCourierReady = 0;

    int awaitSensors = Create(ADMIN_PRIORITY, AwaitSensor);
    TypedReceive(&tid, &type, "", 0);
    assert(termAdmin, type == AWAIT_SENSOR_INIT, "Incorrect init request await sensors", 36);    

    TypedSend(pathManager, PATH_EXEC_INIT, "", 0, &type, (char*) &request, sizeof(request));
    // train information
    int train = request.train;
    int curSpeed, curFromAbove, reservDist, haveReserved;
    int reservIdx = 0;
    int firstReservIdx = 0;
    const struct train_data* data;
    // state while executing
    struct path_node *curNode;
    int position = request.nodeId;
    int positionIdx = 0;
    int pathDelay = 0;
    int isExecutingPath = 0; 
    int plen = 0;
    // state while tracking
    int nextResNode = position;
    // helper
    int reservation;

    int last_incorrect_sensor = -1;

    int isDelaying = 0;

    // finish worker setup
    TypedReply(delayCourier, SUCCESS, (char*) &isDelayUntil, sizeof(isDelayUntil));
    args[0] = position;
    args[1] = train;
    TypedReply(awaitSensors, SUCCESS, (char*) args, sizeof(args));

    //continue; // skip receiving first time to set up either path execution or position tracking

    while(1) {

        // update train data
        TypedSend(trainAdmin, GET_SPEED, (char*) &request.train, 4, &type2, (char*) speed, sizeof(speed));
        curSpeed = speed[0];
        curFromAbove = speed[1];

        data = getTrainData(train, 0, 0, termAdmin);
        assert(termAdmin, data != 0, "no data for train", 17);

        reservDist = data->stopping_distance_mm[curSpeed][curFromAbove] + RESERVE_BUFFER;

        // term_print(termAdmin, "Current reserv dist ", 20);
        // term_print_int(termAdmin, reservDist);
        // term_newline(termAdmin);
        // term_print_int(termAdmin, type);
        // term_newline(termAdmin);
        switch(type) {
            case EXEC_PATH:
                term_print(termAdmin, "Starting exec path", 18);
                term_newline(termAdmin);

                isExecutingPath = 1;

                // set up state
                haveReserved = 0;
                plen = getPathLength(request.pathId);
                positionIdx = -2;
                pathDelay = 0;

                // reset reservations
                TypedSend(reservManager, RELEASE_ALL, (char*) &train, sizeof(train), &type, "", 0);

                // start of execution
                if (getReverseAtStart(request.pathId)) {
                    TypedSend(trainAdmin, REVERSE_TRAIN, (char*) &request.train, sizeof(request.train), &type, "", 0);
                    assert(termAdmin, type == SUCCESS, "Set speed failed", 16);
                    // if(get_reverse_sensor(position) != getPathNode(request.pathId, 0)->nodeId) {
                    //     term_print(termAdmin, "Incorrect starting pos", 22);
                    //     term_newline(termAdmin);
                    //     TypedSend(pathManager, PATH_EXEC_COMPLETE, "", 0, &type, (char*) &request, sizeof(request));
                    //     continue;
                    // }
                    // positionIdx = -1; // current position not in path
                } // else {
                //     positionIdx = getSensorIdx(request.pathId, 0, plen, position);
                //     if (positionIdx == -1) {
                //         term_print(termAdmin, "Pos not in path", 15);
                //         term_newline(termAdmin);
                //         TypedSend(pathManager, PATH_EXEC_COMPLETE, "", 0, &type, (char*) &request, sizeof(request));
                //         continue;
                //     }
                // }

                switch(getBeginCommand(request.pathId, &beginArgs[0], &beginArgs[1])) {
                    case TRAIN_COMMAND:
                    // term_print(termAdmin, "Setting speed ", 14);
                    // term_print_int(termAdmin, beginArgs[0]);
                    // term_print(termAdmin, " ", 1);
                    // term_print_int(termAdmin, beginArgs[1]);
                    // term_newline(termAdmin);
                    curFromAbove = beginArgs[1] < curSpeed;
                    curSpeed = beginArgs[1];
                    TypedSend(trainAdmin, SET_SPEED, (char*) beginArgs, sizeof(beginArgs), &type, "", 0);
                    assert(termAdmin, type == SUCCESS, "Set speed failed", 16);
                    break;
                    default:
                    break;
                }

                int curTime = Time(clockServer);
                pathDelay = curTime - getPathBeginTime(request.pathId);
                // term_print(termAdmin, "Begin ", 6);
                // term_print_int(termAdmin, getPathBeginTime(request.pathId));
                // term_newline(termAdmin);

                // set up reservations
                reservIdx = 0;
                firstReservIdx = reservIdx;

                reservDist = data->stopping_distance_mm[curSpeed][curFromAbove] + RESERVE_BUFFER;

                // term_print(termAdmin, "Reserv dist ", 12);
                // term_print_int(termAdmin, reservDist);
                // term_newline(termAdmin);

                reservation = reserveAlongPath(train, request.pathId, &reservIdx, &haveReserved, reservDist, reservManager);
                if (reservation != -1) {
                    int cmd[2];
                    cmd[0] = train;
                    cmd[1] = 0;
                    TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);
                    assert(termAdmin, type == SUCCESS, "Set speed failed", 16);

                    TypedSend(pathManager, RESERVATION_FAILED, (char*) &reservation, 4, &type, (char*) &request, sizeof(request));
                    continue;
                }

                // term_print(termAdmin, "Res", 3);

                int done = findDelay(
                    request.pathId, 0, plen,
                    curTime, pathDelay, &curSpeed, &curFromAbove, 
                    delayCourier, &delayCourierReady, trackAdmin, trainAdmin, termAdmin
                );
                // term_print(termAdmin, "Found delay", 11);
                // term_newline(termAdmin);
                if (done) {
                    TypedSend(pathManager, PATH_EXEC_COMPLETE, "", 0, &type, (char*) &request, sizeof(request));
                    continue;
                }
            break;

            case TRACK_POSITION:
                if (isExecutingPath) {
                    // setup state
                    term_print(termAdmin, "Switching to tracking", 21);
                    term_newline(termAdmin);

                    isExecutingPath = 0;
                    haveReserved = 0;
                    nextResNode = position;

                    // reset reservations
                    TypedSend(reservManager, RELEASE_ALL, (char*) &train, sizeof(train), &type, "", 0);

                    // reserve on demand
                    int reservation = updateOnDemandReservations(
                        position, train, 
                        reservDist, &haveReserved, &nextResNode, 
                        trackAdmin, reservManager, termAdmin
                    );
                    if (reservation != -1) {
                        int cmd[2];
                        cmd[0] = train;
                        cmd[1] = 0;
                        TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);
                        assert(termAdmin, type == SUCCESS, "Set speed failed", 16);

                        TypedSend(pathManager, RESERVATION_FAILED, (char*) &reservation, 4, &type, (char*) &request, sizeof(request));
                        continue;
                    }
                    if (delayCourierReady) {
                        delayCourierReady = 0;
                        int delay = 200;
                        isDelaying = 1;
                        TypedReply(delayCourier, SUCCESS, (char*) &delay, sizeof(delay));
                    }
                }
            break;

            case SENSOR_RESERVE_FAIL:
                TypedReply(tid, SUCCESS, "", 0);
                term_print(termAdmin, "Failed to wait on sensor", 14);
                term_newline(termAdmin);

                // set speed to zero
                int cmd[2];
                cmd[0] = train;
                cmd[1] = 0;
                TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);
                assert(termAdmin, type == SUCCESS, "Set speed failed", 16);

                if (isExecutingPath && delayCourierReady) {
                    isDelaying = 1;
                    int delay = 300;
                    pathDelay += delay;
                    TypedReply(delayCourier, SUCCESS, (char*) &delay, sizeof(delay));
                    delayCourierReady = 0;
                }
            break;

            case SENSOR_TRIGGERED:
                TypedReply(awaitSensors, SUCCESS, "", 0);

                // term_print(termAdmin, "Exec received ", 14);
                // term_print_int(termAdmin, args[0]);
                // term_newline(termAdmin);

                position = args[0];

                // forward information to pathManager
                if (isExecutingPath) {
                    // is this on the path?
                    int curTime = Time(clockServer);
                    int nextIdx = getSensorIdx(request.pathId, 0, plen, args[0]);

                    if (nextIdx != -1) {
                        last_incorrect_sensor = -1;
                        curNode = getPathNode(request.pathId, nextIdx);
                        pathDelay = curTime - curNode->arrivalTime;

                        // term_print_int(termAdmin, args[0]);
                        // term_print(termAdmin, " ", 1);
                        // term_print_int(termAdmin, pathDelay);
                        // term_newline(termAdmin);

                        // catch up on path execution if necessary
                        int done = findDelay(
                            request.pathId, 0, plen, 
                            curTime, pathDelay, &curSpeed, &curFromAbove, 
                            delayCourier, &delayCourierReady, trackAdmin, trainAdmin, termAdmin
                        );
                        if (done) {
                            TypedSend(pathManager, PATH_EXEC_COMPLETE, "", 0, &type, (char*) &request, sizeof(request));
                            continue;
                        }

                        // term_print(termAdmin, "p", 1);

                        // update reservations
                        reservDist = data->stopping_distance_mm[curSpeed][curFromAbove] + RESERVE_BUFFER;
                        
                        releaseAlongPath(train, request.pathId, &firstReservIdx, nextIdx - 2, reservIdx, &haveReserved, reservManager);
                        // term_print(termAdmin, "l", 1);
                        reservation = reserveAlongPath(train, request.pathId, &reservIdx, &haveReserved, reservDist, reservManager);
                        if (reservation != -1) {
                            int cmd[2];
                            cmd[0] = train;
                            cmd[1] = 0;
                            TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);
                            assert(termAdmin, type == SUCCESS, "Set speed failed", 16);

                            TypedSend(pathManager, RESERVATION_FAILED, (char*) &reservation, 4, &type, (char*) &request, sizeof(request));
                            continue;
                        }
                        // term_print(termAdmin, "r", 1);
                        
                        positionIdx = nextIdx;

                        // update pathManager
                        TypedSend(pathManager, UPDATE_POSITION, (char*) args, sizeof(args), &type, (char*) &request, sizeof(request));
                        // term_print(termAdmin, "u", 1);
                        if (type != EXEC_PATH) {
                            continue;
                        }
                    } else {
                        // allow a couple of bad sensor triggers
                        // TODO: reserve according to bad sensor triggers
                        if (last_incorrect_sensor != -1 && args[0] != get_reverse_sensor(last_incorrect_sensor)) {
                            term_print(termAdmin, "Bad sensor ", 11);
                            term_print_int(termAdmin, args[0]);
                            term_newline(termAdmin);

                            // stop train
                            int cmd[2];
                            cmd[0] = request.train;
                            cmd[1] = 0;
                            TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);
                            assert(termAdmin, type == SUCCESS, "Set speed failed", 16);

                            TypedSend(pathManager, UNEXPECTED_POSITION, (char*) args, sizeof(args), &type, (char*) &request, sizeof(request));
                            continue;
                        }
                        // term_print(termAdmin, "b", 1);
                        last_incorrect_sensor = args[0];
                    }
                } else {
                    // update reservations
                    nextResNode = position;

                    TypedSend(reservManager, RELEASE_ALL, (char*) &train, sizeof(train), &type, "", 0);
                    haveReserved = 0;
                    int reservation = updateOnDemandReservations(
                        position, train, 
                        reservDist, &haveReserved, &nextResNode, 
                        trackAdmin, reservManager, termAdmin
                    );
                    if (reservation != -1) {
                        int cmd[2];
                        cmd[0] = train;
                        cmd[1] = 0;
                        TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);
                        assert(termAdmin, type == SUCCESS, "Set speed failed", 16);

                        TypedSend(pathManager, RESERVATION_FAILED, (char*) &reservation, 4, &type, (char*) &request, sizeof(request));
                        continue;
                    }

                    TypedSend(pathManager, UPDATE_POSITION, (char*) args, sizeof(args), &type, (char*) &request, sizeof(request));
                    if (type != TRACK_POSITION)
                        continue;
                }
            break;

            case DELAY_QUERY:
                delayCourierReady = 1;
                if (isExecutingPath) {

                    if (isDelaying) {
                        isDelaying = 0;
                        int cmd[2];
                        cmd[0] = train;
                        cmd[1] = 7;
                        TypedSend(trainAdmin, SET_SPEED, (char*) cmd, sizeof(cmd), &type, "", 0);
                        assert(termAdmin, type == SUCCESS, "Set speed failed", 16);
                    }

                    int curTime = Time(clockServer);
                    int done = findDelay(
                        request.pathId, 0, plen,
                        curTime, pathDelay, &curSpeed, &curFromAbove, 
                        delayCourier, &delayCourierReady, trackAdmin, trainAdmin, termAdmin
                    );
                    if (done) {
                        TypedSend(pathManager, PATH_EXEC_COMPLETE, "", 0, &type, (char*) &request, sizeof(request));
                        continue;
                    }

                } else {
                    //term_print(termAdmin, "Checking for update", 19);
                    //term_newline(termAdmin);
                    int delay = 200;
                    TypedReply(tid, SUCCESS, (char*) &delay, sizeof(delay));
                    // periodically check if pathmanager wants us to actually do something
                    TypedSend(pathManager, NO_UPDATE, "", 0, &type, (char*) &request, sizeof(request));
                    continue;
                }
            break;

            default:
                // term_print(termAdmin, "help", 4);
                // term_print_int(termAdmin, type);
                // term_newline(termAdmin);
                TypedReply(tid, ERROR, "", 0);
        }

        // wait for more info
        TypedReceive(&tid, &type, (char*) &args, sizeof(args));
    }
}
