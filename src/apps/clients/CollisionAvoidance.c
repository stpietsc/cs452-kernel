#include "../path.h"
#include "../track.h"
#include "../syscall.h"
#include "../io_helpers.h"
#include "../train_data.h"
#include "../velocity_helpers.h"

#define TRAIN_LENGTH 200 // in mm
#define MAX_NODES_USED 30 // an estimate
#define TICKS_TO_CHECK 100


static int termAdmin = 0;
static int trackAdmin = 0;
static int trainAdmin = 0;
static int curTime = 0;
static int pathManager = 0;
static enum MessageType type;
static struct CollisionAvoidanceRequest req;
struct CollisionInfo {
        int trainId;
        int lastCommandTime;
        int speedBeforeCmd;
        int speedBeforeFromAbove;
        int speedAfterCmd;
        track_node* nodes[MAX_NODES_USED];
        int numNodes;
        int stopped;
};

static struct CollisionInfo info[NUM_TRAINS];
static void add_node_to_info(int i, track_node* node){
	info[i].nodes[info[i].numNodes] = node;
	info[i].numNodes++;
}

static int all_nodes[MAX_NODES_USED * NUM_TRAINS];
static int node_to_train_i[MAX_NODES_USED * NUM_TRAINS];
static int node_count;
static void add_node_to_all_nodes(track_node* node, int i){
/*	
	term_print(termAdmin, "train ", 6);
	term_print_int(termAdmin, info[i].trainId);
	term_print(termAdmin, " node ", 6);
	term_print_int(termAdmin, node->id);
	term_newline(termAdmin);
*/	
	all_nodes[node_count] = node->id;
	node_to_train_i[node_count] = i;
	node_count++;
}
static int check_if_node_used(track_node* node){
	int nodeId = node->id;
	for(int i = 0; i < node_count; ++i){
		if(all_nodes[i] == nodeId) return node_to_train_i[i];
	}
	return -1;
}

static void emergency_stop(int i){
	assert(termAdmin, i >= 0, "negative i for stop", 19);
        if(i < 0) return;	
	if(info[i].stopped) return;
	
	term_print(termAdmin, "train ", 6);
	term_print_int(termAdmin, info[i].trainId);
	term_print(termAdmin, " stopped!", 9);
	term_newline(termAdmin);
	
	TypedSend(trainAdmin, EMERGENCY_STOP, (char*) &info[i].trainId, sizeof(int), &type, "", 0);

	// manually update train position (reversed)
	int pos_update_args[3];
	pos_update_args[0] = getNode(req.positions[i])->reverse->id;
	pos_update_args[1] = 0; // since velocity is changed, don't care about position from sensor
	pos_update_args[2] = info[i].trainId;
	TypedSend(pathManager, COLLISION_AVOIDED, (char*) pos_update_args, sizeof(pos_update_args), &type, "", 0);

	info[i].stopped = 1;
}

void check_collision(){
	node_count = 0;
	for(int i = 0; i < req.num_trains; ++i){
		struct CollisionInfo* cur = &info[i];
		if(info->stopped) continue; // if the train has been stopped it will not get here, so we no longer have to consider it
		/*
		term_print(termAdmin, "train ", 6);
		term_print_int(termAdmin, cur->trainId);
		term_print(termAdmin, ": ", 2);
		*/
		for(int j = 0; j < cur->numNodes; ++j){
			/*
			term_print_int(termAdmin, cur->nodes[j]->id);
			term_print(termAdmin, " ", 1);
			*/
			int other_train_i = check_if_node_used(cur->nodes[j]);
			if(cur->nodes[j]->type == NODE_EXIT || other_train_i >= 0){
				/*if(other_train_i >= 0){
					term_print(termAdmin, "train ", 6);
					term_print(termAdmin, " collides with ", 15);
					term_print_int(termAdmin, info[other_train_i].trainId);
				}*/
				// for now just stop both trains
				// TODO: maybe only stop 1?
				//if(cur->speedAfterCmd != 0){
				term_print(termAdmin, "collision predicted at ", 23);
				term_print_int(termAdmin, cur->nodes[j]->id);
				term_newline(termAdmin);

				// if speed is nonzero OR speed is 0 but we sent the stop command recently, send an em stop just in case
				if(info[i].speedAfterCmd > 0 || curTime - info[i].lastCommandTime < 500){
					emergency_stop(i);
				}
				//}else{
				if(other_train_i >= 0 && (info[other_train_i].speedAfterCmd > 0 || curTime - info[other_train_i].lastCommandTime < 500)){
					emergency_stop(other_train_i);
				}
				//}
			}
		}
		if(info->stopped) continue; // if the train has been stopped it will not get here, so we no longer have to consider it
		for(int j = 0; j < cur->numNodes; ++j){
			add_node_to_all_nodes(cur->nodes[j], i);
			add_node_to_all_nodes(cur->nodes[j]->reverse, i);
		}
	}
	node_count = 0;
}

void update_train_info(int ticks){
	/*
	term_print(termAdmin, "tick ", 5);
	term_print_int(termAdmin, ticks);
	term_newline(termAdmin);
	*/
	for(int i = 0; i < req.num_trains; ++i){
		info[i].numNodes = 0;
		if(info[i].stopped) continue; // if the train has been stopped it will not get here, so we no longer have to consider it
		int train = req.train_ids[i];
		
		int offset = req.offsets[i];
		track_node* last;
		track_node* cur = getNode(req.positions[i]);
		
		int dist = 0;

		// actually, we might not need this if we just look far enough in the future
		/*
		// look a train length behind cur -> needed in case train is stopped
		// TODO: this might not work right after we pass a branch
		track_node* rev = cur->reverse;
//		term_print(termAdmin, "A", 1);
		
		term_newline(termAdmin);
		term_print_int(termAdmin, req.positions[i]);
		term_newline(termAdmin);
		term_print_int(termAdmin, cur->num);
		term_newline(termAdmin);
		term_print_int(termAdmin, cur->id);
		term_newline(termAdmin);
		term_print_int(termAdmin, rev->id);
			
		while(dist < TRAIN_LENGTH){
			if(rev->type == NODE_BRANCH){
                                char pos;
                                TypedSend(trackAdmin, GET_POSITION, (char*) &rev->num, sizeof(int), &type, (char*) &pos, sizeof(char));
                                if(pos == 'S'){
                                        dist += rev->edge[0].dist;
                                        rev = rev->edge[0].dest;
                                }else if(pos == 'C'){
                                        dist += rev->edge[1].dist;
                                        rev = rev->edge[1].dest;
                                }else{
                                        // error?
                                }
                        }else{
                                dist += rev->edge[0].dist;
                                rev = rev->edge[0].dest;
                        }
		}
		cur = rev->reverse;
		*/
//		term_print(termAdmin, "B", 1);
		// currently uses avg velocity, not exact
		dist += distance_micrometers_traveled_between(termAdmin, train, info[i].lastCommandTime, info[i].speedBeforeCmd, info[i].speedBeforeFromAbove, info[i].speedAfterCmd, 0, 0, curTime, curTime + ticks) / 1000; // fricky frick
	        dist += offset;
	/*	
		term_print(termAdmin, "train ", 6);
		term_print_int(termAdmin, train);
		term_print(termAdmin, ": ", 2);
		term_print_int(termAdmin, info[i].speedBeforeCmd);
		term_print(termAdmin, " ", 1);
		term_print_int(termAdmin, info[i].speedAfterCmd);
		term_print(termAdmin, " ", 1);
		term_print_int(termAdmin, dist);
		term_newline(termAdmin);
	*/	
		//term_print_int(termAdmin, dist);
		//term_print(termAdmin, " ", 1);
		

		while(1){
			/*
		term_print(termAdmin, "dist ", 5);
		term_print_int(termAdmin, dist);
		term_print(termAdmin, ", node ", 7);
		term_print_int(termAdmin, cur->id);
		term_print(termAdmin, ", ", 2);
		term_print_int(termAdmin, cur->num);
		term_print(termAdmin, ", ", 2);
		term_print(termAdmin, cur->name, 5);
		term_newline(termAdmin);
		*/
			last = cur;
			if(cur->type == NODE_BRANCH){
				char pos;
				TypedSend(trackAdmin, GET_POSITION, (char*) &cur->num, sizeof(int), &type, (char*) &pos, sizeof(char));
				if(pos == 'S'){
					dist -= cur->edge[0].dist;
					cur = cur->edge[0].dest;
				}else if(pos == 'C'){	
					dist -= cur->edge[1].dist;
					cur = cur->edge[1].dest;
				}else{
					term_print(termAdmin, "weird switch pos ", 16);
					term_print(termAdmin, &pos, 1);
					term_newline(termAdmin);
				}
				// if we reach an exit, that's where we'll end up -> need to stop!
			}else if(cur->type == NODE_EXIT){
				add_node_to_info(i, cur);
				break;
			}else{
				dist -= cur->edge[0].dist;
				cur = cur->edge[0].dest;
			}

			// get all nodes within a TRAIN_LENGTH of the expected position
			if(dist < -TRAIN_LENGTH){
				add_node_to_info(i, last);
				add_node_to_info(i, cur);
				break;
			}else if(dist < TRAIN_LENGTH){
				add_node_to_info(i, last);
			}
			
		}

//		term_print(termAdmin, "C", 1);
	}
}

void CollisionAvoidance() {
    RegisterAs("CollisionAvoidance");
    // init
    termAdmin = WhoIs("TermAdmin");
    trackAdmin = WhoIs("TrackAdmin");
    int clockServer = WhoIs("ClockServer");
    trainAdmin = WhoIs("TrainAdmin");
    pathManager = MyParentTid();

    while (1) {
        TypedSend(pathManager, CHECK_COLLISION, "", 0, &type, (char*) &req, sizeof(req));
	// TODO: change this to use data based on the segment where the train is?	

	// at each tick, compute the offset from the current node
	// transform this into start and end nodes for each train (list?)
	// check if there is a collision
	//term_print(termAdmin, "checking collisions", 19);
	//term_newline(termAdmin);
	curTime = Time(clockServer);

	/*
	// TEST
	Delay(clockServer, 600);
	req.num_trains = 2;
	req.train_ids[0] = 24;
	req.train_ids[1] = 58;
	req.positions[0] = 60;
	req.positions[1] = 76;
	req.offsets[0] = 0;
	req.offsets[1] = 0;
*/
	for(int i = 0; i < req.num_trains; ++i){
		info[i].trainId = req.train_ids[i];
		int repl[3];
		TypedSend(trainAdmin, GET_LAST_CMD, (char*) &info[i].trainId, sizeof(int), &type, (char*) repl, sizeof(repl));
		info[i].lastCommandTime = repl[0];
		info[i].speedBeforeCmd = repl[1];
		info[i].speedBeforeFromAbove = repl[2];
		TypedSend(trainAdmin, GET_SPEED, (char*) &info[i].trainId, sizeof(int), &type, (char*) &info[i].speedAfterCmd, sizeof(int));
		//info[i].speedAfterCmd = SPEED_HIGH;
		info[i].stopped = 0;
	}
	for(int ticks = 0; ticks <= TICKS_TO_CHECK; ++ticks){
		if(ticks % 5 == 0){
			/*
			term_print(termAdmin, "checking tick ", 14);
			term_print_int(termAdmin, ticks);
			term_print(termAdmin, ": ", 2);
			*/
			update_train_info(ticks);
		
			check_collision();
			//term_newline(termAdmin);
		}
	}
    }
}
