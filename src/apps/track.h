#pragma once

#include "track_node.h"

#define NUM_BROKEN_SWITCHES 2

enum Track {
    TRACK_A,
    TRACK_B,
    UNKNOWN
};

struct set_switch {
  enum Track track;
  int num;
  int track_id;
  char posn;
};

void init_broken_switches();
struct set_switch* getBrokenSwitch(int idx);

void setupTrack(enum Track track, int trackAdmin);
track_node getLoopNode();
void setDist(int idx, int dist);
int getDist(int idx);
track_node *getNode(int idx);

void get_diagram_info(int nodeNr, int edge, int *row, int *col, char *c);
