#pragma once

#if defined(DEBUG) || defined(CALIBRATE)

#include <stdint.h>
#include <stddef.h>

// blocking writes/reads
void putc(char c);
void puts(const char* buf, size_t blen);
char getc();
void blocking_print_int(int n);

#endif
