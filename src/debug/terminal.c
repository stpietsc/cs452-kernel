#if defined(DEBUG) || defined(CALIBRATE)
#include "../kernel/rpi.h"
#include <stddef.h>
#include "../apps/syscall.h"

void putc(char c) {
  while (uart_read_register(0, 0, UART_TXLVL) == 0) asm volatile("yield");
  uart_write_register(0, 0, UART_THR, c);
}

void puts(const char* buf, size_t blen) {
  static const size_t max = 32;
  char temp[max];
  temp[0] = (0 << UART_CHANNEL_SHIFT) | (UART_THR << UART_ADDR_SHIFT);
  size_t tlen = uart_read_register(0, 0, UART_TXLVL);
  if (tlen > max) tlen = max;
  for (size_t bidx = 0, tidx = 1;;) {
    if (tidx < tlen && bidx < blen) {
      temp[tidx] = buf[bidx];
      bidx += 1;
      tidx += 1;
    } else {
      spi_send_recv(0, temp, tidx, NULL, 0);
      if (bidx == blen) break;
      tlen = uart_read_register(0, 0, UART_TXLVL);
      if (tlen > max) tlen = max;
      tidx = 1;
    }
  }
}

char getc() {
  while (uart_read_register(0, 0, UART_RXLVL) == 0) asm volatile("yield");
  return uart_read_register(0, 0, UART_RHR);
}

static int num_digits(uint64_t i) {
  if (!i)
    return 1;
  int count = 0;
  while (i) {
    i /= 10;
    ++count;
  }
  return count;
}

static void print_uint64(uint64_t i){
  int n = num_digits(i);
  int j;
  char s[n];
  for (j = n - 1; j >= 0; --j, i /= 10) {
    s[j] = i % 10 + '0';
  }
  puts(s, n);
}

void blocking_print_int(int i) {
  if(i < 0){
        putc('-');
        i = -i;
  }
  print_uint64((uint64_t) i);
}

#endif
