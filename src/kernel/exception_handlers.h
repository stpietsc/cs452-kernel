#pragma once

#include <stdint.h>

void dummy_handler(uint64_t N, uint64_t exception);
void syscall_handler(uint64_t N);
void interrupt_handler();

#ifdef TIMING
#define NUM_RUNS 100000
uint64_t get_total_test_time();
void reset_test();
#endif
