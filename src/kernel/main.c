#include "rpi.h"
#include "../apps/userTasks.h"
#include "taskQueue.h"
#include "../apps/io_helpers.h"

extern char __bss_start;
extern char __bss_end; // defined in linker script
// function from contextSwitch.S
extern void switch_to_task(struct TaskDescriptor *task);

void activate(struct TaskDescriptor *task) {
    task->state = Active;
    switch_to_task(task);
}

void main() {
    
    // INITIALIZATION

    // hardware
    init_gpio();
    init_spi(0);
    init_uart(0);

    // bss
    memset(&__bss_start, 0, &__bss_end - &__bss_start);

    initialize_tasks();
    create_task_descriptor(5, -1, firstUserTask);

    //char msg1[] = "\033[2J\033[HWelcome to the Kernel! It is (" __TIME__ ")\r\n\r\n";
    //puts(msg1, sizeof(msg1) - 1);
    
    // enable interrupts and set the timer's compare register    
    init_interrupts();
    
    // END OF INITIALIZATION
    
    while(1) {
        struct TaskDescriptor *curTask = schedule();
	    if(!curTask) 
            break;
        activate(curTask);
	
        if(curTask->state == HaltState) break;

        if(curTask->state == Ready) 
            append_task_descriptor(curTask);
    }

    debug("Exiting kernel\r\n", 16);

#ifdef CALIBRATE
#include "../apps/train_data.h"
    print_train_data();
#endif
}
