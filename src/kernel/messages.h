#include "taskDescriptor.h"

void send_message(struct TaskDescriptor* sender, struct TaskDescriptor* receiver);
void reply_message(struct TaskDescriptor* sender, struct TaskDescriptor* receiver);

void mailbox_push(struct TaskDescriptor* sender, struct TaskDescriptor* receiver);
struct TaskDescriptor* mailbox_pop(struct TaskDescriptor* receiver);
