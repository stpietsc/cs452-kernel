#pragma once

#include <stdint.h>

enum TaskState {
    Active,
    Ready,
    Zombie,
    SendWait,
    ReceiveWait,
    EventWait,

    HaltState // special state to tell kernel to halt
};

// when changing this, need to change offsets in contextSwitch.S
struct TaskDescriptor {
    int tid;
    int parentTid;
    int priority; 
    enum TaskState state;
    char* stackPtr;
    uint64_t pstate;
    uint64_t ret_addr;
    uint64_t regs[31]; // x0 - x30

    struct TaskDescriptor* schedulerNext; // the next task in the scheduler queue

    struct TaskDescriptor* mailbox; // message queue for this task
    struct TaskDescriptor* mailboxEnd;
    struct TaskDescriptor* messageNext; // when this task is in another task's message queue, the next task in the queue
};
