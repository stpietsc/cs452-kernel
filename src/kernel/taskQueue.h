#pragma once

#include "taskDescriptor.h"


void initialize_tasks();
int create_task_descriptor(int priority, int parentTid, void (*function)());
void append_task_descriptor(struct TaskDescriptor* task);
struct TaskDescriptor* get_task_descriptor(int idx);
struct TaskDescriptor* get_cur_task_descriptor();
struct TaskDescriptor* schedule();
