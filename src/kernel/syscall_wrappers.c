#include "../apps/io_helpers.h"
#include "../apps/NameServer.h"
#include <string.h>
#include "../apps/syscall.h"

/*
 * Typed message passing
 */

int TypedSend(int tid, enum MessageType type, const char *msg, int msglen, enum MessageType *rtype, char *reply, int rplen) {
    char tmsg[msglen + 1];
    char treply[rplen + 1];
    tmsg[0] = type;
    for (int i = 0; i < msglen; ++i)
        tmsg[i+1] = msg[i];
    int len = Send(tid, tmsg, msglen + 1, treply, rplen + 1);
    // check cases
    if (len >= 1) {
        *rtype = treply[0];
        if (len > 1) {
            for (int i = 0; i < rplen; ++i) 
                reply[i] = treply[i+1];
        }
        return len - 1;
    }
    if (len == 0) { // no type in response
        *rtype = NO_MESSAGE;
    }
    return len;
}

int TypedReceive(int *tid, enum MessageType *type, char *msg, int msglen) {
    char tmsg[msglen + 1];
    int len = Receive(tid, tmsg, msglen + 1);
    // check cases
    if (len >= 1) {
        *type = tmsg[0];
        for (int i = 0; i < msglen; ++i) 
            msg[i] = tmsg[i+1];
        return len - 1;
    }
    if (len == 0) {
        *type = NO_MESSAGE;
    }
    return len;
}

int TypedReply(int tid, enum MessageType type, const char *reply, int rplen) {
    char treply[rplen + 1];
    treply[0] = type;
    for (int i = 0; i < rplen; ++i) // strncpy did not work here
		treply[i+1] = reply[i];
    int len = Reply(tid, treply, rplen + 1);
    if (len > 0)
        return len - 1;
    return len;
}

/*
 * Name Server
 */

// require name to be null terminated
int RegisterAs(const char* name) {
    enum MessageType type;
    char resp;
    int nlen = strlen(name); // doesn't include null terminator
    if (nlen > NAME_LENGTH) // name too long error
        return -2; // NAME TOO LONG ERROR
    int len = TypedSend(NAME_SERVER_TID, REGISTER_NAME, name, nlen + 1, &type, &resp, 1);
    // must have not sent to name server correctly
    if (len < 0 || type != SUCCESS)
        return -1; // TASK ID INCORRECT ERROR
    return 0;
}

int WhoIs(const char* name) {
    enum MessageType type;
    char resp[1];
    int nlen = strlen(name); // note: doesn't include null terminator
    if (nlen > NAME_LENGTH)
        return -2; // NAME TOO LONG ERROR
    int len = TypedSend(NAME_SERVER_TID, QUERY_NAME, name, nlen + 1, &type, resp, 1);
    // must have not sent to name server correctly
    if (len < 0)
        return -1; // TASK ID INCORRECT ERROR
    if (len && type == TASK_ID) {
        return resp[0];
    }
    if (type == ERROR)
        return -3; // NO MATCHING REGISTRATION

    // unknown message type
    debug("No matching message type\r\n", 26);
    return -3; // INTERNAL ERROR
}

/*
 * Clock server
 * Note that these wrappers cannot detect if the tid is valid or not if they get a SUCCESS response
 * Hence the final responsibility of providing a correct tid lies with the user task
 */

int Time(int tid) {
    enum MessageType type;
    int resp;
    int len = TypedSend(tid, TIME_REQUEST, "", 0, &type, (char*) &resp, 4);
    if (len < 0)
        return -1; // TID is not a valid clock server task
    if (len && type == SUCCESS)
        return resp;
    return -1; // TID is not a valid clock server task
}

int Delay(int tid, int ticks) {
    if (ticks < 0)
        return -2; // negative delay
    enum MessageType type;
    int resp;
    int len = TypedSend(tid, DELAY_REQUEST, (char*) &ticks, 4, &type, (char*) &resp, 4);
    if (len < 0)
        return -1; // TID is not a valid clock server task
    if (len && type == SUCCESS)
        return resp;
    return -1; // TID is not a valid clock server task
}

int DelayUntil(int tid, int ticks) {
    if (ticks < 0)
        return -2; // negative delay
    enum MessageType type;
    int resp;
    int len = TypedSend(tid, DELAYUNTIL_REQUEST, (char*) &ticks, 4, &type, (char*) &resp, 4);
    if (len < 0)
        return -1; // TID is not a valid clock server task
    if (type == NEGATIVE_DELAY)
        return -2; // negative delay
    if (len && type == SUCCESS)
        return resp;
    return -1; // TID is not a valid clock server task
}

/*
 * UART Servers
 * Note that these wrappers cannot detect if the tid is valid or not if they get a SUCCESS response
 * Hence the final responsibility of providing a correct tid lies with the user task
 */

int Getc(int tid) {
    enum MessageType type;
    char msg;
    int len = TypedSend(tid, GET_CHAR, "", 0, &type, &msg, 1);
    if (len < 0)
        return -1; // TID is not a valid uart server task
    if (len && type == SUCCESS)
        return msg;
    return -1; // TID is not a valid uart server task
}

// returns the number of characters successfully read
int Gets(int tid, char *chars, int len, int timeout) {
    int args[2];
    args[0] = len;
    args[1] = timeout;
    enum MessageType type;
    int replyLen = TypedSend(tid, GET_STRING, (char*) args, sizeof(args), &type, chars, len);
    if (replyLen < 0)
        return -1; // TID is not a valid uart server task
    if (type == SUCCESS)
        return replyLen;
    return -1; // TID is not a valid uart server task
}

int Putc(int tid, char ch) {
    enum MessageType type;
    int len = TypedSend(tid, PUT_CHAR, &ch, 1, &type, "", 0);
    if (len < 0)
        return -1; // TID is not a valid uart server task
    if (type == SUCCESS)
        return 0;
    return -1; // TID is not a valid uart server task
}

int Puts(int tid, char* chars, int len) {
    enum MessageType type;
    int replyLen = TypedSend(tid, PUT_STRING, chars, len, &type, "", 0);
    if (replyLen < 0)
        return -1; // TID is not a valid uart server task
    if (type == SUCCESS)
        return 0;
    return -1; // TID is not a valid uart server task
}

