#pragma once

#include <stdint.h>
#include <stddef.h>

static const char UART_CHANNEL_SHIFT           = 1;
static const char UART_ADDR_SHIFT              = 3;

// UART registers
static const char UART_IER       = 0x01; // R/W
static const char UART_IIR       = 0x02; // R
static const char UART_MSR       = 0x06; // R

void init_gpio();
int check_gpio_pin(int pin);
void clear_gpio_pin(int pin);

void init_spi(uint32_t channel);
void spi_send_recv(uint32_t channel, const char* sendbuf, size_t sendlen, char* recvbuf, size_t recvlen);

void init_uart(uint32_t spiChannel);
char uart_read_register( size_t spiChannel, size_t uartChannel, char reg);
void uart_write_register(size_t spiChannel, size_t uartChannel, char reg, char data);
void enable_uart_interrupt(int uartId, int interruptId);
void disable_uart_interrupt(int uartId, int interruptId);

void* memcpy(void* restrict dest, const void* restrict src, size_t n);
void *memset(void *s, int c, size_t n);

uint64_t get_ticks();

void init_interrupts();
uint32_t get_interrupt_id();
void timer_mark_interrupt_handled();
void gic_mark_interrupt_handled();
void update_timer_compare();
