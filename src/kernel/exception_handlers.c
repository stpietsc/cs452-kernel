#include "../apps/io_helpers.h"
#include "taskQueue.h"
#include "syscall_codes.h"
#include "messages.h"
#include "../apps/interrupts.h"
#include "rpi.h"
#include "../apps/syscall.h"

#ifdef TIMING
#include "exception_handlers.h"
uint64_t test_start = 0;
uint64_t test_end = 0;
uint64_t num_exchanges = 0;

// should only be called after NUM_RUNS exchanges
uint64_t get_total_test_time(){
        return (test_end - test_start);
}

void reset_test(){
	test_start = 0;
	test_end = 0;
	num_exchanges = 0;
}
#endif

void dummy_handler(uint64_t N, uint64_t exception){
	// only keep lower 24 bits to get the syscall code
	N = N & ((1 << 24) - 1);
	char msg[] = "dummy exception #: ";
	debug(msg, sizeof(msg)-1);
	debug_int(exception);
	debug("\r\n", 2);
	debug("code: ", 6);
	debug_int(N);
	debug("\r\n", 2);
}

#define NUM_EVENTS 150 // number of different events (interrupts), really just a big number
static struct TaskDescriptor* awaiting_event[NUM_EVENTS] = {0};
static int events_missed[NUM_EVENTS] = {0};

void syscall_handler(uint64_t N){
	// only keep lower 24 bits to get the syscall code
	N = N & ((1 << 24) - 1);
	struct TaskDescriptor* cur_task = get_cur_task_descriptor();
	// move it out of active state for now
	cur_task->state = Ready;
	/*
	 * Assumptions:
	 * registers x0-x7 were saved in the task descriptor before entering this function
	 */
	switch(N){
		case CREATE_CODE:
			;
			uint64_t priority = cur_task->regs[0];
			uint64_t function = cur_task->regs[1];

			int new_tid = create_task_descriptor((int) priority, cur_task->tid, (void (*)()) function);

			cur_task->regs[0] = (uint64_t) new_tid;
			break;
		case MY_TID_CODE:
			cur_task->regs[0] = (uint64_t) cur_task->tid;
			break;
		case MY_PARENT_TID_CODE:
			cur_task->regs[0] = (uint64_t) cur_task->parentTid;
			break;
		case YIELD_CODE:
			// shouldn't have to do anything here, as we'll automatically get a new task
			break;
		case EXIT_CODE:
			cur_task->state = Zombie;
			// TODO: send -2 to all tasks in mailbox, also what if this task has received but not replied?
			break;
		case SEND_CODE:
			;
			#ifdef TIMING
			if(!test_start){
				test_start = get_ticks();
			}
			#endif
			int receiver_tid = (int) cur_task->regs[0];
			struct TaskDescriptor* receiver = get_task_descriptor(receiver_tid);
			if(!receiver || receiver->state == Zombie){
				// can't find this task, return -1
				cur_task->regs[0] = (uint64_t) -1;
				return;
			}
			if(receiver->state == ReceiveWait){
				send_message(cur_task, receiver);
			}else{
				mailbox_push(cur_task, receiver);		
			}
			break;
		case RECEIVE_CODE:
			#ifdef TIMING
			if(!test_start){
				test_start = get_ticks();
			}
			#endif
			if(cur_task->mailbox){
				send_message(mailbox_pop(cur_task), cur_task);
			}else{
				cur_task->state = ReceiveWait;
			}
			break;
		case REPLY_CODE:
			;
			int sender_tid = (int) cur_task->regs[0];
			struct TaskDescriptor* sender = get_task_descriptor(sender_tid);
			if(!sender || sender->state == Zombie){
				// can't find this task, return -1
				cur_task->regs[0] = (uint64_t) -1;
				return;	
			}
			if(sender->state != SendWait){
				// task is not waiting for response, return -2
				cur_task->regs[0] = (uint64_t) -2;
				return;	
			}
			reply_message(sender, cur_task);
			#ifdef TIMING
			++num_exchanges;
			if(num_exchanges == NUM_RUNS){
				test_end = get_ticks();
			}
			#endif
			break;
		case AWAIT_EVENT_CODE:
			;
			int eventid = (int) cur_task->regs[0];
			if(eventid < 0 || eventid >= NUM_EVENTS){
				// invalid event, return error code -1;
				cur_task->regs[0] = (uint64_t) -1;
				return;
			}
			if(awaiting_event[eventid]){
				// another task is waiting on this event, return error code -3
				cur_task->regs[0] = (uint64_t) -3;
				return;
			}

			awaiting_event[eventid] = cur_task;
			cur_task->state = EventWait;

			if(enable_on_await(eventid)){
				int uartId = eventid / 10;
				int interruptId = eventid % 10;
				enable_uart_interrupt(uartId, interruptId);
			}
			
			break;
		case UART_WRITE_REGISTER_CODE:
			;
			int write_channel = (int) cur_task->regs[0];
			char write_reg = (char) cur_task->regs[1];
			char data = (char) cur_task->regs[2];

			// if we are trying to write to a uart FIFO, check if it is ready first
			if (write_reg == UART_THR && uart_read_register(0, (size_t) write_channel, UART_TXLVL) == 0){
				cur_task->regs[0] = -1;
				break;
			}

			uart_write_register(0, (size_t) write_channel, write_reg, data);
			cur_task->regs[0] = 0;
			/*	
			if(write_channel){
                               debug("writing ", 8);
                               debug_int((int) data);
                               debug(" to reg ", 8);
                               debug_int((int) write_reg);
                               debug("\r\n", 2);
                        }
			*/
			// if we're writing to the train set, need to enable + check for CTS changes
			if (write_reg == UART_THR && write_channel == 1) enable_uart_interrupt(1, 3);

			break;
		case UART_READ_REGISTER_CODE:
			;
			int read_channel = (int) cur_task->regs[0];
			char read_reg = (char) cur_task->regs[1];
			
			// if we are trying to read from a uart FIFO, check if it is ready first
			if (read_reg == UART_RHR && uart_read_register(0, (size_t) read_channel, UART_RXLVL) == 0){
				cur_task->regs[0] = -1;
				break;
			}

			int ret = uart_read_register(0, (size_t) read_channel, read_reg);
			cur_task->regs[0] = ret;

			break;
		case HALT_CODE:
			cur_task->state = HaltState;
			break;
		default:
			;
			char msg[] = "Unknown syscall: ";
			debug(msg, sizeof(msg)-1);
			debug_int(N);
			debug("\r\n", 2);
			break;
	}
}

// try to wake a task waiting on an event
void try_wake_task(int event_id){
        if(awaiting_event[event_id]){
                struct TaskDescriptor* task = awaiting_event[event_id];
                awaiting_event[event_id] = NULL;

                debug_assert(task->state == EventWait, "awaiting task not in EventWait\r\n", 32);
                task->regs[0] = events_missed[event_id]; // return the number of such interrupts that had no event waiting on them
                events_missed[event_id] = 0;
                task->state = Ready;
                append_task_descriptor(task);
        }else{
                events_missed[event_id]++;
        }
}

void try_wake_and_disable_interrupt(int eventid){
	int uartId = eventid / 10;
	int interruptId = eventid % 10;
	disable_uart_interrupt(uartId, interruptId);
	try_wake_task(eventid);
}

void check_uart_interrupts(){
	// terminal
	uint8_t UART0_IIR_val = uart_read_register(0, 0, UART_IIR) & ((1 << 6) - 1);
	if(UART0_IIR_val == (1 << 1)) try_wake_and_disable_interrupt(TERMINAL_TRANSMIT_INTERRUPT);
	if(UART0_IIR_val == (1 << 2) || UART0_IIR_val == (3 << 2)) try_wake_and_disable_interrupt(TERMINAL_RECEIVE_INTERRUPT);
	
	// train
	uint8_t UART1_IIR_val = uart_read_register(0, 1, UART_IIR) & ((1 << 6) - 1);
	if(UART1_IIR_val == (1 << 1)) try_wake_and_disable_interrupt(TRAIN_TRANSMIT_INTERRUPT);
	if(UART1_IIR_val == (1 << 2) || UART1_IIR_val == (3 << 2)) try_wake_and_disable_interrupt(TRAIN_RECEIVE_INTERRUPT);
	
	// check for modem interrupt
	if(UART1_IIR_val == 0){
		uint8_t MSR_val = uart_read_register(0, 1, UART_MSR);
		// check if CTS has changed
		if(MSR_val & 1){
			// check if CTS is active
			if(MSR_val & (1 << 4)){
				//debug("CTS up\r\n", 8);
				// if active, can disable the modem interrupt
				try_wake_and_disable_interrupt(TRAIN_CTS_INTERRUPT);
			}else{
				//debug("CTS down\r\n", 10);
				// if inactive, we need to wait for another
				try_wake_task(TRAIN_CTS_INTERRUPT);
			}
		}
	}

}

void interrupt_handler(){
	struct TaskDescriptor* cur_task = get_cur_task_descriptor();
	// move it out of active state for now
	cur_task->state = Ready;
	
	// only keep lowest 10 bits
	// if we need more efficiency in the future, can put this in a while loop to handle multiple interrupts
	uint32_t interrupt_id = get_interrupt_id();
	
	// do any interrupt-specific stuff
	switch(interrupt_id){
		case TIMER_INTERRUPT:
			timer_mark_interrupt_handled();
			update_timer_compare();
			break;
		case GPIO_INTERRUPT:
			if(!check_gpio_pin(24)) break; // if pin 24 did not trigger ignore it
			
			check_uart_interrupts(); // reading from IIR should ack any interrupts
			clear_gpio_pin(24); // clear the interrupt from the gpio
			break;
		default:
			debug("Unknown interrupt code ", 23);
			debug_int(interrupt_id);
			debug("\r\n", 2);
	}

	try_wake_task(interrupt_id);

	// tell the GIC the interrupt has been handled
	gic_mark_interrupt_handled();
}
