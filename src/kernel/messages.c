#include "taskDescriptor.h"
#include <string.h>
#include "../apps/io_helpers.h"
#include "taskQueue.h"
/*
 * Invariants:
 * Before:
 * - sender has called Send and has (int tid, const char *msg, int msglen, char *reply, int rplen) in x0-x4
 * - receiver has called Receive and has (int *tid, char *msg, int msglen) in x0-x2
 * After:
 * - sender is in SendWait
 * - receiver is in Ready and is scheduled either here or in the main loop
 */
void send_message(struct TaskDescriptor* sender, struct TaskDescriptor* receiver){
	const char *src = (char *) sender->regs[1];
	char *dest = (char *) receiver->regs[1];
	uint64_t msglen = receiver->regs[2]; // receiver specifies max number of bytes in message
	if(sender->regs[2] < msglen) msglen = sender->regs[2]; // if sender wants to send fewer bytes, use that instead
	for (uint64_t i = 0; i < msglen; ++i)
		dest[i] = src[i];

	sender->state = SendWait;
	int* tid = (int*) receiver->regs[0];
	(*tid) = sender->tid;
	receiver->regs[0] = sender->regs[2]; // return the number of bytes the sender attempted to send
	if (receiver->state != Ready) { // only if curTask is not receiver
		receiver->state = Ready;
		append_task_descriptor(receiver);
	}
}
/*
 * Invariants:
 * Before:
 * - sender has called Send and has (int tid, const char *msg, int msglen, char *reply, int rplen) in x0-x4
 * - receiver has called Reply and has (int tid, const char *reply, int rplen) in x0-x2
 * After:
 * - sender is in Ready and is scheduled
 * - receiver is in Ready and will be scheduled in the kernel's main loop
 */
void reply_message(struct TaskDescriptor* sender, struct TaskDescriptor* receiver){
	debug_assert(sender->state == SendWait, "sender not in SendWait", 22);
	const char *src = (char*) receiver->regs[1];
	char *dest = (char*) sender->regs[3];
	uint64_t rplen = sender->regs[4]; // sender specifies max number of bytes in reply
	if(receiver->regs[2] < rplen) rplen = receiver->regs[2]; // if receiver wants to reply fewer bytes, use that instead
	for (uint64_t i = 0; i < rplen; ++i) // strncpy did not work here
		dest[i] = src[i];

	receiver->regs[0] = rplen;
	sender->state = Ready;
	sender->regs[0] = receiver->regs[2]; // return the number of bytes the receiver attempted to reply with
	append_task_descriptor(sender);
}

/*
 * Invariants:
 * Before:
 * - sender has called Send
 * - receiver has NOT called Receive
 * After:
 * - sender is in SendWait
 * - receiver does not change state
 */
void mailbox_push(struct TaskDescriptor* sender, struct TaskDescriptor* receiver){
	if(receiver->mailbox){
		// add to end of mailbox
		receiver->mailboxEnd->messageNext = sender;
		receiver->mailboxEnd = sender;
	}else{
		receiver->mailbox = sender;
		receiver->mailboxEnd = sender;
	}
	sender->messageNext = NULL;
	sender->state = SendWait;
}

/*
 * Invariants:
 * - receiver has called Receive
 * - receiver's mailbox is NOT empty
 */
struct TaskDescriptor* mailbox_pop(struct TaskDescriptor* receiver){
	struct TaskDescriptor* val = receiver->mailbox;
	debug_assert(val != NULL, "mailbox empty", 13);
	receiver->mailbox = receiver->mailbox->messageNext;

	return val;
}
