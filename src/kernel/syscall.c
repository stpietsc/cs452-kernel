#include "syscall_codes.h" // make sure this file is consistent with syscall_codes.h
#include <stdint.h>


// disable compiler warnings for this file
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

// when running with optimization, don't call one of these functions from the same file.

/*
 * Task creation
 */
int Create(int priority, void (*function)()) {
    // args should already be in x0, x1	
    asm ("svc 1");
    
    int tid;
    asm ("add %x[val], x0, xzr" : [val] "=r" (tid));
    return tid;
}

int MyTid() {
    int tid;
    asm ("svc 2");
    
    asm ("add %x[val], x0, xzr" : [val] "=r" (tid));
    return tid;
}

int MyParentTid() {
    int tid;
    asm ("svc 3");
    
    asm ("add %x[val], x0, xzr" : [val] "=r" (tid));
    return tid;
}

void Yield() {
    asm ("svc 4");
}

void Exit() {
    asm ("svc 5");
}

/*
 * Message passing
 */
int Send(int tid, const char *msg, int msglen, char *reply, int rplen){
	int to_return;
	asm ("svc 6");

    	asm ("add %x[val], x0, xzr" : [val] "=r" (to_return));

    	return to_return;
}

int Receive(int *tid, char *msg, int msglen){
	int to_return;
	asm ("svc 7");

    	asm ("add %x[val], x0, xzr" : [val] "=r" (to_return));
    	return to_return;
}

int Reply(int tid, const char *reply, int rplen){	
	int to_return;
	asm ("svc 8");

    	asm ("add %x[val], x0, xzr" : [val] "=r" (to_return));
    	return to_return;
}

/*
 * Interrupts
 */
int AwaitEvent(int eventid){
	int to_return;
	asm ("svc 9");

    	asm ("add %x[val], x0, xzr" : [val] "=r" (to_return));
    	return to_return;
}

/*
 * UART access
 */

int UartWriteRegister(int channel, char reg, char data){
	int to_return;
	asm ("svc 10");

    	asm ("add %x[val], x0, xzr" : [val] "=r" (to_return));
    	return to_return;
}

int UartReadRegister(int channel, char reg){
	int to_return;
	asm ("svc 11");

    	asm ("add %x[val], x0, xzr" : [val] "=r" (to_return));
    	return to_return;
}

// HALT
int Halt(){
	asm ("svc 99");
	
	//debug("THIS SHOULD NEVER PRINT\r\n", 25);
	return -1;
}

// re-enable warnings
#pragma GCC diagnostic pop
