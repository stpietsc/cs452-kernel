#include "taskDescriptor.h"
#include "../apps/io_helpers.h"
#include "../apps/syscall.h"

typedef char STACK[65536]; // STACK is a type which corresponds to 0x10000 bytes

#define MAX_TASKS 100
static struct TaskDescriptor task_descriptors[MAX_TASKS];
static STACK task_stacks[MAX_TASKS];
static int curTid = 0;
static char tooManyTasks[] = "WARNING: number of tasks exceeds maximum";

#define MAX_PRIORITY 10
struct TaskQueue {
    struct TaskDescriptor* start;
    struct TaskDescriptor* end;
    int len;
};
static struct TaskQueue taskQueues[MAX_PRIORITY];

void initialize_tasks(){
	for (int i = 0; i < MAX_PRIORITY; ++i){
		taskQueues[i].start = NULL;
		taskQueues[i].end = NULL;
		taskQueues[i].len = 0;
	}
}

// set as return value of new tasks, to ensure all tasks exit
void task_exit() {
    Exit();
}

// just declaring so I can have these in a nice order
void append_task_descriptor(struct TaskDescriptor* task);

int create_task_descriptor(int priority, int parentTid, void (*function)()) {
    if(priority < 0 || priority >= MAX_PRIORITY) return -1;
    debug_assert(curTid < MAX_TASKS, tooManyTasks, sizeof(tooManyTasks) - 1);
    if(curTid >= MAX_TASKS) return -2;
    
    char *stackPtr = task_stacks[curTid+1] - 8; // stack grows upwards through task_stacks[curTid]
    uint64_t ret_addr = (uint64_t) function;
    uint64_t pstate = (uint64_t) 5 << 6; // mask SError and FIQ interrupts, allow IRQ interrupts
    
    struct TaskDescriptor *task = &task_descriptors[curTid];
    task->tid = curTid;
    task->parentTid = parentTid;
    task->priority = priority;
    task->state = Ready;
    task->stackPtr = stackPtr;
    task->pstate = pstate;
    task->ret_addr = ret_addr;
    // return to start of task_exit function after finishing task
    // by setting link register value 
    task->regs[30] = (uint64_t) task_exit;

    // initialize linked list ptrs
    task->schedulerNext = NULL;
    task->mailbox = NULL;
    task->messageNext = NULL;

    // update curTid
    ++curTid;

    append_task_descriptor(task);

    return task->tid;
}

void append_task_descriptor(struct TaskDescriptor* task){
    debug_assert(task->state == Ready, "task not ready", 14);
    debug_assert(task->priority < MAX_PRIORITY, "priority too big", 16);
    debug_assert(task->priority >= 0, "priority too small", 18);
    struct TaskQueue* tq = &taskQueues[task->priority];

    if(tq->len){
    	tq->end->schedulerNext = task;
	tq->end = task;
	tq->len++;
    }else{
    	tq->start = task;
	tq->end = task;
	tq->len = 1;
    }
    
}

struct TaskDescriptor* get_task_descriptor(int idx) {
    debug_assert(idx < curTid && idx >= 0, "Index out of bounds (taskQueue)\r\n", 33);
    if(idx < 0 || idx >= curTid) return NULL;
    return &task_descriptors[idx];
}

struct TaskDescriptor* tq_pop(struct TaskQueue* tq){
	debug_assert(tq->len, "task queue empty", 16);
	if(tq->len){
		struct TaskDescriptor* next = tq->start;
		tq->start = tq->start->schedulerNext;
		--tq->len;
		return next;
	}else{
		return NULL;
	}
}

static struct TaskDescriptor *cur_task_descriptor;

struct TaskDescriptor *get_cur_task_descriptor(){
	return cur_task_descriptor;
}

struct TaskDescriptor *schedule() {
    for(int i = 0; i < MAX_PRIORITY; ++i){
        if(taskQueues[MAX_PRIORITY - 1 - i].len){
                cur_task_descriptor = tq_pop(&taskQueues[MAX_PRIORITY - 1 - i]);
		return cur_task_descriptor;
        }
    }
    return 0;
}
