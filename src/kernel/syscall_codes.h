#pragma once

// Task creation
#define CREATE_CODE 1
#define MY_TID_CODE 2
#define MY_PARENT_TID_CODE 3
#define YIELD_CODE 4
#define EXIT_CODE 5

// Message passing
#define SEND_CODE 6
#define RECEIVE_CODE 7
#define REPLY_CODE 8

// Interrupts
#define AWAIT_EVENT_CODE 9

// UART access
#define UART_WRITE_REGISTER_CODE 10
#define UART_READ_REGISTER_CODE 11

#define HALT_CODE 99
