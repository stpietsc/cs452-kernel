# cs452-kernel


## Description
A micokernel which automatically schedules several tasks and executes them. The kernel implements context switching, task creation, message passing, scheduling and interrupts. We have a name server user task with which tasks can register, a clock server user task which tasks can use to delay execution and an idle task which reports the fraction of execution time spent idle. Further user tasks re-implement the A0 functionality, in particular supporting the `tr`, `sw`, `rv` and `q` commands.

Custom additional commands:
- `pa <train num> <node num> <offset>` will route a train to the given node number and stop it at the offset from the node number. Afterwards, the train will travel to random additional nodes.
- `sp <train num>` halts random pathfinding.
- `st <train num> <node num>` will stop a train after hitting a certain sensor
- `rs` will reset the switch positions

For more information on our kernel structure, task structure as well as the results of experiments, see `Code documentation.pdf`.

## Installation
Use the following commands to download and build the kernel:
```
git clone https://git.uwaterloo.ca/stpietsc/cs452-kernel.git
cd cs452-kernel
make
```
Then, upload `kernel8.img` to a TFTP server and download it onto a Raspberry Pi during the boot process. 

If running on track A, you must use `make TERMONLY=1` instead. This ensures that delays are used instead of waiting for CTS interrupts.

### Compilation Options

To enable debug messages, use `make DEBUG=1`.

To disable optimization, use `make NOOPT=1`.

To disable the data cache, use `make NOICACHE=1`.

To disable the instruction cache, use `make NODCACHE=1`.

To run without a train set or on track A, use `make TERMONLY=1`.

To print results of calibration after exiting, use `make CALIBRATE=1`.

To run the message passing performance tests, use `make TIMING=1`.

It is possible to combine multiple flags, for example `make NOOPT=1 NOICACHE=1`.

## Usage
After booting, the kernel will automatically create the first user task, which creates all user tasks required. Once the interface has been initialized, the kernel starts accepting user commands through terminal input.

## Group members
Sophia Pietsch and Ethan Zhang
