XDIR:=/u/cs452/public/xdev
ARCH=cortex-a72
TRIPLE=aarch64-none-elf
XBINDIR:=$(XDIR)/bin
CC:=$(XBINDIR)/$(TRIPLE)-gcc
OBJCOPY:=$(XBINDIR)/$(TRIPLE)-objcopy
OBJDUMP:=$(XBINDIR)/$(TRIPLE)-objdump

# COMPILE OPTIONS
WARNINGS=-Wall -Wextra -Wpedantic -Wno-unused-const-variable
CFLAGS:=-g -pipe -static $(WARNINGS) -ffreestanding -nostartfiles\
	-mcpu=$(ARCH) -static-pie -mstrict-align -fno-builtin -mgeneral-regs-only

ifdef DEBUG
CFLAGS += -DDEBUG
$(info COMPILING DEBUG VERSION)
endif

ifdef TIMING
CFLAGS += -DTIMING
$(info COMPILING PERFORMANCE TESTS)
endif

ifdef NOOPT
$(info NO OPTIMIZATION)
CFLAGS += -DNOOPT
else
CFLAGS += -O3
endif

ifdef NOICACHE
$(info I CACHE DISABLED)
CFLAGS += -DNOICACHE
endif

ifdef NODCACHE
$(info D CACHE DISABLED)
CFLAGS += -DNODCACHE
endif

ifdef TERMONLY
$(info TERMINAL ONLY - NO CTS!)
CFLAGS += -DTERMONLY
endif

ifdef CALIBRATE
$(info CALIBRATING, WILL PRINT VELOCITIES ON QUIT)
CFLAGS += -DCALIBRATE
endif

ifdef NOCA
$(info NO COLLISION AVOIDANCE)
CFLAGS += -DNOCA
endif

ifdef NORESERVE
$(info NO RESERVATION FAILURES)
CFLAGS += -DNORESERVE
endif

# -Wl,option tells g++ to pass 'option' to the linker with commas replaced by spaces
# doing this rather than calling the linker ourselves simplifies the compilation procedure
LDFLAGS:=-Wl,-nmagic -Wl,-Tlinker.ld

# Source files and include dirs
MODULES := apps apps/clients apps/global apps/servers apps/workers kernel debug structs
SRC_DIR := src $(addprefix src/,$(MODULES))
BUILD_DIR := bin $(addprefix bin/,$(MODULES))
# $(info SRC_DIR is $(SRC_DIR))
SOURCES := $(wildcard *.c $(foreach sdir,$(SRC_DIR), $(sdir)/*.c)) $(wildcard *.S $(foreach sdir,$(SRC_DIR), $(sdir)/*.S))
# $(info SOURCES is $(SOURCES))
# Create .o and .d files for every .cc and .S (hand-written assembly) file
OBJECTS := $(patsubst src/%.c, bin/%.o, $(patsubst src/%.S, bin/%.o, $(SOURCES)))
# $(info OBJECTS is $(OBJECTS))
DEPENDS := $(patsubst src/%.c, bin/%.d, $(patsubst src/%.S, bin/%.d, $(SOURCES)))
# $(info DEPENDS is $(DEPENDS))

# The first rule is the default, ie. "make", "make all" and "make kernel8.img" mean the same
all: checkdirs kernel8.img

clean:
	rm -rf bin
	rm -f kernel8.elf kernel8.img

kernel8.img: kernel8.elf
	$(OBJCOPY) $< -O binary $@

kernel8.elf: $(OBJECTS) linker.ld
	$(CC) $(CFLAGS) $(filter-out %.ld, $^) -o $@ $(LDFLAGS)
	@$(OBJDUMP) -d kernel8.elf | fgrep -q q0 && printf "\n***** WARNING: SIMD INSTRUCTIONS DETECTED! *****\n\n" || true

bin/%.o: src/%.c Makefile
	$(CC) $(CFLAGS) -MMD -MP -c $< -o $@

bin/%.o: src/%.S Makefile
	$(CC) $(CFLAGS) -MMD -MP -c $< -o $@

checkdirs: $(BUILD_DIR)

$(BUILD_DIR):
	@mkdir -p $@

-include $(DEPENDS)
